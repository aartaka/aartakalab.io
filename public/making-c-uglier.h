#define TITLE Making C Code Uglier
#define DESCRIPTION "C code is scary. \
It's often hard to read and has lots of footguns. \
But it can get even uglier than you imagine. Lo and behold!"
#define IMAGE assets/pieter-van-der-heyden-anger.jpg
#define IMAGE_ALT "Engraving with a hell landscape filled with people and demons chopping, spiking, eating, and otherwise harming each other"

#include "template/header"

FIG()
IMG(IMAGE, IMAGE_ALT)
FIGCAP(A group of C programmers arguing about which indentation style is better. \
Just kidding, it is "Anger" by Pieter van der Heyden, 1558)

P()
C++ is practical, yet sometimes scary.
C is outright frightening. If someone writes code in C++, they must be smart.
If someone writes code in C, they must be crazy (well, at least I am.)

P()
But still, C—with its guts full of eldritch horrors—is
the lingua franca of programming and the most portable assembly language.

P()
C is readable enough to most programmers, because most mainstream languages
are
A(https://wikipedia.org/wiki/List_of_C-family_programming_languages, C progenies.)
Pointers and macros are loathsome, but they are rare enough (are they?) to ignore.

P()
So how scary can C code get?
Not as a production use, but rather as an exercise in aesthetics.
This post goes through a set of things that can convolute/obfuscate C code,
from the minute details to critical readability losses.

P()
Note that some obvious things like
ULI Inconsistency.
LI Typos.
LI Bad naming.
LI A(https://wiki.c2.com/?ThreeStarProgrammer, Pointer abuse).
END(UL)

P()
are not mentioned to leave space for the scarier ones.

SECTION2(test-program, Test Program)

P()
I'm going to use a slightly modified
A("https://wikipedia.org/wiki/TPK_algorithm#C_implementation", C version of Trabb Pardo Knuth algorithm)
from Wikipedia because it's small enough
while still showcasing most of C constructs/features:

PRE(c)
HASH()include LT()math.h>;
HASH()include LT()stdio.h>;

double f (double t)
{
    return sqrt(fabs(t)) + 5 * pow(t, 3);
}

void tpk (void)
{
    double y, a[11] = {0};
    for (int i = 0; i LT() 11; i++)
         scanf("%lf", AMP()a[i]);

    for (int i = 10; i GT()= 0; i--)
        if ((y = f(a[i])) GT() 400)
            printf("%d TOO LARGE\n", i);
        else
            printf("%d %.16g\n", i, y);
}

int main (void)
{
    tpk();
    return 0;
}
PRECAP(Trabb Pardo Knuth algorithm implementation in C)

SECTION2(style, Benign: Indentation and Bracket Placement Style)

P()
Two of four spaces? Eight? Or three, maybe?
Or—God almighty—tabs?
A(https://wikipedia.org/wiki/Indentation_style, C code styles are numerous)
and these styles have only one thing in common:
all the styles are mutually incompatible and un-aesthetic.
No matter which style one prefers—they're delusional and wrong,
at least to the ones exhorting another style.

P()
I use Linux kernel style, which might make you scream from the 8 spaces-wide tabs. But I'm not surrendering it.

P()
As a matter of example, I'll use the Pico indentation style (four/five spaces)
and bracket placement (before the first expression and after the last one.)
Plus added spaces mimicking the Glib style:

PRE(c)
double
f (double t)
{   return
    sqrt (fabs (t)) + 5 * pow (t, 3); }
PRECAP(Sub-function of TPK algorithm re-indented in Pico style)

P()
Ugh, block scope and control flow are illegible now.

SECTION2(subscripts, Confusing: Subscripts)

P()
A queer behavior of the standard array subscripts:
the index and array parts can be swapped:

PRE(c)
double a[11] = {0}, y;
for (int i = 0; i LT() 11; i++)
        scanf ("%lf", AMP()i[a]);
PRECAP(Inner loop code with confusingly reversed array subscripts)

P()
This reversal is modest, but nonetheless galling.

P()
An exercise to the reader: can you find the exact spot where the subscript is reversed?

SECTION2(k-n-r-style, Antiquated: K AMP() \R style)

P()
That's where the post gets shuddery.
K AMP() \R style, or, as they call it, "I don't understand old C code".

PRE(c)
double
f(t)
double t;
{   return
    sqrt (fabs (t)) + 5 * pow (t, 3); }
PRECAP(A function rewritten with types after the parameter list)

P()
This style

ULI duplicates parameter names,
LI moves the type information further away from the parameter list,
LI removes the typing information from the function.
END(UL)

P()
Luckily, C23 finally removes it,
after more than thirty years of yielding to the horror
and maintaining it in deprecated status.

SECTION2(recursion, Smart: Recursion)

P()
Reordering and refactoring functions is always fun.
So how about turning all the for-loops into recursion?
Recursion is cool, I've heard.
So here's a recursive rendering of the number printing loop:

PRE(c)
void
print_nums(a, i)
double *a;
int i;
{    if (i LT() 0)
         return;
     double y = f (i[a]);
     if (y GT() 400)
         printf ("%d TOO LARGE\n", i);
     else
         printf ("%d %.16g\n", i, y);
     print_nums (a, --i); }
PRECAP(Number printing loop refactored as a recursive function folded over an array)

P()
Five more code lines, lots of stack frames (unless you have tail call elimination),
and overall less comprehensible control flow. Yay!

<details>
<summary>
Recursion is good, actually
</summary>
P()
Like some un-aesthetic and alienating changes this page lists, recursion might be useful. It can make your algorithms simple and powerful when done right. I often use recursion when writing Lisp. But I can relate to people seeing it as vile and perplexing.
</details>

SECTION2(ternaries, Terse: Ternaries)

P()
This is my favorite: switching from if-else to ternaries.
It's shorter, expression-only, and it makes code look more daunting.
And there's a rumor that compilers increase the optimization level
when they see ternaries.
Likely, out of regard for programmer's bravery.

PRE(c)
void
print_nums (a, i)
double *a;
int i;
{    double y;
     (i LT() 0) ? 0 :
               (y = f (i[a]),
                (y GT() 400 ? printf ("%d TOO LARGE\n", i) :
                           printf ("%d %.16g\n", i, y)))
               print_nums (a, --i); }
PRECAP(Code with if statements replaced with ternaries)

P()
If only comma operator allowed for variable declaration
(wink wink C standard committee),
this function might've had no <code>double y</code> in it either.
But, for now, let this stateful statement stay there.

<details>
<summary>
Ternaries are good, actually
</summary>
P()
I like the ternary-formatted code because it forces a side effect-less algos where I want it to. It's even more useful in other C-like languages because they have less restrictive blocks and more abstractions compatible with functional style.
</details>

SECTION2(delimiter-first, Unconventional: Delimiter-First Code)

P()
There are reasons one can use
A(https://arogozhnikov.github.io/2022/11/29/delimiter-comes-first.html, leading-delimiter style in SQL and Haskell.)
But in other languages...

PRE(c)
void
print_nums (a, i)
double *a;
int i;
{    double y;
     (i < 0)
     ? 0
     : (y = f (i[a])
        , (y GT() 400
           ? printf ("%d TOO LARGE\n"
                     , i)
           : printf ("%d %.16g\n"
                     , i, y))
        , print_nums (a, --i)); }
PRECAP(Using leading commas and ternaries in function calls)

P()
I like how the ternaries become more pronounced
and how it promotes a functional-ish style.
But I bet, your eyes are already hemorrhaging,
so feel free to ignore my aesthetic preferences.

SECTION2(alt-representations, Awful: Alternative representations)

P()
That's the most horrifying one:
C has alternatives to some characters
that weren't there at the time of the first standard.
There are two-(digraphs)
and three-character (trigraphs, deprecated in C23) encodings
for CD([), CD(^), CD({) etc.
Here's a table of transformations:

<table>
<thead>
<tr> <th>C char <th>Digraph <th>Trigraph
<tbody>
 <tr> <td>{<td>LT()% <td>??LT()
 <tr> <td>} <td>%GT() <td>??GT()
 <tr> <td>[<td>LT(): <td>??(
 <tr> <td>] <td>:GT() <td>??)
 <tr> <td># <td>%: <td>??=
 <tr> <td>\\ <td> <td>??/
 <tr> <td>^ <td> <td>??SQUOTE()
 <tr> <td>| <td> <td>??!
 <tr> <td>~ <td> <td>??-
 <caption> All the digraphs and trigraphs with their decoding </caption>
</table>


P()
And here's the code with encoded parts:

PRE(c)
void
read_nums (a, i)
double *a;
int i;
LT()%  if (i == 11)
    LT()%  return; %GT()
    else
        LT()%  scanf ("%lf", AMP()i LT():a:GT());
        read_nums (a, ++i);%GT() %GT()
PRECAP(C code using digraphs)

P()
And that's just digraphs, trigraphs are even worse!

<details>
<summary>
Alternative representations are good, actually
</summary>
P()
There is a more useful side to alternative encodings.
CD(LT()iso646.h>) provides the spelled-out logical operators
far more readable than single-character operators:

TAB2(C operator, iso646.h spelled-out macro)
TRO2(AMP()AMP(), and)
TRO2(AMP()=,     and_eq)
TRO2(AMP(),      bitand)
TRO2(|,          bitor)
TRO2(~,          compl)
TRO2(!,          not)
TRO2(!=,         not_eq)
TRO2(||,         or)
TRO2(|=,         or_eq)
TRO2(^,          xor)
TRO2(^=,         xor_eq)
TABCAP(Relatively unreadable C operators vs. respective iso646.h macros)

P()
Even though it's atypical, I'm tempted to use these in my projects.
</details>

SECTION2(wrapping-up, Wrapping Up)

P()
Here's the final code for TPK algorithm.
It compiles under Clang 13.0.1 on my CD(x86_64-unknown-linux-gnu) 😵
(the exact command is CD(clang tpk.c -trigraphs -lm).)

PRE(c)
%:include LT()math.h>;
??=include LT()stdio.h>;

double
f(t)
double t;
??LT() return
    sqrt (fabs (t))
    + 5
    * pow (t
           , 3); %GT()

void
read_nums(a, i)
double *a;
int i;
LT()%  if (i == 11)
    LT()%  return; %GT()
    else
    ??LT() scanf ("%lf"
                 , AMP()iLT():a??));
        read_nums (a
                   , ++i);%GT() %GT()


void
print_nums(a, i)
double *a;
int i;
LT()%   double y;
     (i LT() 0)
     ? 0
     : (y = f (i??(a:GT())
        , (y GT() 400
           ? printf ("%d TOO LARGE\n"
                     , i)
           : printf ("%d %.16g\n"
                     , i, y))
        , print_nums (a
                      , --i)); ??GT()

void tpk ()
 ??LT() double a LT():11:GT() = ??LT()0??GT()
           , y;
    read_nums (a
               , 0);
    print_nums (a
                , 10);
    /* Absolutely unnecessary, but irritating. */
    return; %GT()

int main ()
LT()%  tpk();
    return 0; ??GT()
PRECAP(Final code with all the ugly gotchas above applied)

P()
If you want some job security as a C or C++ programmer,
you might use some of the things discussed above.
But in any other scenario: you don't want to write code this way!
Be kind to each other, even when y'all write chthonic C code.

P()
Update: some commenters on Reddit mentioned
A(https://ioccc.org/, IOCCC)
as an additional inspiration and further research direction.
This post is by no means exhaustive,
and you will likely find much more gory details if you explore IOCCC.

P()
Another update:
A(https://www.reddit.com/user/insanelygreat/, u/insanelygreat)
shared an absolutely horrendous piece of code
and set of macros that turn C into something BASIC.
Here's a small piece of code from their
A(https://www.reddit.com/r/C_Programming/comments/17sfc4a/comment/k8q39ij/, comment you should read in full:)

PRE(sh)
/* check for meta chars */
BEGIN
   REG BOOL slash; slash=0;
   WHILE !fngchar(*cs)
   DO   IF *cs++==0
        THEN    IF rflg ANDF slash THEN break; ELSE return(0) FI
        ELIF *cs=='/'
        THEN    slash++;
        FI
   OD
END
PRECAP(Heavily macro-infused piece of C code that looks like shouting in Ada (citing u/insanelygreat))

#include "template/feedback"
#include "template/footer"
