#define TITLE Parameterized Procedures for Testing, Mocking, Plumbing
#define DESCRIPTION "It's often the case that a functional (Scheme?) codebase needs to plug something into the computation. \
Here's one way to do that with almost no syntactic overhead."
#define IMAGE assets/parameterized.png
#define IMAGE_ALT "A jarring black-and-white poster. \
On it, 'parameterize' and 'procedures' are written in dark orange pumpkin color. \
Between them (below 'parameterize'), there's a set of parentheses and 'd', \
hinting that a word 'parameterized' results from these.\
There's an all-caps 'AARTAKA.ME' in the corner."
#define LIGHT_COLOR white
#define LIGHT_ACCENT_COLOR HASH()177e89
#define DARK_ACCENT_COLOR HASH()bd5f00

#include "template/header"

IMG(IMAGE, IMAGE_ALT)

P()
I'm about to suggest a simple solution to the problem of

ULI Mocking a procedure for testing.
LI  Inserting pre-computed results into computation.
LI  Temporarily overriding a procedure.
END(UL)

P()
From this list, it's more or less evident why one might need this.
But I'll explain my context anyway:

SECTION2(problem, The Problem)

P()
Biological computations are fun:

ULI They are hard to read on paper.
LI They are even harder to read in code.
LI And they take an awful lot of resources to compute.
END(UL)

P()
So I ran one computation piece that took several hours.
It wasn't nice waiting for so long.
(Even though I got some delicious cherry ice cream while waiting for it.)
I've sworn to optimize it, or otherwise make it bearable to debug.

P()
There was this one function, CD(mph-em), that was taking most of the time.
Especially so on first run.
It was an important one and there were no corners to cut.
I tried:

ULI Memoization/caching.
LI  Micro-optimizations.
LI  Rewriting parts of it in C.
END(UL)

P()
It wasn't enough.
I needed to somehow avoid the long-running computation altogether.
And just get the result.
I needed to pre-compute the results of CD(mph-em) and plug them into the program.
But what if it's too deep into the program and I can't feasibly pass anything from outside?

SECTION2(solution, The Solution: Parameterize)

P()
Lisps have a long tradition of dynamic vs. lexical binding wars.
Lexical bindings won, but there are uses for dynamic bindings.
Like temporary procedure/value overriding!
So I reached for Scheme's take on dynamic bindings,
A(https://srfi.schemers.org/srfi-39/, SRFI-39 Parameter objects)
and came up with this macro:

PRE()
;; Define a NAMEd procedure and PARAMETER-NAMEd parameter
;; variable. Proceeds with running BODY with ARGS when PARAMETER-NAMEd
;; variable is #false. When PARAMETER-NAME is `parameterize'd to a new
;; procedure, call this procedure on ARGS instead. Useful to override
;; a procedure (like replacing the results for testing or providing
;; shortcut data for long-running computation.)
(define-syntax define-parameterized
  (syntax-rules ()
    ((_ ((name parameter-name) . args) body ...)
     (begin
       (define parameter-name (make-parameter HASH()f))
       (define (name . rest)
         (apply (or (parameter-name)
                    (lambda args body ...))
                rest))))
    ((_ (name . args) body ...)
     (define (name . args) body ...))
    ((_ name value)
     (define name (make-parameter value)))))
PRECAP(define-parameterized macro allowing to easily override a procedure/variable temporarily)

P()
So now I can CD(define-parameterized) my CD(mph-em) and plug (via CD(parameterize)) it in whenever I need it:

PRE()
(define-parameterized ((mph-em %mph-em) reml? eval x y vg ve b)
  HASH()|...|HASH())
(parameterize ((%mph-em (lambda args (mtx:alloc HASH()(HASH()|...|HASH())))))
  (code-calling-mph-em))
;; Or, if you only need it to fire once
(parameterize ((%mph-em (lambda args
                          (%mph-em #f)
                          (mtx:alloc HASH()(HASH()|...|HASH())))))
  (code-calling-mph-em))
PRECAP(Use of define-parameterized)

P()
I was unsure whether this post should've been written altogether.
It's quite obvious, right?
But it might be just my knowledge bias, so here goes nothing!
Tell me whether it's obvious or not, in the feedback form below ↓

P()
And hey, give this macro a go!
SRFI 39 is the only dependency and
A(https://practical-scheme.net/wiliki/schemexref.cgi?SRFI-39, is pretty well supported),
so no trouble in trying!

#include "template/feedback"
#include "template/footer"
