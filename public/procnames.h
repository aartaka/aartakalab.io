#define TITLE Procnames Start Lines. But Why?
#define DESCRIPTION "Putting procedure names at column 0 is a widespread convention in C codebases. \
But the exact reasons are not really elaborated anywhere. \
We just do that. \
So I decided to make my own summary for why it's a thing."
#define IMAGE assets/procnames.png
#define IMAGE_ALT "Soft beige thumbnail with green text split over multiple lines. \
First line says 'void', \
Second says 'procname()', \
and the third is '{}'. \
This is mimicking a style of putting C procedure/function names on their own lines. \
'aartaka.me' and 'Artyom Bologov' are added in the corners, also split over multiple lines."
#define LIGHT_COLOR HASH()f5ead8
#define DARK_COLOR HASH()131417
#define LIGHT_ACCENT_COLOR HASH()615e39
#define DARK_ACCENT_COLOR HASH()ff5e13

#include "template/header"

PIC(IMAGE, assets/procnames-dark.png, IMAGE_ALT)

P()
Looking at some old Open Source C codebase
(everyone does that, right?)
one can notice a weird convention.
Procedure names and return types are split on two separate lines:

PRE(c)
char *
hello(char *you)
{
// ...
}
PRECAP(Weird convention, right?)

P()
Pros of this style are

OLI It's easy to find procedure definition with
CD(grep): just search for CD(/^procname/).
LI  It saves some space for argument list.
Especially when the return type includes qualifiers like CD(static) and CD(const).
Or something like CD(struct long_struct_name *)
LI  K&R did that.
That's the reason someone on StackOverflow provided, but it's wrong.
"The C Programming Language" had return type on the same line as procedure name.
But! It also had CD(main()) starting the line, because it was possible to omit the "obvious" CD(int) return type.
That might be where the confusion started from.
END(OL)

P()
Another reason, provided by Žarko Asen over email:

BQ()return type, procedure qualifications, template characteristics go on the line above the procedure's name its easier to distinguish the procedure name from the code when it resides in the beginning of a new line
BQCAP(Highlighting the visual recognition this style gives (text preserved verbatim))

P()
Relatively obvious cons are:

OLI It splits return type and name on separate lines.
Looking up full prototype of the procedure is harder—one needs to open the file and look at the line above the name.
LI  It likely doesn't match the aesthetic preferences of most programmers and languages.
END(OL)

P()
What I'm about to describe is a well-established synthesis of the two conventions: split and joined.
This synthesis is:

ULI Let procnames start lines in source (.c) files.
LI  But list the whole procedure prototype on one line in header (.h) files.
END(UL)

P()
This way, procedure prototype is easy to search for (look for .h files),
while the actual procedure source is equally easy to locate (CD(/^procname/)).

P()
Not forcing that on anyone, of course.
But... does it make any sense now?

#include "template/feedback"
#include "template/footer"
