#define TITLE Three Minutes Writing
#define DESCRIPTION "We're all suffering with attention deficit. So let's finally write like we do."
#define IMAGE assets/3mins.png
#define IMAGE_ALT "A light (pale skin colored?) thumbnail. \
On it, two clock arrows for an acute angle. \
Near the ends of these arrows, '3 mins' is written. \
Near the mount point of these, 'Writing' is added. \
The first 'i' in 'Writing' is consisting of these arrows, and the second one is another arrow. \
In the corners, 'aartaka.me' and 'Artyom Bologov' are added."
#define LIGHT_COLOR HASH()f5e0e2
#define DARK_COLOR HASH()271116
#define LIGHT_ACCENT_COLOR HASH()3b6da8
#define DARK_ACCENT_COLOR HASH()bf07e3

#include "template/header"

PIC(IMAGE, assets/3mins-dark.png, IMAGE_ALT)

P()
I just finished watching second season of Arcane.
One of the focal moments was this Silco's monologue.
Delivered to Jinx when she's sitting in a jail and going (even more) insane.

BQ() Killing is a cycle. One that started long before Vander and me. And it will continue long after the two of you. [...] We build our own prisons. Bars forged of oaths, codes, commitments. Walls of self-doubt and accepted limitation. We inhabit these cells, these identities, and call them "us." I thought I could break free by eliminating those I deemed my jailors. But... Jinx... I think the cycle only ends when you find the will to walk away.
BQCAP(Silco's monologue, Arcane season 2 episode 8)

P()
It wasn't focal because it was a good monologue (it wasn't one.)
Or because it coincided with episode name/topic (episode name was mostly meaningless.)
It was focal because I realized: I can't understand it.
I had to read it as text to digest it.
Otherwise it was just white noise to me.

P()
As I've already said,
AHERE(scrollbar, I need scrollbars to allocate attention for reading).
My attention deficit is that bad.
Yours is likely close to that.
That's why we all need Three Minutes Writing.

P()
As a writer, my goal is to deliver the idea.
In a form I myself can understand later.
Three Minutes Writing means:

ULI Write small paragraphs and sections.
LI  Add visual anchors:
 ULI sections,
 LI  lists,
 LI  code listings,
 LI  quotes.
 END(UL)
LI  Keep the language simple.
LI  Keep the scrollbar big.
LI  Optimize for three minutes of reading time!
END(UL)

P()
My
AHERE(hypertext, longer-form)
AHERE(prose-vs-tweet, writing)
fails at these, and I often get comments like

BQ() So chaotic writing. I try to distill the essence from this article, but it is jumping back and forth between different topics
BQCAP(u/Revolutionary_Ad7262 on Printf is Useless)

P()
My sweet person, I can't focus on it either.
Sorry.

P()
So let's all focus on Three Minutes Writing instead.
We need to understand each other, right?

P()
See also
A(https://www.todepond.com/wikiblogarden/academia/style/two-beat/, Two beat style by Lu Wilson).

#include "template/feedback"
#include "template/footer"
