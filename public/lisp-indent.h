#define TITLE All Lisp Indentation Schemes Are Ugly
#define DESCRIPTION "Indentation styles are a hot topic in every language. \
So let's see how Lisps do indentation! \
Uuuuuuugh!"
#define IMAGE assets/lisp-indent.png
#define IMAGE_ALT "Pinky beige thumbnail with 'UGLY LISP INDENT' in huge letters. \
Every word is indented with a return char '⮑'. \
In the right top corner, 'Artyom aartaka.me Bologov' is written in likewise indented way."
#define LIGHT_COLOR HASH()d6c9cc
#define DARK_COLOR HASH()080b19
#define LIGHT_ACCENT_COLOR HASH()4e3824
#define DARK_ACCENT_COLOR HASH()455949

#include "template/header"

PIC(IMAGE, assets/lisp-indent-dark.png, IMAGE_ALT)

P()
Once you get used to Lisp, you stop noticing the parentheses and rely on indentation instead.
That's partially why there are several alternative syntaxes based solely on indentation.
AHERE(wisp, Like Wisp),
A(https://srfi.schemers.org/srfi-110/, or sweet expressions).
But then, the question stands: how to indent the code, actually?
Especially so—in Lispy syntax.

SECTION2(no-indent, No Indent: One Can Only Go So Far)

P()
A solution that will likely satisfy a proponent of any indentation style:
"Just put it all on one line lol."
Of course, lines are not infinitely readable and there's a column cap,
(whether natural or enforced.)
So this line (adapted from
A(https://github.com/aartaka/cl-blc, cl-blc)),
while devoid of indentation problems, is unreadable:

PRE(lisp)
(list (tree-transform-if predicate transformer (first tree) depth) (tree-transform-if predicate transformer (second tree) depth))
PRECAP(Absurdly long line of a Lisp code)

P()
That's the problem statement: some forms need multiple lines and indentation.
But what kind of indentation?

SECTION2(function-indent, (Dis)Functional Aligned Indent)

P()
There's an established style of indentation: align the function arguments on the same column:

PRE(lisp)
(list (tree-transform-if predicate transformer (first tree) depth)
      (tree-transform-if predicate transformer (second tree) depth))
(inc! d (/ (* (mtx:get x i k)
              (mtx:get x j k))
           (1+ (* (vec:get dl l)
                  (vec:get eval k)))))
(let ((s (if (< j i) j i))
      (l (if (< j i) i j)))
  (+ (* s 1/2 (- (* 2 d-size) (1+ s)))
     l
     (- s)))
PRECAP(Examples of function-like indentation)

P()
This style if useful in reflecting the code structure: just look at what's indented and what's outdented.
It works especially well for short function/macro/form names, like CD(+) or CD(vec:get).
Not so well for long ones:

PRE(lisp)
(tree-transform-if predicate
                   transformer
                   (second tree)
                   depth)
PRECAP(A problematic function indentation)

P()
19!
19 spaces of indentation!
It's getting unruly.
Such an indent, when used in deeply nested code, makes it too wide and unreadable.
If you add the strict one-per-line alignment of arguments, it's also painfully long line-wise.
Let's handle the verticality first:

SECTION2(filling-indent, Space-filling Indent)

P()
No sane Lisper would write a CD(loop) with every keyword on its own line:

PRE(lisp)
(loop for
      i
      below
      10
      collect
      i)
PRECAP(Ugly loop)

P()
(Some pretty-printers do that too (I'm looking at you, ECL!), but that's a topic for another day.)

P()
We don't have to put every argument on its own line.
That's the intuition behind the space-filling indent:

PRE(lisp)
(loop for i below 10
      collect i)
PRECAP(Less ugly loop)

P()
One can go as far as splitting the argument list in arbitrary places.
Putting some keyword arguments on the first line, and then some on the second/third/etc.
This utilizes the space efficiently enough to be used.
But what if one's stuck really deep in nesting levels?

SECTION2(macro-indent, Sick Macro Indent)

P()
Now what I'm about to suggest is likely not to your taste:

PRE(lisp)
(tree-transform-if
 predicate transformer (second tree) depth)
PRECAP(My indentation style suited for deeply nested code)

P()
This style of indentation
(putting function name on one line, and arguments on the other)
was frowned upon more than once in my practice.
But what it achieves is perfect indentation control.
You only get one space of indentation per form.
Complex algorithms are easier to read when written in this style.

P()
And!
This style also plays well with most indentation tools, even the simplest ones.
I had this situation more than once:

ULI Writing a CD(with-*) macro in Scheme;
LI  Using it;
LI  And realizing that Geiser's indentation functions think that this macro is a procedure.
END(UL)

P()
Indenting all the arguments of the CD(progn)/CD(begin)-like macro in this sick style helps:

PRE(scheme)
;; From
(mtx:with-column (uab-col uab index-ab)
                 (mtx:set!
                  ppab 0 index-ab
                  (blas:dot hi-hi-eval uab-col)))
;; To
(mtx:with-column
 (uab-col uab index-ab)
 (mtx:set!
  ppab 0 index-ab
  (blas:dot hi-hi-eval uab-col)))
PRECAP(Sick indent helps you to manage macros)

SECTION2(favorite, What Is Your Favorite Style?)

P()
I often prefer the macro-like indentation, because I sometimes write deeply nested code.
But I see the value in all the other indentation schemes!

#include "template/feedback"
#include "template/footer"
