#define TITLE Falsehoods Programmers Believe About HTML
#define DESCRIPTION "There are many things programmers believe about HTML that are not necessarily true. Here're some."
#define IMAGE assets/thumbnail-551A8B-D1D9EC-000000.png
#define LIGHT_COLOR white
#define LIGHT_ACCENT_COLOR HASH()6b4ba1
#define DARK_ACCENT_COLOR HASH()3366cc

#include "template/header"

P()
Web is beautiful.
Web is ugly.
Web is astonishing.
A part of this appeal is HTML, with its historical quirks.
Many a programmer believe many things about HTML.
And some of the beliefs are not necessarily true.
So let's explore some falsehoods programmers believe about HTML.

SECTION2(language, Language AMP() Parsing)

DETAILS(HTML is just XML. All tags have matching closing tags.)
Some tags (like CD(LT()li>) or CD(LT()p>)) have implicit closing tags:
PRE(html)
LT()li> List item without closing tag
LT()li> Another list item right after it
PRECAP(Example of implicit closing tags in CD(LT()li>) tag)
END(DETAILS)

DETAILS(HTML is almost XML. All tags have closing tags, even if implicit)
CD(LT()img>) and CD(LT()input>) are self-closing:
PRE(html)
LT()!-- Notice the / here!-->
LT()input type=text/>
PRECAP(Self-closing CD(LT()input>) tag)
END(DETAILS)

DETAILS(Okay, okay, HTML is not XML. But all elements either have closing tags or self-close)
CD(LT()br>) and CD(LT()hr>) don't even need a self-close slash.

P()
Actually, self-close slash
A(https://jakearchibald.com/2023/against-self-closing-tags-in-html/, is mostly optional (and discouraged) in HTML),
so the difference is less pronounced.
END(DETAILS)

SECTION2(standard, Standard)

DETAILS(HTML is defined by the standard)
It's defined by browser vendors and WHATWG (= browser vendors)
END(DETAILS)

DETAILS(The standard does not change after validation)
The standard is "Living", and you can see (a very recent) date of last change at
A(https://html.spec.whatwg.org/, Living Standard page.)
END(DETAILS)

DETAILS(The standard is self-contained (relating to HTML only))
HTML is also relating to a group of standards, including DOM and JavaScript.
In fact, many features of HTML are defined as JavaScript classes.
END(DETAILS)

DETAILS(There is only one (two? three?) doctypes for HTML documents)
Oh my sweet summer child...
PRE(xml)
LT()!DOCTYPE html>
LT()!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML//EN">
LT()!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
LT()!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
LT()!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
LT()!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
LT()!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
LT()!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
LT()!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
LT()!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.0//EN" "http://www.w3.org/TR/xhtml-basic/xhtml-basic10.dtd">
LT()!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
LT()!DOCTYPE math PUBLIC "-//W3C//DTD MathML 2.0//EN" "http://www.w3.org/Math/DTD/mathml2/mathml2.dtd">
LT()!DOCTYPE math SYSTEM "http://www.w3.org/Math/DTD/mathml1/mathml.dtd">
PRECAP(Many (not all) HTML doctypes)
END(DETAILS)

SECTION2(practices, Practices)

ULI All websites follow the standard.
LI No one uses HTML4.
LI No one uses HTML3 anymore.
LI No one uses HTML2 anymore.
LI No one uses HTML1 anymore.
LI No one uses tables for markup anymore.
END(UL)

DETAILS(No one uses XHTML)
ePub, a widespread ebook format, uses XHTML for content markup. It sucks, but it's a practice.
END(DETAILS)

SECTION2(runtime, Runtime)

DETAILS(Modifying DOM is slow)
React propaganda is probably to blame for this illusion.
DOM is the most optimized data structure out there.
Whatever you put in it—it'll sustain.
React will not.
END(DETAILS)

DETAILS(Browsers are just messy HTML parsers)
Browsers are JS evaluators.
Browsers are layout engines.
Browsers are computer graphics toolkits (WebGL and fonts).
Browsers are OSes (they have file system interfaces, audio output, and many other APIs).
END(DETAILS)

DETAILS(SEO is hard and you need frameworks for it)
Not really if you write simple semantic HTML.
Because it's easy to parse and index, especially compared to JS-generated markup.
END(DETAILS)

DETAILS(WebAssembly will deprecate HTML and JS)
These are different niches.
You can't really make accessible websites with WebAssembly.
So if you want universal pages openable everywhere, you have to stick with HTML etc.
END(DETAILS)

DETAILS(HTML is not Turing-complete)
A(https://github.com/efoxepstein/stupid-machines, It is, given CSS and user input.)
END(DETAILS)

SECTION2(did-i-forget, Did I Forget Any?)

P()
In case you haven't found your favorite falsehood, feel free to suggest more!
This post will likely be on Reddit and Hacker News, so use comments there.
Or use the contacts from the
AHERE(about, About page!)

#include "template/feedback"
#include "template/footer"
