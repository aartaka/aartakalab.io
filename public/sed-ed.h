#define TITLE s/sed/ed
#define DESCRIPTION "ed is a stupid simple text editor. \
sed is a nice streaming text processing tool. \
Why would one even want to use ed for anything, let alone for text processing if there's sed?"
#define IMAGE assets/sed-ed.png
#define IMAGE_ALT "Digital thumbnail with handwritten text on it. \
The text in the center says 's/sed/ed'. \
Asubstitution command seplacing 'sed' with 'ed' in the given text. \
In the corners, 'a Artyom Bologov .' and 'a aartaka.me .' are written in dark-red curvy letters. \
These are mimicking the append command from ed, adding 'Artyom Bologov' and 'aartaka.me' to the text."
#define LIGHT_COLOR white
#define DARK_COLOR HASH()444444
#define LIGHT_ACCENT_COLOR HASH()660000
#define DARK_ACCENT_COLOR HASH()ffcccc

#include "template/header"

IMG(IMAGE, IMAGE_ALT)

P()
This post starts with holding a grudge:

BQ()Posix regular expressions are extremely hard to get wrong? Uh... Have you really written any? Sounds like you might not really know either Posix or PCRE.
BQCAP(u/bigmell in reply to 5 (Wrong) Regex To Parse Parentheses)

P()
Yes, I've written some regular expressions.
AHERE(https://github.com/aartaka/modal.ed, I made a compiler from Modal to ed, made as an ed script.)
AHERE(scripts/wisp.ed, I implemented Wisp syntax preprocessor for Scheme, the only Wisp implementation besides the reference by Arne Babenhauserheide.)
A(https://rosettacode.org/wiki/Category:Ed, Most of 100 RosettaCode entries for ed are completed by me, and I created ed category in the first place.)

P()
... come again? ed?????
Why are you using such an antiquated editor?
Why are you writing scripts for it?
It wasn't even intended for scripting, right?
There's sed for scripting, so just use sed!

P()
No.
I stay with ed, because it is

ULI Simple (yes, including regex.
AID(https://manpage.me/?q=ed, REGULAR_EXPRESSIONS, BRE specification fits on two screens of text)).
LI  Feature-complete.
LI  More powerful than sed.
END(UL)

P()
But I guess I have to list all the things that sed misses and ed has.
If only to convince myself that I need ed after all.
So...

SECTION2(interactive, Interactive: I Want to Test Things Out)

P()
The difference between ed and sed is akin to the interpreted/compiled language difference.
You have to run the whole sed script to see the result.
While with ed, you can fool around, test regex, and CD(u)ndo them.
I use ed as a regex playground before integrating the regex into my projects, like
A(https://github.com/aartaka/reb, Reb, my regex-heavy Brainfuck toolkit.)

P()
I think this problem is less acknowledged than it should be.
Likely because sed is implied to be the a smart one-liner tool.
One substitution and we're done.
Easy to iterate on, but gets increasingly hard as commands/substitutions accumulate.
And no one stays with sed (and ed as an "obvious" "equivalent" of sed) after things get complicated.
Most just switch to awk or something.

P()
A shame—ed might have worked well enough and would certainly be easier to iterate on.

SECTION2(multiline, Multiline: Because Most Inputs Are)

P()
Many of my scripts are working with multiline input.
A pattern that I keep using is:

ULI Add a line marker to (the end of) every line.
LI  Join lines.
LI  Operate on these lines with "multiline" regex.
LI  And split the lines back out.
END(UL)

PRE(sed)
# Join lines with semicolons (there are none left)
# Also add one semicolon to the front (useful in wrapping forms below)
g/$/s//;/
,j
s/^/;/
PRECAP(Example of line joining commands)

DETAILS(But why join if you can use multiline regex feature?)
Because this feature is not portable.
I know this is premature, but I want my scripts to be runnable in most ed versions out there.
Including BSD ed, Mac ed, and...
A(https://merveilles.town/@aartaka/113432052692702413, they work in ex too!)
This portability point especially applies to scripts on RosettaCode—too many eyes on them to fail.
END(DETAILS)

P()
Reproducing this "multiline" behavior in sed means using the forbidden magic:
repeatedly reading new lines and (inevitably) using multiline regex.
Much harder to understand and only covered by the
AID(https://www.gnu.org/software/sed/manual/sed.html, Multiline-techniques, "Advanced" section of the manual), in passing.
Compared to basic commands ed has for these: CD(s) and CD(j).

SECTION2(stateful, Stateful and Chaotic: All Over the Place)

P()
The fact that ed works with files and buffers allows for some flexibility.
One can overwrite files, re-read them, run shell commands, move lines around, and undo anything.
AID(,interactive, Especially so—interactively.)

P()
Some of my scripts
(like the unreleased Binary Lambda Calculus scripts)
are working on files, and overwriting the contents when ready.
Because files are a nice way to retain state between script runs.
And sed, being a streaming editor, is missing out on state.
Yes, you can redirect the output to file or use CD(w) command to append to files.
But that's an afterthought, not a fundamental feature.

SECTION2(ed-perfect, So, is ed perfect then?)

P()
No.
Here are some quality of life features from sed that I miss:

DL(-f for script files)
It's relatively annoying to have to
CD(cat) and pipe files into ed, but nothing critical.
DD(y command for transcription)
My ed script for Armenian transliteration is 30 lines longer than respective sed one.
CD(y) command is rarely useful, but when it is there's no ed command for this.
I have to substitute every char individually.
DD(looping and branching)
Now this one is a killer feature—I can run scripts until the file is ready!
I can achieve this with ed by overwriting files in a Bash CD(for) loop.
Or by copy-pasting the commands multiple times in the script file.
But still.
No ed-native way.
END(DL)

P()
As you can see, there are not many features I miss.
And it's nothing compared to ed's killer features!

SECTION2(use, Use Ed!)

P()
I mean, no, you don't have to.
It's unlikely that you (like me) are into sticking with obscure tech just because it's fun.
And then discovering that it's more powerful than expected/mainstream.

P()
But still, the next time you use sed or awk, think:
This one-liner could've been an ed script.
A more stateful yet clear and powerful script.

#include "template/feedback"
#include "template/footer"
