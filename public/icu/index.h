#define TITLE I C \U Study Sessions

#include "template/header"

P()
I C \U (I See You) sessions are short sessions where we learn programming together.
My (Artyom Bologov, C programmer) goal is to make sure you understand basics of programming.
If you learn C to the level of relatively independent programming, I consider it a success.
(I am also using these sessions to practice as an instructor, moderator, and programmer.
So bear in mind we're all guinea pigs and we're in this together!)

SECTION2(prerequisites, Prerequisites)

P()
Please install C compiler (program that will make your code runnable.)
If you're on Windows, install MSVC (Microsoft Visual C) and Visual Studio.
In you're on Mac, install Xcode and Clang.
If you're feeling adventurous, are on Linux, and/or want nicer experience, install Clang.

SECTION2(c-basics, C Basics)

P()
We'll start with C and will work with it for a certain set of sessions.
This is to not confuse anyone with several different languages and ways of doing things.
One language is enough to make one confused already, so let's pick C.
In addition to that, switching to Pharo/Smalltalk after C will be a relief, so let's suffer first!

P()
Here's the structure I'm suggesting:

DL(Basic Types and Values)
This session will be an introduction to the idea of types.
We'll see what kinds of things there can exist in C and what we can do with them.
As the highlight, we'll learn to print/output/display them
with CD(printf) function.
AHERE(types-values, Here's a link to session notes.)

DD(Control Structures)
Programming is not only about typed data, it's also about instructing the computer.
What to do, when, how, and how many times.
We can do these via "control flow" or "control structures".
These change how data flows through the program, thus the name.
AHERE(control-structures, Here's a link to session notes.)

DD(Aggregate/Compound Types)
This session will be about more complex (as in manifold and multi-part) types.
Structures and arrays, in particular.
We'll learn to put the basic things from the previous session together and taking them out of there.

DD(Functions)
We already know how to put data together—via structs and arrays.
But we don't yet know how to stitch together the instructions we force the computer to do.
That's what functions/procedures/subroutines do—stucture and put the code together.

DD(Bytes and Sizes)
Computers are boring and obscure in how they need their data served.
They need it properly sized and packed in uniform batches.
Bytes, kilobytes, megabytes, gigabytes etc.
All the data we work with in our programs has size and we'll figure out how to get and use it.
And we'll be more ecologically conscious after this session—the smaller the code/data, the "greener" it is!

DD(Pointers and Memory)
A topic even most professional programmers fear, actually.
We'll play with pointers (locations of data in memory) and "dynamic" memory.
Which means getting more (sized) space for the things we need to store.
And giving it back when we don't need it anymore.

DD(Headers/libraries/files/projects)
Data is combined in structures.
Code is combined in functions.
But we need to somehow combine both data, structures, and functions somewhere.
To reuse and share it as files.
We'll learn how files and projects are usually structured when programming in C.

DD(C Preprocessor and Macros)
C Preprocessor is a (relatively) powerful system for code generation.
It's when you don't want to type all the repetitive parts and you make the computer write it instead.
With Preprocessor, you can hide and reuse some ugly and awkward pieces of code behind "macros".
But it is special in how it does many things and needs some getting used to.

DD(String library)
C strings are misleading—they look like simple pieces of text.
But they are raw arrays without any useful information about the text they contain.
Strings need some more code to make it bearable working with them.
Like CD(string.h) header.

DD(Input/Output)
We already know about CD(printf), but it's actually a part of a larger library
called CD(stdio.h) (standard input/output.)
The idea of input and output needs some review too—it's the raw basics of interaction with computer.
END(DL)

#include "template/footer"
