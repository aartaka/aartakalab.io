#define TITLE I C \U: Types and Values

#include "template/header"

P()
There are several concepts that are quite fundamental to programming.
Some of them are:

ULI Types.
LI  Values.
LI  Variables.
LI  Functions.
LI  Libraries.
LI  etc.
END(UL)

P()
We will cover them all eventually, but today will be about the first three: types, values, and variables.

SECTION2(setup, Setup)

P()
First, some setup.
Here's a program we will be modifying to play with values and types:

PRE()
HASH()include LT()stdio.h>
HASH()include LT()stdbool.h>
HASH()include LT()iso646.h>

int main()
{
        // Put your code here:
        return 0;
}
PRECAP(Example code to try things on)

DETAILS(What are these // slashes?)
These things, used across these notes, are code comments.
They don't do anything themselves, they just allow to add context.
END(DETAILS)

P()
Copy-paste it into a separate file (say, test.c) and try compiling it with a C compiler you have.
It's either
CD(clang test.c) or CD(gcc test.c).
After that, you can run the resulting file (CD(a.out)) as:

PRE()
./a.out
PRECAP(Running the compiled file)

P()
It's not showing anything, but at least is compiles and runs.
Which is already quite a lucky coincidence for a C program.

SECTION2(basic-types, Basic Types)

P()
So, variables.
Variables are a way to attach names to all the things you want to store.
Marked boxes for thing.
This is how creating a variable looks in C:

PRE(c id=variable-template)
type name = value;
PRECAP(Template for a new variable)

P()
Notice the equal sign (it'll matter later on) and semicolon.
Equal sign means you put a thing (data/CD(value)) into the CD(name).

P()
One thing that needs explanation (besides everything else) is CD(type).
Types are a dark turn in the programming history.
Types restrict what you can put into the variable.
They were initially introduced so that numbers and other things don't take up too much memory.
And then everyone just decided they need to stay.
Okay, enough rant, time for some types!

P(id=boolean)
Probably the simplest type is CD(bool)ean.
A type to ascertain the truth of something.
It is either CD(true) or CD(false).
In computing, it's often used to change the computation based on whether something is true or not.

PRE()
bool am_i_a_girl = false;
bool i_am_gonna_die_someday = true;
PRECAP(Examples of boolean variables)

P()
Then there are boolean operators that are made exclusively to work with booleans.
Here's a summary:

TAB2(Operator, Meaning)
TRO2(and/&&,   Only is CD(true) when both sides are CD(true). CD(false) otherwise.)
TRO2(or/||,    Is CD(true) when any of its sides are CD(true). CD(false) otherwise.)
TRO2(not/!,    Is CD(true) when the thing to the right of it is CD(false). CD(false) otherwise.)
TABCAP(Logical operators and their meanings)

DETAILS(Wait, what are operators?)
Operators are things you can usually put between two pieces of data (binary/two-place operators.)
Or something you can put before/after the data (prefix/suffix unary/one-place operators.)
They do something to the data they are passed and give the result back to you.
END(DETAILS)

P()
Using these, we can mix and match the booleans and see what comes out:

PRE()
bool a_girl_is_going_to_die = am_i_a_girl and i_am_gonna_die_someday;
bool a_non_girl_is_immortal = not am_i_a_girl and not i_am_gonna_die_someday;
bool maybe_girl_maybe_mortal = am_i_a_girl or i_am_gonna_die_someday;
PRECAP(Operating on booleans)

P()
Another important type is CD(int)eger.
A type of numbers that contains countable quantities.
0, 1, 2, 3 etc. and -1, -2, -3 etc.

PRE()
int answer = 42;
int three = 3;
PRECAP(Examples of integers)

P()
Integers are useful because one can do math on them.
And mathematical operations are really frequent in programming.
For example, all the computer graphics is multiplications and additions of matrices (don't ask me.)
Here are some operations you can do on integers:

PRE()
int sum = answer + three; // 45
int difference = answer - three; // 39
int product = answer * three; // 126
int quotinent = answer / three; // 14
int remainder = andswer % three; // 0
PRECAP(Integer operators)

P()
These operators are just a symbol soup, so you'd have to remember them.
Luckily, this comes with the advantage of most programming languages using them.
It's a symbol soup everyone does.

P()
Now, what if we want to represent something that isn't integral?
Like CD(1.5).
Floats and doubles!

DETAILS(Wait, what is float?)
It stands for "floating-point number".
Roughly meaning that there's a point.
There's a multi-digit number before and after it.
And there might be any number of digits before and after it.
So it kind of floats between two numbers, never certain about how many digits are before or after.
END(DETAILS)

PRE()
float a_freq = 440.0;
// Or a more established practice—using doubles:
double a_freq = 440.0;
double a_alt_freq = 442.0;
PRECAP(Examples of floats)

P()
Another type of number operators (common for both CD(int)s and floats) are comparison ones.
These take two numbers and tell whether they are the same or not, and in which way.

TAB2(Operator, Meaning)
TRO2(==,       Equality, whether the numbers are the same.)
TRO2(!=,       Inequality, whether the numbers are different.)
TRO2(<,        Whether the number on the left is less than the one on the right.)
TRO2(>,        Reverse of the CD(<), whether left is greater than right.)
TRO2(<=,       Whether the number on the left is less than or equal to the one on the right.)
TRO2(>=,       Left one is greater than or equal  to the right one.)
TABCAP(Comparison operators)

P()
Another (slightly more useful) type is strings.
Arbitrary text you can display on the screen (ooops, spoilers!)
And read it kind of aloud.

PRE()
char *name = "Artyom";
char *text = "1\n2\n\3";
PRECAP(Examples of strings)

DETAILS(Wait, what is char*?)
We'll cover it later, but for now it will suffice to say two things:
it's called pointer; and it means a set of things put one after another.
So a string is a sequence of CD(char)acters.
END(DETAILS)

DETAILS(What is this "\\n"?)
It's called escape sequence.
A way to represent some characters that are not easy to type in.
Like the newline character (CD('\\n')), tab (CD('\\t')), or escape (CD('\\e')).
But the most important one is newline, to move the output to next line.
END(DETAILS)

P()
Strings consist of CD(char)s.
Chars represent one symbol each.
Chars might be letters, digits, punctuation, and something else I can't remember now.

PRE()
char a = 'A';
char tab = '\t';
PRECAP(Examples of characters)

P()
Now to actually seeing the data we created:

SECTION2(displaying, Finally, Displaying The Data!)

P()
Up till now our program displayed nothing.
It was silently succeeding.
Which is not bad (remember, C is brutal and sometimes breaks in horrible ways!),
but it would be nice to know what our variables hold.
Enter CD(printf), a function to display a thing.

DETAILS(Wait, what is a function?)
Function is a piece of code that you can give some data to.
(The data is passed in parentheses and separated by commas.)
And then it does something to this data and maybe gives something back to you.
Like operator, but with a different name?
END(DETAILS)

PRE()
printf("Answer to the Ultimate Question of Life, the Universe, and Everything is %i\n", answer);
PRECAP(Printing an integer)

PRE()
printf("A4 frequency is %f (although some say it's %f.)\n", a_freq, a_alt_freq);
printf("Hello %s!\n", name);
PRECAP(Printing other types)

PRE()
printf("True is %i.\n", true);
PRECAP(Displaying a boolean)

P()
It prints CD(True is 1.)
So boolean is a lie!
It is all just CD(int)s underneath!

P()
CD(printf) has
A(https://en.cppreference.com/w/c/io/fprintf, lots of different directives (these percent strings)), most of them for numbers.
Which makes it relatively useless (I'll write about it someday.)
But still, it's the simplest and most reliable way to output something in C.

SECTION2(reading, Reading Something)

P()
Knowing how to output numbers, it's only consequential that we want to input/read something.
One function that can help with it is CD(scanf).
It's reusing the same directives (percent signs) as
CD(printf) and it also relies on strings to understand what to input.

PRE()
char *name;
scanf("%s", name);
printf("Your name is %s\n", name);
PRECAP(Reading a string with scanf)

P()
This puts a thing you input (think CD(Artyom) with a Return/Enter following it) into CD(name).
And then you can use CD(name) and output it.

P()
You can also input integers with CD(%i) or floats/doubles with CD(%f).

SECTION2(signed-unsigned-long-short, Signed, Unsigned, Long, Short, What?)

P()
There are different sizes for CD(int)s: CD(short int), CD(long int), CD(long long int).
They mostly exist to ensure that numbers fit into-computer-provided size requirements.
So, history.
It exists to shoot feet off when you use big enough numbers.
Because there's a limited set of things you can put into differently sized numbers.

P()
Another axis to add to integers is CD(signed)/CD(unsigned) distinction.
These are mostly historic too.
They exist so that some numbers can only contain positive numbers (CD(unsigned)),
or also containing negative numbers (CD(signed).)
CD(unsigned) numbers are also containing twice as many numbers,
because the size is the same but there're no longer negatives.

#include "template/footer"
