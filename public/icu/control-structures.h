#define TITLE I C \U: Control Structures

#include "template/header"

P()
Data is not all there is to computers.
Otherwise programming will (arguably) be quite boring.
We need ways to make decisions, repeat instructions, and otherwise make computer do some work.
That's where control structures step in.

SECTION2(if, if: Only Do It When I Say To)

P()
The simplest control structure is CD(if).
It only runs the block or statement that follows it when the data it is passed
(in parentheses right after CD(if))
AID(types-values, boolean, is true):

DETAILS(Wait, what is a block?)
In all of these control structures, there's a notion of a block.
The body of every control structure is either a single statement or a block.
Blocks are sets of statements wrapped in curly braces (CD({})).
If there's only one statement, braces can be omitted.
END(DETAILS)

DETAILS(Wait, what is a statement?)
Statement, to simplify things, is either AID(types-values, variable-template, variable declaration),
Or function call (like CD(printf) and CD(scanf) we already used.)
Statements usually are put on a line of their own and must have a semicolon after them:
PRE()
int statement_var = 3;
printf("I am a statement!\n");
PRECAP(Examples of statements)
END(DETAILS)

P()
Some examples of CD(if) might be:

PRE()
// The parentheses around the condition are mandatory.
if (true)
        always_runs();
// A block!
if (boolean) {
        statement1();
        int statement2 = 5;
}
// No block, semicolon replaces it.
if (maybe_useless_form);
PRECAP()

P()
Now, running something only on condition might be too simple.
We need a way to also run things when the condition is not true.
We can accomplish this with boolean operations, of course: CD(if (not boolean) {...}).
But what if we want to only run one of these?
That's what
CD(else) does:

PRE(c id=else)
if (boolean) {
        something();
} else {
        something_else();
}
PRECAP(Examples of a simple else)

P(id=else-if)
A more involved case of multiple conditions is accomplished with an CD(else if) pattern:

PRE()
if (...) {}
else if (...) {}
else if (...) {}
else () {}
PRECAP()

P()
Other languages have special control structures for that, like Lisps'
CD(cond), but C is quite barebones, so we only have this composition of CD(if) and CD(else).
And that's
all there is to CD(if/else), really.

SECTION2(while, While: Do It While You Can)

P()
Doing things once based on conditions is getting one quite far,
but what about repeating things?
We can copy-paste code, but that's harder to read and easier to make mistakes in.
The less code you have (structure-wise, not character-count-wise,) the harder it is to get it wrong.
And what if we want to repeat things indefinitely?
We can use CD(while) to repeat the following statement/block
until the condition (like in CD(if)) is false.

PRE()
while (true) {
        repeat_indefinitely();
}
int i = 0;
while (i < 10) {
        i = i + 1;
        printf("i is %i\n", i);
}
PRECAP(Examples of while)

P()
Now this last pattern with CD(i) looks quite wrong:
there's too much repetitive and intertvined code.
We must somehow make it tighter, closer, neater.
And, in fact, there's a pattern for that:

SECTION2(for, For: Ever?)

P()
The repeat-until-10 pattern can be expressed as:

PRE()
for (int i = 0; i < 10; i = i + 1) {
        printf("i is %i\n", i);
}
PRECAP(Example of counter loop (for))

P()
That's a lot to unpack, but you can see the similar pieces.
With the knowledge of CD(while), we can mark  this up into a general CD(for) structure:

PRE(c id=for-template)
for (var = ...; while_condition; var = step) {
        statements();
}
PRECAP(General template for... for)

P()
CD(for) is quite powerful with this abundance of parts:

ULI One can do multiple variable declarations in the first part.
LI  One can do arbitrarily complex conditions in the second part.
LI  One can modify any variable in whatever way they want in the third part.
END(UL)

P()
CD(for) is cursed to be the most powerful and most misused control structure.
You'll always need it.
Hopefully for simple iteration and not some cursed algorithms
(like the ones I do.)

SECTION2(special-statements, Special Statements: Break and Continue)

P()
Conditions and booleans seems to be a recurring theme to all these.
(Because we do often need to do something conditionally.)
And conditions are the point that likely accumulates the most code in all of these control structures.
There's this Russian saying, "After rain on Thursday", which means "likely never".
Such a moment may happen, though.
So what if we wanted to stop some loop after Thursday rain?
We can encode it in the loop condition:

PRE()
for(int i = 0; i < 10 and not (thursday and after_rain); i = i + 1)
PRECAP(Complicating the loop condition)

P()
What if we wanted to account for ten different combinations of days and weather conditions?
We can encode it in the conditions, but that would make it less readable.
So C has a nice shortcut statement: CD(break).
It allows one to break out of the CD(for)/CD(while) loop when they want to.
So our Thursday rain escape might look like:

PRE()
for(int i = 0; i < 10; i = i + 1) {
	if (thursday and after_rain) {
		break;
	}
}
PRECAP(Complicating the loop condition)

P()
There's also
CD(continue) (used similarly to CD(break))
to skip just one iteration, if one has to.
I rarely find use for it, but my experience may differ from yours.

SECTION2(goto, Goto: The Forbidden Art)

P()
There's a control structure/statement everyone says not to use:
CD(goto).
But it is still useful in some marginal cases.
One of these cases may be to scare programmers on Halloween night.
A goto cosplay will likely get you infinite candies if you walk around Silicon Valley.

P()
CD(goto) jumps to a certain label.
It's a statement, so it can be used anywhere a statement
(like CD(break) or CD(continue)) can be used.

DETAILS(Wait, what is a label?)
Label marks a certain line of code with a name.
Just put it at a separate line with a colon: CD(name:)
END(DETAILS)

P()
Here's how our loop-until-10 pattern
might look like with CD(goto):

PRE(c)
int i = 0;
loop:
if (i < 10) {
        i = i + 1;
        goto loop;
}
PRECAP(Example of goto use)

P()
It looks even worse than CD(while):

ULI It introduces labels.
LI  CD(goto) can be used anywhere in the code, even quite far from the label itself.
LI  It might result in really tangled code.
END(UL)

P()
That's why everyone tries to avoid it.

SECTION2(missing, Missing Control Structures)

#include "template/footer"
