#define TITLE I Am Not an AI—My Writing Is Terible
#define DESCRIPTION 'My writing is imperfect. "AI" could help. But, you know, I no longer want to hide myself. My terible writing is the only things that differentiates me from the sterile AI slop you find everywhere now.'
#define IMAGE assets/not-ai.png
#define IMAGE_ALT "A light-colored cold text thumbnail. Title reads 'I'm not AI, My writing is TERIBLE' with 'an' marked in between 'not' and 'AI'. \
'TERIBLE' (notice the typo) is written in an aggressive doodly fashion.\
'aartaka.me' and 'Artyom Bologov' in a similarly aggressive doodly letters in the corner."
#define LIGHT_COLOR HASH()CBEAFB
#define DARK_COLOR HASH()1D263B
#define LIGHT_ACCENT_COLOR HASH()5C1A1B
#define DARK_ACCENT_COLOR HASH()EB6B6D

#include "template/header"

IMG(IMAGE, IMAGE_ALT)

P()
AIs (Large Language Models, and ChatGPT in particular) with text interfaces seem to have changed the Internet.
Everywhere one goes, they find a writing that's sterile, faceless, overly polite, issue-avoidant.
Eerily resemblant of human writing, luring one in with false authenticity.
And then making one realize—halfway through the post—that what they read is "hallucinated" pretense on meaning.
AI slop.

P()
Many of my friends (many non-algophones, like me) "write" emails, posts, and essays with the help of ChatGPT.
The problem is: the writing they end up with is... AI writing.
It's characteristic.
Recognisable.
Repulsing.
Mediocre.

P()
My writing is ChatGPT-less, out of principle (I distruct the current wave of "AI".)
But I have to write things somehow, with my eroded focus and scarse vocabulary.
So I write raw drafts.
And then I use Grammarly to clean up the forgotten a/the (my arch nemesis) and make sentences flow easier.
It's good yet unobtrusive.
Premium subscription would likely clean much more things up.
Phrasing-wise and word use-wise.
But that would make my writing more... average?

P()
Thus my intention for the (medium-term) future: no AI use.
Even Grammarly, which, ostensibly, still uses Common Lisp grammar engine (ah, Lisp comrades...)
Even text summarizers for post descriptions.
Even image-generating neural networks (I draw post thumbnails myself now, all rasps implied.)
Writing dirty drafts, fixing some phrasing, and publishing them.
As is, no matter how <del>much</del> many mistakes and improperly used words there might be.

P()
Of course, practice makes perfect.
My writing (and drawing/calligraphy) will (supposedly) become better in the process of this dirty drafting.
But (also supposedly), my writing will also become more characteristic of myself.
With my anger, dark/gory stories, and awful puns.

P()
I want to remain a human(e) writer, so I'm no longer using any AI for this blog.
Come prepared.

P()
Also see:
A(https://scribe.rip/@moyosoreale/the-paul-graham-vs-nigerian-twitter-saga-lexical-racism-and-language-bias-masked-as-chatgpt-53ee9f6459aa, The Paul Graham vs Nigerian Twitter Saga; Lexical racism and language bias masked as ChatGPT…)

#include "template/feedback"
#include "template/footer"
