#define TITLE Artyom Bologov's Projects
#define DESCRIPTION "Artyom's projects—mostly weekend nerd snipes"

#include "template/header"

P()
Given my restless/bipolar nature,
I'm starting lots of projects—all
just to abandon them half a week after.
Still, some of them are/were quite fun:

SECTION2(bes, Brainfuck Enterprise Solutions)

P()
BES is the company I'm a proud CEO of. Its products (all written in Brainfuck) include:

DL(A(https://github.com/bf-enterprise-solutions/os.bf, OS.bf))
 a minimalist OS with a shell, file system, and a script engine.
DD(A(https://github.com/bf-enterprise-solutions/ed.bf, ed.bf))
 somewhat opinionated UNIX ed reimplementation.
DD(A(https://github.com/bf-enterprise-solutions/meta.bf, meta.bf))
 Turing-complete Brainfuck meta-interpreter.
DD(A(https://github.com/bf-enterprise-solutions/str.bf, str.bf))
 a solid fully embeddable string manipulation library.
DD(A(https://github.com/bf-enterprise-solutions/bf.doc, bf.doc))
 guidelines for clear and readable code docs.
DD(A(https://github.com/bf-enterprise-solutions/bf.style, bf.style))
 style guidelines for medium-to-huge BF codebases.
DD(A(https://github.com/aartaka/sade, Sade))
 optimizing compiler from Brainfuck to Lisp, just for the purpose of bootstrapping BF Enterprise Solutions' products.
DD(A(https://github.com/aartaka/reb, Reb))
 another shot at Brainfuck implementation, this time interpreted/transpiled to C and heavily regex-based.
END(DL)

SECTION2(atlas-nyxt, Atlas Engineer AMP() Nyxt Work)

P()
Projects made as part of Nyxt browser work and abstracted from its code.

DL(A(https://github.com/atlas-engineer/history-tree, History Tree))
 my Bachelor of Arts thesis idea of a browser-global history tree.
DD(A(https://github.com/atlas-engineer/njson, NJSON))
 convenience library (not a parser!) for JSON indexing, validation, and discovery.
DD(A(https://github.com/atlas-engineer/nsymbols, Nsymbols))
 symbol listing and binding inspection library.
DD(A(https://github.com/atlas-engineer/ndebug, Ndebug))
 custom (GUI too) debugger toolkit for CL.
DD(A(https://github.com/atlas-engineer/nyxt, And Nyxt itself!))
 Lots of things: WebKitGTK interfacing, graphical object inspection, UI framework building.
 Like, A(https://github.com/atlas-engineer/nyxt/pulls?q=is%3Apr+author%3Aaartaka, REALLY LOTS OF THINGS).
END(DL)

SECTION2(lisp-libs, Lisp Libraries and Projects)

P()
Several Common Lisp projects and libraries around them. Projects:

DL(A(https://github.com/aartaka/graven-image, Graven Image))
 Portability library for better CLI/text interaction with the running image.
DD(A(https://github.com/aartaka/tripod, Tripod))
 My polyglot blog engine A(/tripod, I wrote about).
END(DL)

P()
Libraries. These are mainly built around the projects above, but often are chaotic:

ULI A(https://github.com/aartaka/cl-telegram-bot-auto-api, Auto-generated Telegram Bot API bindings).
LI A(https://github.com/aartaka/trivial-toplevel-commands, Trivial Toplevel Commands) to define new REPL commands.
LI A(https://github.com/aartaka/trivial-toplevel-prompt, Trivial Toplevel Prompt) for nicer prompts like CD(aartaka>).
LI A(https://github.com/aartaka/trivial-time, Trivial Time) for code timing/benchmarking.
LI A(https://github.com/aartaka/trivial-inspect, Trivial Inspect), a portable toolkit for building inspectors.
LI A(https://github.com/aartaka/cl-pure-data, libpd (Pure Data) bindings).
LI A(https://github.com/aartaka/cl-translit, Cyrillic transliteration library).
END(UL)

SECTION2(turing-tarpits, Turing Tarpits)

P()
I'm into using the tech in ways not initially intented for this tech.
While I'm not yet at that level in hardware, I certainly am deep enough in software tarpits:

DL(AID(,bes, Brainfuck Enterprise Solution (above)))
 The primary example of me going all-in with an esoteric tech and ending up with real tools built in it.
DD(A(https://github.com/aartaka/advent-of-code-in-cl-loop, advent-of-code-in-cl-loop))
 Solving Advent of Code 2021 only using Common Lisp CD(loop) macro.
DD(A(https://github.com/aartaka/stdlambda, stdlambda))
 A Lispy standard library for Lambda Calculus, finally making this Turing tarpit a practical software environment.
DD(A(https://rosettacode.org/wiki/Ed, My Ed entries at RosettaCode))
 CD(ed) was not intended to be a general-purpose programming system, but here we are.
DD(A(https://github.com/aartaka/modal.ed, Modal->ed compiler))
 Now that we talk about CD(ed)...
DD(A(scripts/wisp.ed, And Wisp syntax preprocessor!))
 Wisp is an indentation-based syntax for Lisps, and I implemented with with a cursed set of regex.
END(DL)

SECTION2(configs, My Configs)

P()
Configs for everything:
ULI A(https://github.com/aartaka/lisp-config, Commmon Lisp + Readline)
with custom commands and prompt, terminal auto-completion,
and A(https://github.com/aartaka/graven-image, Graven Image) integration.
LI  A(https://github.com/aartaka/guix-config, Guix System).
LI  A(https://github.com/aartaka/stumpwm-config, StumpWM)
with lots of apps,
A(https://github.com/stumpwm/stumpwm-contrib/blob/master/util/binwarp/README.org, Binwarp mode),
and Emacsy keybindings for any CUA app.
LI  A(https://github.com/aartaka/emacs-config, Emacs).
LI  A(https://github.com/aartaka/nyxt-config, Nyxt browser). Abandoned yet still useful and exemplary.
LI  A(https://github.com/aartaka/st, My fork of Suckless Terminal).
LI  A(https://github.com/aartaka/surf, My heavily-hardened and Emacsified fork of Surf, Suckless Browser).
LI  A(https://github.com/aartaka/emvi, Classic Vi (not Vim!) config for Lisp programming).
LI  A(https://github.com/aartaka/laconia-theme, Laconia) and
A(https://github.com/aartaka/dark-atoll-theme, Dark Atoll), my themes for the software above.
END(UL)

#include "template/footer"
