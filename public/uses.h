#define TITLE Things I Use
#define DESCRIPTION "Even though I'm striving for Minimalism, there's a lot of stuff I use."

#include "template/header"

P()
I am aiming to produce less e-waste, so I don't use much hardware.
And I stick with whatever I have until it's unusable and I need to replace it.
Software-wise, I'm constantly looking for more resilient and minimal platforms.
Thus my interest in C, Lambda Calculus, ed(1), Lisp, and HTML.

SECTION2(laptop, Laptop: Lenovo Thinkpad L13 Yoga)

FIG()
IMG(assets/laptop.jpg, Photo of a laptop lid with lots of stickers with text in Russian and English. A ginger cat is also visible in the photo.)
FIGCAP(The Stickers and The Cat)

P()
A gift from Atlas Engineer folks, so thanks to them!

DL(OS)
Arch Linux.
(Used to be Guix System, but I was irritated with outdated package situation there.)
DD(Shell/DE/WM)
A(https://github.com/aartaka/stumpwm-config, Heavily-configured) StumpWM.
DD(Browser)
LibreWolf; Ungoogled Chromium for Chromium-reliant sites;
A(nyxt-to-surf, Surf (segfaults right now, but I am working on it!))
DD(Programming Languages/Environments)
Common Lisp (SBCL, ECL)
A(https://github.com/aartaka/graven-image, with Graven Ima...)
A(https://github.com/aartaka?tab=repositories&q=trivial, ahem, Trivial libs!)
then Clojure/Scheme, C+POSIX, HTML/CSS 3-5+,
A(sed-ed, ed(1)).
DD(Writing/Website/PDF/Gemini Generator)
A(this-post-is-cpp, C Preprocessor).
DD(Games)
Dead Cells, TIS-100 (and other Zachtronics titles), Trine (except part 3, we don't talk about it here).
DD(Everything else)
A(https://github.com/aartaka/emacs-config, Heavily-configured Emacs).
I'm trying to get off this drug, but the thing is strong.
END(DL)

SECTION2(smartphone, Smartphone: OnePlus 9)

P()
I have an ISP-locked iPhone and KaiOS Nokia 2720 laying around.
But neither was powerful enough for daily-driving.

ULI I had a OnePlus 1 before, but it fell from quite a height and didn't sustain the damage.
LI  So I got a (used, of course) OnePlus 5T and set up LineageOS there. It has fallen from moderate heights and is damaged too, though.
LI  I got OnePlus 9 recently and (unfortunately!) was unable to install LineageOS on it—it's unsupported.
END(UL)

P()
The setup is:

DL(OS)
OxygenOS, Gapps disabled (but not removed yet), not rooted yet.
DD(Browser)
Firefox Beta/Nightly; Brave for Chromium-reliant sites.
DD(Socials)
Telegram FOSS or
A(https://github.com/wrwrabbit/Partisan-Telegram-Android, P-Telegram);
A(https://f-droid.org/packages/fr.gouv.etalab.mastodon/, Mastodon via Fedilab);
LinkedIn
A(cv, (hire me!)).
DD(Games)
Shattered Pixel Dungeon, GTA Chinatown Wars, Card Thief.
END(DL)

SECTION3(blindeck, Cyberdeck/Blindeck)

FIG()
IMG(assets/blindeck.jpeg, Photo of a mechanical keyboard with a circuitry and wires sticking out of the back, standing on a wooden table. The keyboard is beige with some light-red keys (one of them reads 'Vortex'.) the circuit in the back has several USB ports, Ethernet port, GPIO pins, HDMI and Type C ports. There are several cables plugged into USB ports. The place the circuit is plugged in looks janky and cut out by hand.)
FIGCAP(It looks slightly better now, but here is an older photo)

P()
I have this project: no-screen cyberdeck with audio interface only.
Preferably with Lisp OS.
And it's not even a project idea—I built it!

DL(Hardware)
Raspberry PI 400.
DD(Case/keyboard)
Vortex PC66 ANSI, 68 Cherry MX Clear keys.
DD(OS)
Debian/Raspbian/Raspberry PI OS.
DD(Shell)
Embeddable Common Lisp.
Yes, I'm booting right into ECL REPL and using it as a way to interact with the system.
A(https://github.com/aartaka/lisp-config/blob/master/talkative.lisp, And it is fully speech-enabled too!)
END(DL)

P()
And that's roughly it for the tech I use.
Should I tell you about my Tarot deck?
No, not today.

#include "template/footer"
