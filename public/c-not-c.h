#define TITLE C Until It Is No Longer C
#define DESCRIPTION "Get horrified by how pretty I'm making my C code!"
#define IMAGE assets/pieter-van-der-heyden-pride.jpg
#define IMAGE_ALT "Engraving of a landscape with pompous buildings and anthropomorphic creatures looking into mirrors, galloping around on horses, and otherwise engaging in unidentifiable acts of sinful pride."

#include "template/header"

FIG()
IMG(IMAGE, IMAGE_ALT)
FIGCAP(Enjoying C while it is still C. "Pride" by Pieter van der Heyden, 1558)

P()
We have to admit that C is
AHERE(making-c-uglier, inherently/potentially ugly).
One can try
AHERE(making-c-prettier, making it prettier),
but there's only so much one can do to C without compromizing its nature.
That's what I'm going to do here—stretching the limits of what C is, introducing some prettier things.
If you're a C programmer (like me), you might end up horrified by what I'm doing.
Otherwise, sit back and watch how readable C can become!

SECTION2(standard-headers, Standard Headers: Booleans and Nicer Logic)

P()
C23 made booleans part of the language.
Which is a good direction.
But what if I don't want to wait for C23 to be rolled out in GCC or Clang?
Well, I can always define some booleans myself!

PRE()
HASH()if (__STDC_VERSION__ >= 199901L AMP()AMP() __STDC_VERSION__ LT() 202000L)
HASH()include LT()stdbool.h>
HASH()elif __STDC_VERSION__ < 199901L
HASH()define true  1
HASH()define false 0
typedef int _Bool;
HASH()define bool _Bool
HASH()endif
PRECAP(Making booleans accessible on every version of the standard)

P()
Et voilá!
Now we can do proper booleans:

PRE()
// Check whether the char is a control one
// Yes, I know of iscntrl, bear with me
bool
iscontrol (unsigned int c)
{
        return (127 == c || c < 32);
}
PRECAP(Using booleans)

P()
Yes, implicit conversion to booleans.
Because booleans are nothing but unsigned integers 1 and 0.
Now what doesn't work for me is this ugly double vertical bar.
I want some Pythonesque boolean logic!

P()
It turns out I can have this nicer boolean logic, just one CD(HASH()include) away!

PRE()
HASH()include LT()iso646.h>
HASH()define eq ==
HASH()define bitnot ~
HASH()define bitxor ^
PRECAP(iso646 and some more aliases)

P()
I'm also defining some missing bits and fixing the inconsistently named
CD(xor) and CD(compl).
With these, CD(iscontrol) becomes even more readable!

PRE()
bool
iscontrol (unsigned int c)
{
        return (127 eq c or c < 32);
}
PRECAP(iso646 macro use)

P()
CD(eq) feels sligtly off here.
Why not define another macro for it?
A couple of macros, actually.

PRE()
HASH()define is ==
HASH()define isnt !=
PRECAP(Defining (in)equality)

P()
And use it like:

PRE()
return c is 127 or c < 32;
PRECAP(iso646 macro use)

P()
Notice that I switched the order of arguments to a more intuitive one.
Putting a constant before the equality operator is no longer necessary.
(C programmers do that to avoid typos like CD(c = 127),
relying on compiler to scream when it sees CD(127 = c).)
After all, the spelled-out operator cannot end up as assignment.
Readability and reliability win.

SECTION2(nicer-types, Nicer Types: Fixed Width and Custom Shortcuts)

P()
A("making-c-prettier#types-and-constants", I already mentioned these before).
But it never hurts to use these more:

PRE()
HASH()include LT()stdint.h>
PRECAP(Including fixed-width types like int32_t)

P()
And then, we can go even further, inspired by brevity of CD(uint8_t):

PRE()
typedef unsigned char  uchar;
typedef unsigned char  ubyte;
typedef unsigned short ushort;
typedef unsigned int   uint;
typedef unsigned long  ulong;
PRECAP(Defining shorter aliases for standard types)

P()
Going even further, here are some more Go-inspired types:

PRE()
typedef char*          string;
typedef char           byte;
typedef char*          bytes;
typedef void*          any;
PRECAP(More shortcut/convenience types)

PRE()
bool
iscontrol (byte c) // Or uchar, or uint
{
        return c is 127 or c < 32;
}
PRECAP(Using new "byte" type)

SECTION2(type-inference, Type Inference)

P()
Another nice-but-not-quite-C thing C23 added is... type inference!
You may disagree with this decision, but it certainly is nice to have.
So let's add it:

PRE()
HASH()if defined(__GNUC__) || defined(__GNUG__)
HASH()define var   __auto_type
HASH()define let   __auto_type
HASH()define local __auto_type
HASH()elif __STDC_VERSION__ > 201710L || defined(__cplusplus)
HASH()define var   auto
HASH()define let   auto
HASH()define local auto
HASH()endif
PRECAP(Defining auto-inferred variable definition macros)

P()
And use it too!

PRE()
bool
iscontrol (byte c)
{
        var delete = 127,
             space = ' ';
        return c is delete or c < space;
}
PRECAP(Using type-inferred vars)

P()
Okay, I should probably stop here.
Both because the example is no longer improvable.
And because most of the readers are already hemorrhaging.
Sorry!
I could've promised I won't do it again, but alas.
Have a good rest of the day with this newly acquired phobia.

P()
Oh and check out
A(https://github.com/aartaka/pretty.c, Pretty.C),
my project making C even further from God!

#include "template/feedback"
#include "template/footer"
