#define TITLE Printf Is Useless
#define DESCRIPTION "Printf (and the derivatives every language has) are a bane \
that should only be tolerated when printing floats. \
Otherwise, I beg you, use string interpolation or structured output."
#define IMAGE assets/printf.png
#define IMAGE_ALT "Formal-looking white canvas. \
On it, 'printf' is written in huge formal-ish blue letters and crossed out. \
In the corners, 'aartaka.me' and 'Artyom Bologov' are written in less formal font. \
Percent signs (used by printf for formatting) are floating all around the canvas."
#define LIGHT_COLOR white
#define LIGHT_ACCENT_COLOR HASH()0645ad
#define DARK_ACCENT_COLOR HASH()008000

#include "template/header"

PIC(IMAGE, assets/printf-dark.png, IMAGE_ALT)

DETAILS(A story that actually motivated this post)
P()
I worked on extending a C++ tool.
Making it produce results with higher precision, in particular.
I discovered that it's surprisingly hard to output the custom precision number in C++.
Here are two ways I found:

PRE(C++)
cout.precision(18);
cout << setprecision(18) << "rest of the output";
PRECAP(Changing the precision of stream output in C++)

P()
Both of these mean modifying the global state of the stream.
Which, in case of CD(cout), is quite a consequential change.
Should be avoided at all costs.

P()
The solution my friend (an experienced C++ dev) suggested was to just use CD(printf).
But... Is CD(printf) the C++ way?

P()
Anyway, printing floats is a mess.
Unlike printing anything else.
So this post goes into why you don't need
CD(printf) and the like...
unless you, like me, is stuck trying to print floats.
END(DETAILS)

P()
We (programmers) all use formatted output daily.
But most formatting primitives, especially in low-level languages, are for numbers.
So formatted output

ULI Is un-ergonomic in the simplest case of outputting a single number;
LI  Puts data away from its representation;
LI  And is overcomplicated/unnecessary for non-number output cases.
END(UL)

P()
So what I'm trying to say here is not that format strings are useless.
I'm rather of the opinion they can be shortened, optimized, and simplified.
Except for float printing cases where printing parameters actually matter.
The rest is easily accomplished with string interpolation or even separate printing expressions.
No meter-thick historic crust.

SECTION2(scheme-format, Scheme and SRFI 48 format)

P()
Let's start with something simple.
There were several attempts to standardize format strings in Scheme.
One is
A(https://srfi.schemers.org/srfi-28/, SRFI 28 Basic Format Strings),
A(https://srfi.schemers.org/srfi-48/, then SRFI 48 Intermediate Format Strings),
A(https://srfi.schemers.org/srfi-54/, and SRFI 54 (cat) for alternative take),
and others.

P()
SRFI 28 is quite basic with only three directives:

DL(~a) for human-readable output.
DD(~s) for machine-readable output.
DD(and ~%) for newline.
END(DL)

P()
It's more or less clear why one would want to extend this—while
human-readable vs. machine-readable is quite enough for many cases, one might need finer output.
That's where SRFI 48 comes in:

BQ()This SRFI extends SRFI 28 in being more generally useful but is less general than advanced format strings in that it does not allow, <mark>aside from ~F</mark>, for controlled positioning of text within fields.
BQCAP(Abstract of SRFI 48, highlighting mine)

P()
In other words: SRFI 48 exists for the sole purpose of printing floats prettily.
(I mean, there are lots of other directives, mimicking for C CD(printf) and Common Lisp CD(format).
(Ooops, spoilers!)
But these are still mostly falling into the human/machine-readable category.)
Nothing new in SRFI 48, except float printing.
All the rest is covered by SRFI 28, really.

SECTION2(c-printf, printf In C: Exists For Floats)

P()
C CD(printf) is powerful, just enough to
A(https://github.com/carlini/printf-tac-toe, power a game of Tic-Tac-Toe).
It has lots of modifier flags for its directives:

DL(-) Left-justify the printed value.
DD(+) Always show the sign.
DD(space) Pad the value with spaces.
DD(0) Pad the value with zeros.
DD(HASH()) Alternative form.
END(DL)

P()
And there's a moderate number of directives to choose from.
Here's my categorization:

DL(% and n)
Trivial/dangerous.
DD(c and s)
Have dedicated functions (CD(fputc) and CD(fputs) respectively) matching them.
DD(p)
Somewhat special, but can also be hexadecimal or whatever integer printing by default.
DD(d/i, \u, o, x)
For integers in different bases.
DD(\f, e, a, and g)
For floating point numbers.
END(DL)

P()
It's useless, because you have an assortment of number-printing directives.
Some of which you're unlikely to ever use.
And the useful stuff like string printing is so stripped down that it's almost useless too.
How often does C programmer implements conditional printing with

PRE(c)
printf("%s%s", (boolean ? "!" : ""), "string");
PRECAP(Conditional printing, C interpretation)

P()
Too often.
String interpolation akin to JavaScript would make more sense.
It would both put the data into the output.
And won't bother with the obvious (from the type system point of view) details of printing directives.

PRE(js)
console.log(`${boolean ? "!" : ""}string`)
PRECAP(JavaScript interpolated version )

P()
While this example would require type inference to be ported to C, it's a good things to aim for.
(And you'll see that it's achievable in the end of this post.)

SECTION2(cl-format, Counterexample: Common Lisp format)

P()
Common Lisp's
CD(format) is likely Turing-complete.
Not because of some CD(%n) hack that makes CD(printf) dangerous.
But rather because it was intended as a one-stop shop for output.

P()
I'll refer you to
AID(cl-is-lots, format, my earlier post) for a full feature listing (and horrible format string from my own experience!)
But the gist is that CD(format) is a control flow of its own, replacing conditionals and loops by format strings.
And that makes it more useful, but the problem of increasing formatting-data distance is still there.

SECTION2(solution, Solution: Generic Printing and Interpolation)

P()
I'll handle the hardest case out of these three: C data formatting.
Thanks to C11 generics, it's possible to implement generic printing
(and that's exactly what I'm doing in my
A(https://github.com/aartaka/pretty.c, Pretty.C))
that covers 99% of the printing cases:

PRE(c)
HASH()if __STDC_VERSION__ >= 201112L
HASH()define print(...)                                        \
        _Generic((__VA_ARGS__),                                \
                 _Bool: fputs(stdout, (_Bool)(__VA_ARGS__) ? "true" : "false"), \
                 default: printf(                              \
                         _Generic((__VA_ARGS__),               \
                                  char*:              "%s",    \
                                  char:               "%c",    \
                                  signed char:        "%hhi",  \
                                  short:              "%hi",   \
                                  int:                "%i",    \
                                  long:               "%li",   \
                                  long long:          "%lli",  \
                                  unsigned char:      "%hhu",  \
                                  unsigned short:     "%hi",   \
                                  unsigned int:       "%u",    \
                                  unsigned long:      "%lu",   \
                                  unsigned long long: "%llu",  \
                                  float:              "%g",    \
                                  double:             "%g",    \
                                  long double:        "%Lg",   \
                                  default:            "%p"),   \
                         (__VA_ARGS__)))
HASH()endif
PRECAP(Generic printing in C)

P()
If there is CD(tgmath.h) in C, why is there no CD(tgio.h)?
I dunno, maybe I should file a proposal to C standard people?

P()
Here's how the example from earlier looks with this new macro:

PRE()
if (boolean)
        print("!");
print("string");
PRECAP(Problematic example fixed with newly defined print() macro)

P()
It's the same length as printf (or even less if we don't consider 8 spaces of indentation!)
But the structure and syntax are easier on both human and machine readers.

P()
The solution in Scheme and CL
A(http://cs-www.cs.yale.edu/homes/dvm/format-stinks.html, was already proposed):
just use language macro facilities to implement printing that actually reflect the output shape.
Something like string interpolation, but even more structured and enforceable!

P()
So yes, you likely don't need
CD(printf), you need structured and simple output.
Like string interpolation or generic printing.
You're still stuck with
CD(printf) if you're on C compilers nostalgic for the times before C11.
But, that aside, use some structured output instead of formatted output, won't you?

#include "template/feedback"
#include "template/footer"
