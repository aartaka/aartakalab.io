#define TITLE Prose vs. Tweet: How We Tell Stories
#define DESCRIPTION "We always told stories. We always will. Though our stories fit into 140 chars now, which is both a blessing and a curse."

#include "template/header"

P()
My attention span is getting shorter and shorter.
Yours does too, I'm sure.
Blaming someone doesn't help
(although there's plenty entities to blame.)
Reiterating on what we have and what we're losing does.
Knowing what forms of communication humans can have.
Trying these forms of communication.
Preserving them.

P()
Homo sapiens is a species that survived because of ability to

ULI Make tools.
LI  Establish social structures.
LI  And communicate effectively (although this is such a stained phrase now that it almost means nothing.)
END(UL)

P()
One of these forms of communication always was stories.
A way to transfer meaning metaphorically.
Communicate the idea as vividly as possible.
Evoke a feeling and make it stick.
Gift a new knowledge.
Make it transferrable and reproducible.

P()
Sounds a bit like a definition of computer, at least the one I remember from high school:
"An electronic machine that stores, processes, and transmits information."
So computers are working with information, much like humans.
But what they are incapable of is telling stories.
Even with ChatGPT, human(e) stories are something left for humans to enjoy.

P()
There's one way human-machine cyborg changed the stories, though.
They became short (like our attention span), infinitely reproducible (even more than they initially were), and slightly distracted.
Which might sound bad: we want full-fledged unique and focused stories, right?
Not necessarily, especially given that we can have best of both worlds.

P()
What I'm trying to say is:
even though our attention span and stories are getting shorter,
our ability to transmit them and emphathize with them is amplified by the Internet and its hypertextual immediacy.
So let's use that for the better.

SECTION2(narrans, Homo Narrans: Speaking in Stories)

P()
Information existed before Internet, books, or even writing itself were there.
Recited stories and songs are the primordial Internet.
The occupation of a minstrel was to go around the globe telling stories of gods and monsters.
Of humans and animals.
Of life and death.
Of do's and don'ts.

P()
These stories were unique in more than one way.

DL(Stories were reproducible)
Once one heard the story, they can tell it again.
DD(Stories were structured)
Once the person knew one story blueprint, they could tell a different one, derived from it.
A(https://en.wikipedia.org/wiki/Hero%27s_journey, (Yes, hi Campbell!))
DD(Stories had a message)
The person hearing the story ends up with a new knowledge and a new (moral?) stance on something.
END(DL)

P()
That's why the genre of fiction exists to this day.
(Even if oblivious to its own origins.)
To tell stories, to learn, to share them as prose and anecdotes.

P()
Yet our stories are undergoing a change.

SECTION2(twittees, Homo Twittees: Speaking in 140 Characters)

P()
You know the "Ernest Hemingway once bet that he would write the shortest story that could touch anyone" meme?
That's what Twitter/X/Mastodon (going to refer to it as Twitter from now on) type of communication boils down to.
Fitting the thought into 140/280/500 chars.
Trying to evoke maximum emotions possible given the format restrictions.
Well, if one's addicted to the chase/dopamine, at least.
Most (including myself) are.

P()
Twitter (and the like) erode one's attention span, deepen the depression, and cause one to anxiously check notifications.
Bad, right?
Well, that's the reality we live in, and we have to make do with what we have.
In particular, the form of communication that the restrictions of social media provoked.
The snappy, insightful, and to the point type of communication.
Amplified by the unparalleled outreach Internet gives one.

P()
The tweet is truly a miracle of modern engineering.
A short message that doesn't clobber the DB.
Easy to display, because it usually doesn't overflow the bounding box.
Easy to transmit in the first 14kb of the HTTP response (well, supposedly...)
And reliable in the way it mediates one's thought.
If I were
A(https://en.wikipedia.org/wiki/Linguistic_relativity, a Whorfian),
I'd say "shapes the thought".

P()
Tweets, especially with their initial 140 character restriction, are provocative as a genre.
The thought should be cleaned up from all the extra layers of narrative shell.
Less metaphors, less structures, more snappy thought.
Convince.
Amaze.
Floor.
Sell the pen.

P()
One problem (relating to stories, rather than depression) with Twitter-type messages:
They are not sufficient to transmit complex ideas or to express them comprehensively.
That's why there's the genre of threads: sequence of tweets, splitting some big topic into many digestible tweets.
Reminds one of paragraphs in the story, right?
Except that every one of these is supposed to be freestanding and hilarious.

SECTION3(ludens, Homo Ludens: Speaking in Puns)

P()
One of the theories of humour is the Incongruity Theory.
It might be summarized with Henri Bergson's quote "something mechanical encrusted on the living".
Once our joke uncovers the mechanical character of something, we laugh.
And our goal as an individual, at least in this paradigm,
is to cause as much insights and "a-ha!" moments about the mechanical reality as possible.
(I must make a disclaimer: Incongruity Theory is but one of many theories of humour, all with their shortcomings.)

P()
The role of a clown is the role of a travelling sage.
Clown—at least a good one—uncovers what we didn't notice before.
Some weirdness of our reality.
A stagnant flow of thought.
New type of interaction/communication we didn't notice.

P()
I had an honor to have a clown as my university instructor once.
And that's the person that put a leaning to anarchism and ecology into me.
Often just one incongruous joke at a time.
That's the power of proper humour.
The power to transform a human.

P()
That's the perfect tweet: metamorphic, evocative, engaging.
Yet still telling the story.

P()
That's what Bakhtin's
A(https://en.wikipedia.org/wiki/Carnivalesque, idea of carnival) was:

ULI a transformative power,
LI  subverting the established structures,
LI  and making new forms of interaction to spring up to life.
END(UL)

P()
Bakhtin's descriptions of medieval carnivals sound mouth-watering.
A world without subordination, structure, piety.
Something against the protestant ethic—
A(https://en.wikipedia.org/wiki/The_Hacker_Ethic_and_the_Spirit_of_the_Information_Age, remember Weber and Himanen?)—
we all are stuck in.
Some new type of human, really.
Homo Ludens, Human the Playful.

P()
This section is already quite heavy with academic references, so I'm going to stop.
But the idea is there: play is a separate and vital mode of human activity.
Linguistic play too!
Puns/dad jokes are exactly the kind of linguistic play that our stories can take.
A final (yet often far historically preceding the tweet—jokes, idioms, epigrams) spin on Twitter stories.
Evolving towards a long repressed carnivalesque mode of existence.

SECTION2(symbolicus, Homo Symbolicus: Speaking In Doodles)

P()
One type of communication that I have to include for completeness: math.
A "transcendental language",
A(https://en.wikipedia.org/wiki/Women%2C_Fire%2C_and_Dangerous_Things, as George Lakoff) calls it.
A way to communicate complex ideas via doodles usually called "symbols".
Symbols as in "symbolic intelligence" that bred human success.
(It's a sad thing that many, including myself, are missing out on math.)
Manipulating abstract concepts represented by these doodles.

P()
Someone might say that symbolic intelligence is actually not about math.
It's about language.
Merging syntactic trees, Noam Chomsky might claim (while he's still alive.)
But again, math and languages are falling into the same category: telling stories with symbols.
Stories that are harder to process, but are even more structured and reproducible than the recited ones.

SECTION2(sapiens, Homo Sapiens: Speaking)

P()
We've gone full circle.
Let this be a wrap up summarizing all of the above:

ULI We always told stories.
LI  We always will.
LI  Our stories take a new shape now, though.
LI  A shape of short puns and insights visible to the whole globe.
LI  Long stories and symbolic languages might seem to be in disregard, but are still there hiding in the structure of the Tweet/Thread, anyway.
END(UL)

P()
To respond to a
A(https://tech.lgbt/@laxla/112654290462842096, post on non-Internet vs. Internet genres by Laxystem):
Tweets have a real-world analogue.
Jokes.
Stand-up artists, even though a modern-ish phenomenon, are real life microblogs.
Everyday stories, sloppy punchlines, and often too many poop jokes.
But still, a valid real-life format for tweets.

P()
Humans possess a huge variety of ways to communicate (I only highlighted some!) and tell stories.
And this variety is worth preserving,
A(https://www.australiangeographic.com.au/news/2024/05/awakening-a-sleeping-language/, much like extinct/sleeping languages and cultures are).
So let's tell more stories in whatever format we like.
Even if these are shitposty tweets/toots.
A(https://merveilles.town/@aartaka, Follow me on Mastodon to get your daily dose of shitposts, by the way!)

#include "template/feedback"
#include "template/footer"
