#define TITLE 5 (Wrong) Regex To Parse Parentheses
#define DESCRIPTION "Regex are powerful. \
To the point you may try to parse HTML or Lisp with it. \
A doomed enterprise, right? \
But it's possible, actually."
#define IMAGE assets/paren-regex.png
#define IMAGE_ALT "A banner on light gray (smoke shade) background. \
On it, REGEX 4 PARENS is written in large parentheses-mimicking letters. \
'PARENS' is interspersed with green parentheses. \
In the corner, it reads 'AARTAKA' in same parenthetical letters."
#define LIGHT_COLOR HASH()e0e0e0
#define DARK_COLOR HASH()202020
#define LIGHT_ACCENT_COLOR HASH()008700
#define DARK_ACCENT_COLOR HASH()ff8700

#include "template/header"

IMG(IMAGE, IMAGE_ALT)

P()
I'm trying to write
A(https://github.com/aartaka/modal.ed, Modal.ed, a compiler) from
A(https://wiki.xxiivv.com/site/modal.html, Modal) to
A(https://www.man7.org/linux/man-pages/man1/ed.1p.html, ed(1)) scripts.
Which is quite a story of its own, I guess?
I'll leave it for later.

P()
One part of this project is reliably parsing Modal syntax.
Here are some examples of Modal rules:

PRE()
<> ((unwrap ?x)) (unwrap ?x)
<> (q ?x) (q ?x)
<> (`?: ?0 ?1) ((Int ?:))
<> (?: print ') (?:)
<> (iterating ?x ?y ?n    (1) ?a ?b) (?((?:) ?:) \s\s)
<> (?0 ?1 `?:) ?:
<> rule0 data
PRECAP(Modal rules in all their diversity)

P()
The general pattern (hopefully apparent from the snippets) is:

ULI Rules are starting with CD(<>)
LI  Rules have a left hand side (pattern) and a right hand side (replacement.)
LI  Both sides are wrapped in parentheses, but parens are sometimes omitted for obvious single-symbol cases.
LI  The pattern/replacement can get as complex as the programmer wishes. But (hopefully) the parentheses are reliably balanced.
END(UL)

P()
So all we need is to parse balanced parentheses in ed.
Easy, right?
This post is an exercise of desperation trying to parse parentheses with regex.
A journey that has a happily ever after, unfortunately.
But here's a bit of boring stuff first:

SECTION2(pragmatic, Pragmatic: Recursive Regex)

P()
The search for perfect regex led me to StackOverflow.
A(https://stackoverflow.com/questions/546433/regular-expression-to-match-balanced-parentheses, To the "Regular expression to match balanced parentheses" question in particular).
A place where the best minds of the industry were either:

ULI "No, it's not possible.", or
LI  Suggesting non-portable vendor-specific regex extensions.
END(UL)

P()
.NET and Perl regex are suggested multiple times, with a recurring form of

PRE(perl)
\((?:[^)(]+|(?R))*+\)
PRECAP(Perl recursive regex for parentheses matching)

P()
Perl is nice, but
AHERE(the-regex, There Is No Such Thing As The Regex),
and I have a soft spot for ed regex, and POSIX BREs in particular.
These don't have recursive matching.
So I was bored out of my mind reading all these StackOverflow exercises in conformity.
And I decided to prove them wrong:
One does not need .NET or Perl to parse parentheses properly.
<span id=my-approach>POSIX BREs and text substitution are more than enough</span> to work with parentheses and Modal rules.

SECTION2(naive, Naive: It Is Balanced, Right?)

P()
So the simplest way to match parentheses is to... match parentheses.
And nothing/anything else in between:

PRE(sed)
(.*)
PRECAP(Implying parentheses are balanced, because they must be?)

P()
There are multiple cases where this regex breaks:

DL((...) (...))      Is matched as one piece due to regex greediness.
<dt>()))))))<dt><dd> Is matched too, because we actually don't look for balance.
<dt>()(((((()<dt><dd> A variation on the previous too, but ugly enough to include.
END(DL)

P()
We need more structure to paren matching.
And we'll have it:

SECTION2(restrictive, Restrictive: I Will Be Correct Whatever the Cost)

P()
So we don't want stray parens and we need balanced parentheses.
We can arrange exactly that:

PRE(sed)
([^()]*)
PRECAP(A regex to match a parenthesised list with no nested lists)

P()
So we match two parentheses and some number of non-parentheses inside.
Which doesn't give us false positives like in the first iteration.
But doesn't scale to more nested patterns.

SECTION2(pedantic, Pedantic: Hardcoding Patterns)

P()
There has to be a way that covers most of the practical cases.
I'm ready to cut corners, like too deeply nested parentheses.
Which means most cases can be hard-coded:

PRE(sed)
HASH() Level one
([^()]*)
HASH() Level two
(\([^() ]\{0,1\}\(([^()]*)\)\{0,1\}\)*)
HASH() Level three
(\([^() ]\{0,1\}\((\([^() ]\{0,1\}\(([^()]*)\)\{0,1\}\)*)\)\{0,1\}\)*)
HASH() Et cetera
PRECAP(Hard-coded nested parentheses regex)

P()
The second and third ones look weird, but it's a way of saying:
there must be a monolythic symbol (like CD(hello))
or a parenthesised list of symbols (like CD((hello there))) inside these two parentheses.
This is due to BREs having no alternation pattern, CD(|).
These rules can be conveniently rewritten in POSIX EREs
(that's the last time I employ such a bloated regex format in this post!)

PRE(sed)
\(([^()]+|\([^()]*\))*\)
PRECAP(POSIX ERE for two-levels-deep parentheses)

P()
Elegant, but less portable than BREs.
I need to run it on BSD, UNIX, and GNU ed, so begone EREs!

SECTION2(lispy, Lispy: Looking for Tiger Tails)

P()
Another way to ensure the balance is to restrict the inputs.
Say, we can require Modal rules to be Lisp-like in only having prefix operators.
With all the argument at the tail.
This would result in a dreaded "tiger tail" or parentheses at the end.
But it also makes the parsing problem irrelevant: just match the tail and you're golden:

PRE(sed)
((*[^)]*)*)
PRECAP(Lispy parentheses matching)

P()
Most of Modal codebases are somewhat lispy already.
So this regex has a higher success/LoC ratio than, for example, hardcoded rules.
But I can see why one might be uneasy about it—it's not really structural.
Unlike...

SECTION2(recursive, Recursive, But Not That One)

P()
The structure of the Modal rules is:

ULI It's a symbol or register.
LI  Or it's a list of symbols/registers.
LI  Or it's a list of symbols/registers and other lists.
LI  And so on.
END(UL)

P()
So it dawned on me: we can isolate symbols,
then use these to highlight the simple lists and then propagate up the list ladder.
So the algorithm I came up with is:

ULI Wrap all simple symbols in Russian quotes: «». (Because they are distinctive and unique enough for parsing, and because I'm ethnically Russian.)
LI  If the list consists entirely of quoted forms or is empty, remove all quotes inside and wrap the list itself into quotes.
LI  Repeat until there's nothing to substitute and the quoted list is the toplevel one.
END(UL)

P()
And here's (an arguably ugly) code that does this in my ed script:

PRE(sed)
HASH() Wrap every symbol/nil into quotes
g/[^() ]\{1,\}/s//«&»/g
g/()/s//«&»/g
HASH() Actual parsing: if the current layer of parens is all quoted, quote it and unquote contents
g/(«\([^»]*\)» «\([^»]*\)» «\([^»]*\)» «\([^»]*\)» «\([^»]*\)» «\([^»]*\)» «\([^»]*\)» «\([^»]*\)» «\([^»]*\)»)/s//«(\1 \2 \3 \4 \5 \6 \7 \8 \9)»/g
g/(«\([^»]*\)» «\([^»]*\)» «\([^»]*\)» «\([^»]*\)» «\([^»]*\)» «\([^»]*\)» «\([^»]*\)» «\([^»]*\)»)/s//«(\1 \2 \3 \4 \5 \6 \7 \8)»/g
g/(«\([^»]*\)» «\([^»]*\)» «\([^»]*\)» «\([^»]*\)» «\([^»]*\)» «\([^»]*\)» «\([^»]*\)»)/s//«(\1 \2 \3 \4 \5 \6 \7)»/g
g/(«\([^»]*\)» «\([^»]*\)» «\([^»]*\)» «\([^»]*\)» «\([^»]*\)» «\([^»]*\)»)/s//«(\1 \2 \3 \4 \5 \6)»/g
g/(«\([^»]*\)» «\([^»]*\)» «\([^»]*\)» «\([^»]*\)» «\([^»]*\)»)/s//«(\1 \2 \3 \4 \5)»/g
g/(«\([^»]*\)» «\([^»]*\)» «\([^»]*\)» «\([^»]*\)»)/s//«(\1 \2 \3 \4)»/g
g/(«\([^»]*\)» «\([^»]*\)» «\([^»]*\)»)/s//«(\1 \2 \3)»/g
g/(«\([^»]*\)» «\([^»]*\)»)/s//«(\1 \2)»/g
g/(«\([^»]*\)»)/s//«(\1)»/g
HASH() Repeating second time for recursive case
HASH() Ad infinitum
PRECAP(Recursive parentheses matching)

P()
You may say I'm cheating, because it's not only regex, it's also substitution rules.
AID(,my-approach, But I warned you!)
The results are relatively good:

ULI The parsed depth is at worst the number of copy-pasted ladders.
LI  And at best the number of inidividual substitutions.
LI  The average lies somewhere around the number I don't know how to compute, but it should be good enough.
LI  The algorithm would be perfect if there were looping constructs in ed.
But there aren't.
I might've used
A(https://www.man7.org/linux/man-pages/man1/sed.1p.html, sed(1)) for looping, but...
AHERE(sed-ed, I despise sed).
END(UL)

P()
So I have my parsing rules happily chewing on Modal code.
A(https://github.com/aartaka/modal.ed, In Modal.ed),
check it out!
Thanks for lending me a ear and embracing the power of regex 🖤

#include "template/feedback"
#include "template/footer"
