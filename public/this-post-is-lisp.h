#define TITLE This Post is Written in Lisp
#define DESCRIPTION "This is probably some fanatic with weird AST magic, you say. But if this magic allows one writing with tab-completion, automatic HTML boilerplate, and other goodies—then why not try it? 😉"
#define IMAGE assets/thumbnail-00FF00-FFFFFF-FFC0CB.png

#include "template/header"

P()
I launch my IDE (well, actually, it's Emacs with SLY),
open up a Lisp file,
start up the console (we call it REPL here) side-by-side with the code,
and start programming.
I pick tab completions, add more procedures,
and iterate over the data.
Occasionally, some syntax error pops at me—some
stray comma or misspelled keyword.
It compiles, memory consumption, and code complexity is bearable.
A neat piece, I'd say.

P()
Okay, that's a good blog post!
Let's publish it.
Maybe program some project afterward: I've got some time left before sunset.

P()
So yes, this post (and other posts on this blog) are written in Lisp. Most of it is regular Lisp syntax:

ULI symbols
LI lists
LI and sometimes strings.
END(UL)

P()
Having these, I get a fancy blog post generated from raw s-expressions,
needing just a couple of syntax quirks to resolve.
Here's how it works:

ULI I load A(https://github.com/ruricolist/spinneret, Spinneret) (a library using Lisp macros to construct HTML) into the running Lisp image.
LI Then comes my boilerplate macros collection:
DL(:page*)
To generate all the <meta> tags, favicons, and styles.
DD(:section*)
To create linkable sections.
DD(:p*)
To generate linkable plaintext paragraphs from raw Lisp code (you've seen these pilcrow signs after the previous paragraph.)
DD(:ul*/:ol*/:dl*/:table*)
To shorten the overly verbose list and table markup (including this list you're reading.)
DD(:a*)
To generate anchor tags from the shortest possible links and text. 
END(DL)
LI Load Lisp files with these macros in them and save the generated HTML.
LI If something doesn't compile (usually due to unescaped comma or colon)—fix and reload.
LI Push the files to the repository where CI will deploy them to the website.
LI Profit!
END(UL)

P()
So here I am, bored before my IDE and uncertain about what to do next because my blog posts are already done compiling and deploying.
Maybe write some more?

P()
Update December 2023: I switched to
AHERE(this-post-is-cpp, C preprocessor for website generation.)

#include "template/feedback"
#include "template/footer"
