#define TITLE No such page?
#define DESCRIPTION "Page not found"

#include "template/header"

P()
This page might've been moved or renamed—I tend to move things around sometimes.
Check out
AHERE(index, the welcome page),
anyway.

#include "template/footer"
