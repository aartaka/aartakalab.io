#define TITLE Making Sense of Lambda Calculus 3: Truth or Dare With Booleans
#define DESCRIPTION "Booleans are simple and elegant in Lambda Calculus, but they take some getting used to. \
This post tries to explain LC booleans to at least myself."
#define IMAGE "assets/thumbnail-CD7F32-FFFFFF-5E5184.png"
#define LIGHT_COLOR HASH()ffffff
#define DARK_ACCENT_COLOR HASH()cd7f32
#define LIGHT_ACCENT_COLOR HASH()5e5184

#include "template/header"

P()
With
AHERE(lambda-2, numbers) out of the way,
we can finally get to booleans—a simple yet quite atypical topic (at least in its Lambda Calculus rendering).
What I'm used to since my university times is truth tables for boolean ops.
Lambda Calculus (LC) booleans work in an absolutely different way.
Abandon tables ye who enters here.

P()
True and False, the fundamental booleans, are simple:
Just take two things and return the first (true) or second (false).

PRE()
true = λf.λx.f
false = λf.λx.x
PRECAP(True and false in Lambda Calculus)

P()
Coincidentally, CD(false) is the same function as 0 and null list (ooops, spoilers!)
Convenient for me as a C programmer used to CD(0 == NULL == false).

SECTION2(branching, Booleans are Branching)

P()
Numbers are
A("lambda-2#compositions", function compositions).
Akin to that, booleans are computation branching.
Meaning, boolean operations are kind of building trees out of given data.
For example, here's how
CD(not) operator looks like in LC:

PRE()
not = λp.p false true
PRECAP(not operator)

P()
So it takes a boolean and applies it to CD(false) and CD(true).
If the boolean is CD(true), then it picks the CD(false) branch, and vice versa.
A decision tree of sorts.
Some more elegant branching operations, CD(and) AMP() CD(or):

PRE()
and = λp.λq.p q p // If p is true, check q
or = λp.λq.p p q // If p is false, check q
// Alternative versions
and = λp.λq.p q false
or = λp.λq.p true q
PRECAP(Basic boolean operators)

P()
Again, branching operations instead of truth tables.

P()
And here's how
CD(XOR) looks like:

PRE()
xor = λp.λq.p (not q) q
PRECAP(XOR in Lambda Calculus)

P()
Simple and smart at the same time.

SECTION2(logic, Ah, Logic Again)

P()
But if we try to define operations like CD(nand) or CD(nor), our branching doesn't help much.
Here's how
CD(nand) might look like:

PRE()
nand = λp.λq.p (q false true) true
PRECAP(nand as a branching boolean)

P()
Not too bad, but it totally obviates the meaning of the operation.
It's just boolean juggling.
The truth tables I'm trying to avoid.
But the good thing about Lambda Calculus is that we can use the functions from before to fix that.
(Well, at least notation-wise—the compiled lambdas won't have names.
Still, most LC implementations allow for some CD(let)/CD(where) syntax, so I'm safe.)
This way derivative operations look much more meaningful:

PRE()
nand = λp.λq.(not (and p q))
nor = λp.λq.(not (or p q))
xnor = λp.λq.(not (xor p q))
PRECAP(Derivative operations expressed almost like their formulae)

P()
We built our primitives out of branching functions.
And then used them to define derivative operations.
An elegant solution mapping boolean logic directly to LC.
No truth tables, which is a win.
Now to actually using these operations:

SECTION2(if, Laconic If)

P()
Cutting it short, CD(if) is:

PRE()
if = λb.λt.λe.b t e
// Or even
if = λb.b
PRECAP(if conditional in Lambda Calculus)

P()
Booleans already select between branches.
So CD(if) doesn't need to do anything: just apply the boolean to both options.
And watch the magic happen.

P()
A person familiar with the regular CD(if) that chooses and runs only one branch:
"But that's dangerous: it's evaluating both branches. There might be side-effects."
There are no side-effects in LC, though.
And there is no "evaluation" in it either:
A("lambda-0#reduction", only reduction).
It's okay.

SECTION2(numbers, Applying Booleans to Numbers)

P()
That's the part I've been teasing you with in the previous post.
Finally, we can do comparisons and predicates on numbers.
Here's the most basic—checking for zero:

PRE()
iszero = λn.n (λx.false) true
PRECAP(Checking a number for zero in LC)

P()
Numbers are applying a function to a value.
If they do apply it even once (number > 0), return CD(false).
If they don't (number = 0), return
CD(true).
That simple.
And it doesn't matter if the number is arbitrarily big.
Applying CD((λx.false)) has no side-effects (again, there are none in LC).
So we can safely apply it as much as we wish to.

P()
Comparisons, the simplest ones:

PRE()
leq = λm.λn.iszero (sub m n)
geq = λm.λn.iszero (sub n m)
PRECAP(Less-than-or-equal and greater-than-or-equal predicates)

P()
The number A is greater than another number B if subtracting B from A yield non-zero.
(Notice that Church/Peano numerals are strictly non-negative.
Smallest value subtraction can yield is zero.)
And vice versa.
Having these, other predicates are easy:

PRE()
gt = λm.λn.not (leq m n)
lt = λm.λn.not (geq m n)
eq = λm.λn.and (leq m n) (leq n m)
PRECAP(Other number predicates)

P()
Maps the mental model perfectly: number A is greater if it's not less than or equal than B.
And equality (this is slightly tricky) is when the numbers are both less than or equal than each other.
Because if both are, then both must be the same number.
Otherwise one of these will be greater and the condition will be false.

SECTION2(using, Finally, Using Booleans!)

P()
The simplest example for usage of all these operators might be minimum and maximum functions:

PRE()
min = λm.λn.if (lt m n) m n
max = λm.λn.if (lt m n) n m
PRECAP(min and max in LC)

P()
I might've used
CD(leq) here, but the behavior stays the same.
It doesn't matter which of the two we pick—both are valid numbers.
If they are the same number, they are fully
A("lambda-0#equivalence", β-equivalent)
and picking either is fine.
So it does not matter whether there's
CD(leq) or CD(lt).

SECTION2(up-next, Up Next: Consing Up The World)

P()
With that, I'm ready to conclude the booleans post.
What happened here was:

ULI Booleans as decision trees.
LI  Branching logic operations.
LI  Perfect mapping from logic to LC for derived ops.
LI  Number predicates.
LI  And, finally, conditional operations on numbers (or any data, really—the idea is the same.)
END(UL)

P()
With basic data types, numbers and booleans, one can already encode a lot.
But there's a point at which one wants aggregate data types.
Like cons trees/lists/structures.
That's the next post topic, so byte (sic) your nails until then.

#include "template/feedback"
#include "template/footer"
