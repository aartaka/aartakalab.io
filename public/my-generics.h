#define TITLE How I Write Generics
#define DESCRIPTION "With great generic functions comes great responsibility of making them readable and maintainable."
#define IMAGE assets/my-generics.png
#define IMAGE_ALT "Soft colors thumbnail with 'How I Write My Generics' text in the center.\
'Generics' is repeated and self-interleaved multiple times.\
Text is written as if with uncertainty—it's wiggling a bit. \
In the corners there's attribution to 'Artyom Bologov' and address 'aartaka.me'."
#define LIGHT_COLOR HASH()E3D8F1
#define LIGHT_ACCENT_COLOR HASH()511730
#define DARK_ACCENT_COLOR HASH()E8B0C8

#include "template/header"


IMG(IMAGE, IMAGE_ALT)

P()
Generic functions are beautiful as an idea.
But they are also somewhat ugly as a syntax.
I use them a lot, so I know what I'm talking about.

P()
My (ab)use of generics led to a certain programming style.
This style, I conjecture, ends up in more readable and correct functions.
Although it's up to you whether to believe me.

P()
This post will be structured around what generics are.
This overview will explain intuition behind my style of writing generics.
With examples and reasoning, of course.
In the end, I'll share a small macro that nicely abstracts away some of my conventions.

P()
Don't expect some eye-opening experience,
it's mostly about formatting and ordering of generic options.
Still, helps in setting up style guides.

SECTION2(structure, Structure of Generics)

P()
Generics are

ULI functions
LI  with multiple methods,
LI  type dispatch,
LI  and metadata (like documentation and method combinations.)
END(UL)

P()
Let's go through these one by one.

SECTION3(functions, Generics Are Ugly Functions)

P()
That's the reason a lot of people prefer
CD(defmethod) over CD(defgeneric): Generics are ugly.
They require a lot of keywords and don't even look like functions.
Even though they are ones.

P()
So my style aims to bring generics as close to regular functions as possible.
Which means putting documentation and declarations first, and methods last
(taken from
A(https://github.com/aartaka/cl-blc, CL-BLC)):

PRE(lisp)
(defgeneric compile (expr AMP()optional stack)
  (:documentation "Compile Lispy EXPR into binary lambdas...")
  (:method ((expr list) AMP()optional stack)
   HASH()|...|HASH()))
PRECAP(Making generics look a bit more like functions)

P()
Why not just use CD(defmethod)?
Because defining a generic via CD(defmethod) doesn't attach source location info.
So CD(xref)/CD(edit-definition)/CD(swank:find-source-location) on generic often gets confused by this.
Define your generics with CD(defgeneric).
And enjoy the increased transparency it brings!

P()
A note on arglists: it's useful listing all the keyword/optional arguments in the generic definition.
Instead of splitting these into separate methods.
This way, even the dumbest tooling (and most inattentive reader) knows what keywords/optionals to expect.
Yes, SLIME/SLY infers the arglist somehow.
But there's no such a thing as too much caring for the user.

HYPERTEXTONLY(</section>)

SECTION3(methods, Generics Have Methods)

P()
That's the main feature of generics: they have methods.
Generics dispatch over argument types to call most relevant method(s).

P()
While most methods are born equal, some have a generic-related role.
Like termination/dispatch/default ones.
These usually represent the empty state or some stub for the actual implementation to replace.

P()
One can put all the methods as CD(:method) options in generic.
One can also define them all as separate CD(defmethod)-s.
The former keeps things together in one place.
The latter gives methods more metadata and pins them to certain source locations.

P()
My preference is to define termination/default methods in generics.
And other methods on their own.
This way, one can immediately see the default behavior of the generic.
And only after that can they get to the actual implementation/extension with CD(defmethod)-s.

P()
Here's a bit of (old and somewhat orthogonal to the style I'm suggesting, but nonetheless exemplary in default method case)
code from
A(https://nyxt.atlas.engineer/article/njson.org, NJSON):

PRE(lisp)
(defgeneric decode-from-stream (stream)
  (:method (stream)
    (declare (ignore stream))
    (signal 'decode-from-stream-not-implemented))
  (:documentation "Decode JSON from STREAM..."))
PRECAP(Putting only the termination/base case method in generics)

P()
I also tend to write all the other methods
in terms of transforming data to a proper format and calling next/default method on it.
This way, most of the actual logic is concentrated in one method.
While other methods provide nice wrappers and validators for it.

HYPERTEXTONLY(</section>)

SECTION3(type-dispatch, Generics Dispatch Over Types)

P()
This is somewhat tautologic with methods, but hear me out:
Generics dispatch over almost all built-in and user-defined types.
Including the CD(eql) types.
And this type-exact nature makes them extremely composable.
One can encode a lot in argument lists.

P()
It's easy enough to re-implement
CD(documentation)-like control args with CD(eql) specializers:

PRE(lisp)
(defmethod coerce ((term list) (type (eql SQUOTE()list)) AMP()optional inner-type final-type)
  HASH()|...|HASH())
(defmethod coerce ((term list) (type (eql t)) AMP()optional inner-type final-type)
  HASH()|...|HASH())
PRECAP(Using type dispatch (including eql types) with generic specializers)

P()
One can also dispatch over type hierarchies, like CD(list)/CD(cons)/CD(null).
This way, typed edge cases are all covered without extra branching in the actual methods.

P()
So yes, I abuse this power of specializers for common good.
And you should too!

DETAILS(But method dispatch is expensive!)
No.
One can use something like CD((declare (optimize speed))) in generic declaration.
This should get one 90% there.
And the rest is up to implementation magic
(which is pretty good even by default, at least on SBCL) to figure out.
Don't over-optimize, focus on designing a clear contract in your generics!
END(DETAILS)

HYPERTEXTONLY(</section>)

SECTION3(meta-data, Meta(data): combinations and classes)

P()
Finally, a bit of cryptic stuff: method combinations and generic classes.
You'll be surprized with how mendable the whole system is.
For example, here's a simple method combination
A(https://github.com/aartaka/trivial-inspect, Trivial Inspect)
uses to reorder and index returned properties:

PRE(lisp)
(defun reverse-append-index (AMP()rest lists)
  (let ((fields (remove-duplicates (reduce NUM()'append (nreverse lists))
                                   :key NUM()'first
                                   :from-end t)))
    (mapcar NUM()'cons (field-indices fields)
            fields)))

(define-method-combination reverse-append-index
  :identity-with-one-argument t)

(defgeneric fields (object AMP()key AMP()allow-other-keys)
  (:method-combination reverse-append-index)
  (:documentation "Return a list of OBJECT fields to inspect..."))
PRECAP(Simple method combination to reorder the results of method calls)

P()
The logic is:

ULI Get the list of properties returned by other methods.
LI  Reverse the list so that most specific methods come last (and thus the default-ish/universal properties come at the top)
LI  And then index it with nice and meaningful numbers.
END(UL)

P()
I might've used an
CD(:around) method to sort things.
A(https://funcall.blogspot.com/2024/04/you-may-not-need-that-around-method.html?m=1, But you may not need that around method).
And I prefer to not touch auxiliary methods.
Downstream user might want to use CD(:around) to override generic behavior.
So custom method combination it is!

P()
Generic function and method classes are even more obscure.
I didn't encounter a use-case for them yet
(except for CD(command) class in Nyxt.)
But once there's such a case, only these classes can save the situation, so better be prepared!

P()
I tend to put this meta-data (pun intended) before or after the docstring, so that it resembles regular functions.
Functions allow declarations after docstring, after all.
But you can see (in the code listing above) that I'm somewhat inconsistent in that.
Still, put metadata/docs at the top and implementation at the bottom—will be clearer this way!

HYPERTEXTONLY(</section>)

SECTION2(syntax, A New Syntax, Maybe?)

P()
But hey, it's much harder to write out generics the way I'm suggesting, even if it's actually useful!
Luckily, we have macros in CL.
One can define a macro to shorten generics and typical use patterns like mine.
A(https://github.com/atlas-engineer/nclasses, define-generic from Nclasses) does that
(Do check out Nclasses version, it clocks at 100 lines of useful macro code.
I've gone to great length to make it work nicely!)
Or use this simplistic macro from my
A(https://github.com/aartaka/graven-image, Graven Image) project:

PRE(lisp)
(defmacro define-generic (name (AMP()rest method-args) AMP()body (documentation . body))
  (check-type documentation string "Documentation string")
  `(let ((generic (defgeneric ,name (,@(mapcar NUM()SQUOTE()first (mapcar NUM()'uiop:ensure-list method-args)))
                    (:method (,@method-args)
                      ,@body)
                    (:documentation ,documentation))))
     (setf (documentation (fdefinition ',name) t) ,documentation)
     (ignore-errors
       (setf (documentation ',name 'function) ,documentation))
     generic))
PRECAP(define-generic, an opinionated (and simplistic) generic-writing macro inspired by Nclasses)

P()
Used as

PRE(lisp)
(define-generic (setf function-lambda-expression*) ((value list) function AMP()optional force)
  "Setter for `function-lambda-expression*'."
  (declare (ignorable force))
  (let ((name (if (symbolp function)
                  function
                  (function-name* function))))
    (function-lambda-expression* (compile name value))))
PRECAP(Example of define-generic use)

P()
Lots of niceties there:

ULI Mandatory docstring.
LI  Method lambda list with type specializers in place of generic arglist to not repeat myself.
LI  Default method as the only method in body.
LI  No need for CD(:documentation) or CD(:method) keywords—they are implied.
END(UL)

P()
Which all compounds to a good and readable generic definition.
It also looks almost like CD(defmethod) without sacrificing any benefits of generics.
It's not some generic-writing Swiss knife, but it gets one 90% there.
Leaving the complex cases to Nclasses or whatever else one prefers.

SECTION2(wrapping-up, Wrapping Up: Write Generics Like I Do)

P()
Just kidding!
I'm not forcing you to any type of style.
You might even ignore generics altogether.
But, hopefully, this post has shown you some useful techniques of working with generics.
Here's a full reference for how I suggest writing generics.

PRE(lisp)
(defgeneric generic-name (arg1 arg2 AMP()optional opt)
  (:documentation "Generic function docs
See https://nyxt.atlas.engineer/article/lisp-documentation-patterns.org for additional context")
  (:generic-function-class meta-data-meta-class)
  (:method-class meta-data-method-class)
  (:method-combination meta-combination)
  (declare (optimize (speed 3)))
  (:method ((arg1 list) (arg2 (eql SQUOTE()t)) AMP()optional (opt 8))
    "Simple default/termination method."
    t))
(defmethod generic-name ((arg1 cons) (arg2 null) AMP()optional opt)
  "Implementation method documentation.
Not strictly necessary, but nice for extra implementation details."
  NUM()|...|NUM())
PRECAP(Reference for my style of generic writing)

P()
But this full form is unlikely to ever get useful, because you usually don't need that much meta.
Here's a more realistic reference:

PRE(lisp)
(defgeneric generic-name (arg1 arg2 AMP()optional opt)
  (:documentation "Generic function docs
See https://nyxt.atlas.engineer/article/lisp-documentation-patterns.org")
  (:method ((arg1 list) (arg2 (eql SQUOTE()t)) AMP()optional (opt 8))
    "Simple default/termination method."
    t))
(defmethod generic-name ((arg1 4) arg2 AMP()optional opt)
  "A less simple method."
  (generic-name (list arg1) t opt))
PRECAP(A simplified reference for my style of generic writing)

P()
I hope that I also motivated you to try more of generics in your next project.
A("lisp-design-patterns#protocols", and maybe even use them as an architectural device!)
Thanks for following me up until here!

#include "template/feedback"
#include "template/footer"
