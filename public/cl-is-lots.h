#define TITLE Common Lisp Is Not a Single Language, It Is Lots
#define DESCRIPTION "Lisp is an ambiguous category. But Common Lisp isn't, right? It's a restricted self-sufficient language, after all."

#include "template/header"

BQ()There's no such thing as C++. C++ is a heap of sloppily stiched languages. Everyone uses a different "C++", picking features from either of these.
BQCAP(Vasily Gerasimov, conversation, rephrased)

P()
Being a part of the Lisp community, I often see a talk of "Lisp" the language.
A(https://www.reddit.com/r/lisp/comments/1c4pls3/what_do_they_mean_by_lisp/, Like here, for example).
What is meant by "Lisp" is often uncertain.
But Common Lisp seems to be the most worthy contender for the title.
It's "Common", after all.
One true Lisp to rule them all.

P()
But Lisp is a family of languages.
"Lisp" might mean anything else—Scheme(s), Clojure, original LISP, and any of its descendants.
So there's no "The Lisp", and implying it is misleading.

P()
But there is "The Common Lisp", right?
It's a standard well-defined language with a name to it.
It's one coherent thing, one can say.

P()
It's not.
In this post, I'll try to show how many languages there are in CL.
Hopefully helping one to understand why saying "Common Lisp is bad"
or "Common Lisp is beautiful" is always playing with the (mis)conception of CL others have.

P()
I'll list the example code in each of these.
The example snippet will be a simplistic implementation of
A(https://geminiprotocol.net/docs/gemtext.gmi, Gemtext) parser.
Don't expect super correct and beautiful code.
I'm doing it for the sake of example, after all.

SECTION2(the-language, The Language?)

P()
Probably the biggest thing that "proves" Common Lisp integrity is the book.
"Common Lisp The Language".
Funnily enough, Wikipedia says

BQ()Common Lisp the Language is a reference book by Guy L. Steele about a set of technical standards and programming languages (sic) named Common Lisp.
BQCAP(Wikipedia saying that Common Lisp is a set of languages)

P()
But let's see whether there is The Language in the book.
The book is 1000 pages long.
And I'm not able to focus on anything longer than 30 pages.
So I'll take a different direction: that of a programmer exploring the API.
The API of CD(trivial-cltl2), the CLtL portability library.
Here's a listing of symbols that
CD(trivial-cltl2) exports in addition to standard ones:

PRE()
(:export HASH():compiler-let
         HASH():variable-information
         HASH():function-information
         HASH():declaration-information
         HASH():augment-environment
         HASH():define-declaration
         HASH():parse-macro
         HASH():enclose)
PRECAP(Trivial CLtL2 symbols)

P()
And that's it.
Not much.
CLtL2 is the same as ANSI CL.

P()
But notice what the list is lacking: the notion of objects.
So CLtL2 (as both the book and the language), although including CLOS and other niceties discussed below...
Is not object-oriented at its initial core.
And all the mentions of CLOS etc. are only happening near the end of the book.
An epilogue.
Appendix.
Erratum.

P()
So we have our first language (out of many): CLtL.
A language built out of variables, functions, and maybe structures.
Like Scheme, but batteries included.
Small and neat nonetheless.
Here's how our parser could look in The Language:

PRE()
;; We don't necessarily need structs if we use lists.
(defstruct element
  (type :paragraph :type keyword)
  (content "" :type string))

(defun parse-elements (stream)
  (if (not (peek-char nil stream nil nil))
      '()
      (cons
       (case (peek-char nil stream)
         (HASH()\= (let ((line (read-line stream)))
                ;; Yes, I know links have text, but bear with me.
                (if (uiop:string-prefix-p "=> " line)
                    (make-element :type :link :content (subseq line 3))
                    (make-element :content line))))
         (HASH()\> (make-element :type :quote :content (subseq (read-line stream) 2)))
         ;; Yes, headings have level. But oh well.
         (HASH()\HASH() (make-element :type :heading :content (subseq line 2)))
         (t (make-element :content (read-line stream))))
       (parse-elements stream))))
PRECAP(Example Gemtext parser in core Common Lisp)

P()
No fancy DSLs, no Turing-complete APIs, no OOP.
Speaking of which, another Common Lisp language:

SECTION2(clos, Common Lisp Object System)

P()
OOP is but a one approach to building Lisp software.
There are CL codebases built entirely out of plain functions.
Even the ones built entirely in a Scheme-y functional style (I worked with some.)
There are others built out of objects and methods on them.
There are ones that are built around
A("lisp-design-patterns#protocols", generics and protocols).
And it's quite hard to work with either if one's typical style is not the same as that of the codebase.

P()
So here goes our first non-CLtL language: CLOS.
Defining classes and methods on them:

PRE()
(defclass base-element ()
  ((content :type string
            :initarg :content)))

(defclass paragraph-element (base-element) ())
(defclass quote-element (base-element) ())
(defclass link-element (base-element) ())
(defclass heading-element (base-element)
  ((level :type integer
          :initarg :level)))

(defmethod parse-elements-clos ((stream stream))
  (if (not (peek-char nil stream nil nil))
      '()
      (cons
       (case (peek-char nil stream)
         (HASH()\= (let ((line (read-line stream)))
                (if (uiop:string-prefix-p "=> " line)
                    (make-instance 'link-element :content (subseq line 3))
                    (make-instance 'paragraph-element :content line))))
         (HASH()\> (make-instance 'quote-element :content (subseq (read-line stream) 2)))
         (HASH()\HASH() (make-instance 'heading-element :level 1 :content (subseq (read-line stream) 2)))
         (t (make-instance 'paragraph-element :content (read-line stream))))
       (parse-elements-clos stream))))

(defmethod parse-elements-clos ((string string))
  (with-input-from-string (s string)
    (parse-elements-clos s)))
PRECAP(CLOS-heavy Gemtext parser)

P()
The code is more verbose than that in The Language.
Yet it's also more reliable and introspectable.
All these classes add type checking and convenient slot access with e.g. CD(with-slots).
The behavior is more overridable with CD(:around) methods.
And the code can be extended with more classes.

P()
I must admit that I cheated by reusing the code from above.
So the parser stays kind of the same procedural piece of code.
It's enhanced with more types and MOP introspection.
But it still is procedural, because OOP
(not the CLOS, but the general idea)
is merely a facade over procedural programming,
encapsulating the procedural behavior into classes.
One can implement it with methods and character dispatch.
But that's no longer a CLOS-y code, it's...

SECTION2(generics-and-protocols, Generics And Protocols)

P()
Generics are part of CLOS, right?
Why make them a separate "language" then?
My reasoning is: one can program anything in them without touching classes.
And if you can, then it's a separate language alright.

P()
Generics shine the most when there's a library intended for extension.
Like lots of libraries by Shinmera or Robert Strandh, where there's

ULI A general protocol.
LI  And implementation-specific extensions to it with the actual behavior.
END(UL)

P()
Here's a weird version of the Gemtext parser with generics:

PRE()
(defgeneric parse-elements-generic (stream state char)
  (:method ((stream stream) (state t) (char null))
    state)
  (:method ((stream stream) (state null) (char null))
    (nreverse
     (parse-elements-generic stream nil (peek-char nil stream nil nil)))))

(defmethod parse-elements-generic ((stream stream) (state list) (char (eql HASH()\=)))
  (let ((line (read-line stream)))
    (parse-elements-generic
     stream
     (cons (if (uiop:string-prefix-p "=> " line)
               (list :link (subseq line 3))
               (list :paragraph line))
           state)
     (peek-char nil stream nil nil))))

(defmethod parse-elements-generic ((stream stream) (state list) (char (eql HASH()\HASH())))
  (parse-elements-generic
   stream
   (cons (list :heading (subseq (read-line stream) 2))
         state)
   (peek-char nil stream nil nil)))

(defmethod parse-elements-generic ((stream stream) (state list) (char (eql HASH()\>)))
  (parse-elements-generic
   stream
   (cons (list :quote (subseq (read-line stream) 2))
         state)
   (peek-char nil stream nil nil)))

(defmethod parse-elements-generic ((stream stream) (state list) (char character))
  (parse-elements-generic
   stream
   (cons (list :paragraph (read-line stream))
         state)
   (peek-char nil stream nil nil)))
PRECAP(Generic-heavy Gemtext parser)

P()
That's where the example stops being useful.
Generic style is nice, but this piece of code is by no means representative.
I must've used classes for elements.
I must've made a simpler protocol.
I must'ven't used lists for state.
A(https://yogthos.net/posts/2022-12-18-StructuringClojureApplications.html, I might have used state machine to drive the parsing).

P()
But even this ugly code has the benefits of generics:

ULI It is easy to extend: just dispatch over the new character.
LI  It provides a strict protocol with type checking—if something goes wrong, a CD(no-applicable-method) signals and shows the problem.
LI  It makes the behaviors compose nicely and puts recursion termination into the method of its own.
END(UL)

P()
Try generics for your new projects.
They are nicer than I represented them here.
Speaking of "nicer", how about some syntactic sugar?

SECTION2(loop, Loop Macro)

P()
Using CD(loop) is always a risk: some Lispers love it, and some hate it.
But one thing is undeniable: it's powerful and reads like English.
Well, given that it's used right.

P()
And no, it's not just "another iteration construct".
CD(loop) has:

ULI Symbol binding.
LI  Conditionals.
LI  Iteration over (almost) all the standard data types.
LI  Data aggregation.
END(UL)

P()
So CD(loop) is quite a free-standing entity.
I've proof:
A(https://github.com/aartaka/advent-of-code-in-cl-loop, I completed two weeks of Advent of Code in it).

P()
So let's try implementing this simple Gemtext parser as one huge loop:

PRE()
(defun parse-elements-loop (stream)
  (loop for char = (peek-char t stream nil nil)
        while char
        when (eql HASH()\= char)
          collect (list :link (subseq (read-line stream) 3))
        else when (eql HASH()\> char)
               collect (list :quote (subseq (read-line stream) 2))
        else when (eql HASH()\HASH() char)
               collect (list :heading (subseq (read-line stream) 2))
        else collect (list :paragraph (read-line stream))))
PRECAP(Gemtext parser as a single CD(loop))

P()
Beautiful, isn't it?
Ugly, isn't it?
Love it or hate it, it's a nice readable syntax.
And you can advertise this small language to your C-family friends.
"This is The Lisp, try it!"
I did this once, and it kinda worked...

SECTION2(clojure, Clojure?!)

P()
The legend goes: Rich Hickey made Clojure after being irritated by ABCL.
So Clojure feels (after programming some sufficiently big projects in it)...
Lispy (sorry for the linguistic abuse here).
Like an opinionated standard lib over CL.
Some reader macros here and there.
Square and curly braces.
Some nicer functional programming constructs.
More consistency and less burden of the past.
A fancy OOP system based on interfaces.

P()
We could've had that in CL.
And there
A(http://cl21.org/, are projects)
A(https://github.com/ciel-lang/CIEL, enabling a "nicer CL"),
A(https://github.com/ruricolist/cloture, or even outright implementing Clojure in CL)!
Many CL programmers squint at them.
These extensions are going "too far" making a different language out of CL.
Yes, yes, yet another Common Lisp language.

SECTION2(format, Notable Mention: Format)

P()
CD(format) function is an enormous text formatting DSL with

ULI Conditionals.
LI  Iteration.
LI  Pretty-printing.
LI  Recursive function calls.
LI  And something I surely forgot to mention.
END(UL)

P()
Here is how one of my CD(format) strings looks like:

PRE()
"~s (~a bit~:p):
HASH()b~b, HASH()o~o, HASH()x~x~
~{~AMP()Universal time: ~2,'0d:~2,'0d:~2,'0d ~
~[~;Jan~;Feb~;Mar~;Apr~;May~;Jun~;Jul~;Aug~;Sep~;Oct~;Nov~;Dec~] ~
~a~[th~;st~;nd~;rd~:;th~], year ~a.~} ~
~{~AMP()Approximate UNIX time: ~2,'0d:~2,'0d:~2,'0d ~
~[~;Jan~;Feb~;Mar~;Apr~;May~;Jun~;Jul~;Aug~;Sep~;Oct~;Nov~;Dec~] ~
~a~[th~;st~;nd~;rd~:;th~], year ~a.~}"
PRECAP(A regular format string, right?)

P()
Horrible, but it gets the job done.
And it's much more effective than writing 100+ lines of printing code.

P()
So while I'm hesitant about calling
CD(format) another Common Lisp language...
It is alien enough to be one.

P()
See also:
A(http://cs-www.cs.yale.edu/homes/dvm/format-stinks.html, a case against format!)

SECTION2(conclusion, Conclusion)

P()
There's no Common Lisp The Language.
There's a set of languages sloppily stitched together.
Well, less sloppy than C++ anyway.
Agree or disagree, these are quite orthogonal to each other:

ULI Core Scheme-y Common Lisp.
LI  CLOS.
LI  Generics.
LI  CD(loop) macro
LI  Clojure and Scheme?!
LI  CD(format) strings.
END(UL)

P()
A lot of sub-languages for one (sorry) "language", right?

P()
(Drafts of this post also included sections on namespaces and OS interfaces.
I decided to drop them to save space and because they weren't adding much.
Lots of languages!)

#include "template/feedback"
#include "template/footer"
