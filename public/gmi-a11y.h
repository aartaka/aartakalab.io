#define TITLE Gemtext Is Not Accessible
#define DESCRIPTION "Gemtext is a minimalist markup/hypertext format initially from Gemini network. It's intended to be a lightweight, easy to learn, and accessible language... But at least the latter is not true with the current state of things."
#define IMAGE assets/thumbnail-1E4147-C1DE9F-AA2E00-9C7CA5.png
#define DARK_COLOR HASH()1e4147
#define LIGHT_COLOR HASH()c1de9f
#define LIGHT_ACCENT_COLOR HASH()e03c00
#define DARK_ACCENT_COLOR HASH()ff6933

#include "template/header"

P()
Gemini and Gemtext are powerful ideas.
If not as the actual implementation/praxis, then at least as a source of inspiration.
How cool could it be to have a markup/hypertext language that doesn't allow any inline decorations?
Only blocks and lines.
The thought flows unhindered through the paragraphs and quotes.
Minimalist writing.
By design.

P()
But Gemtext minimalism is both a blessing and a curse.
And the curse is that it's actually inaccessible.
Many pages (including mine) break the advertized accessibility by using the elements in the wrong way.
And the language/markup itself doesn't make it easier.

P()
Disclaimer: I'm not as much pointing out flaws in Gemtext
(although the accessibility situation is sympthomatic of systemic flaws in it)
as showing that the current practice of Gemtext use is inaccessible.
It's the actual use of markup language that's broken, no matter what the core language claims.

SECTION2(pre, Preformatted Blocks and Alt Text)

P()
The biggest thing that's broken about Gemtext is preformatted text blocks.
A(https://gitlab.com/gemini-specification/gemini-text/-/issues/12, And I am not the first person to notice that).
Protocol spec lists screen reader accessibility as use-case for block alt text.
But there's no way to guess what one would write in there.
And the current frequent practice is either

ULI Leaving alt text blank.
LI  Or putting programming language tag (akin to Github-Flavored Markdown ones) there.
END(UL)

P()
Neither of these helps the screen reader to guess whether to read the block or not.
Which means it is not accessible at all.
We're back to the age of HTML1 here.
Especially with the omnipresent ASCII art used for page decoration.
At least it's not GIFs, I guess?

P()
There are multiple ways to change the status quo:

ULI Suggest to always make alt text a language/content code, like "javascript" or "ascii art".
LI  Add more structure to alt text, like (unofficial yet ubiquitous) CD(lang=javascript) and CD(LT()figcaption>) in HTML.
LI  Clarify in which way the alt text is intended to be processed by screen readers.
LI  Or even introduce a separate "caption" line/block to attach to preformatted blocks.
END(UL)

P()
Notice that all of these are suggestion for the spec, and not for the users.
I'm not sure what to recommend to users.
Caption your preformatted blocks?
At least with something?

SECTION2(lists, Lists Abuse)

P()
Lists are probably the second most decorative and misused type of lines after preformatted ones.
They are (mis)used to

ULI Indent the content.
LI  Add "captions" to pre-s and quotes
A(index.gmi, (see the index.gmi page on my blog for an example.))
LI  Or to make heading-like paragraph breaks
A(projects.gmi, (see project.gmi))
END(UL)

P()
All of these go beyond what lists semantically are.
Yet all of these are present in many Gemtext documents.
We need to do better.

P()
One solution that could work is the one already suggested above:
Add a "caption" line to cover the list-after-blockquote use-case.
Other use-cases are not really fixable:
Indentation is a matter of writer wanting a weird display and going against Gemini philosopy ("styling is on user");
And heading-like use will benefit from real headings instead.

SECTION2(aux, Paragraphs as Auxiliary Information)

P()
Paragraphs are often used to expand on the lines preceding them.
As longer explanation for list bullet points, for example.
I'm misusing them this way, at least.

P()
Having caption/aux line could fix that too?

SECTION2(is-that-it, Is That It?)

P()
These are the biggest pieces of Gemtext praxis that I've seen that are inaccessible.
And semantically broken too.
Not that many breakages, but all too frequent on real pages.

P()
The definitive suggestion that could cover most of these cases is adding one more line.
"Aux" or "caption" line.
Basically what CD(LT()figcaption>) does to HTML5.
Something like:

PRE(gemtext)
> A blockquote
? Author of the quote, year XXXX
PRECAP(Caption line example use)

P()
But I realize that
A(https://gitlab.com/gemini-specification/gemini-text/-/issues/, Gemtext is not really changeable at this point.)
So we're stuck with inaccessible and semantically broken content?

#include "template/feedback"
#include "template/footer"
