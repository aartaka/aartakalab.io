#define TITLE Escaped String Regex
#define DESCRIPTION "Parsing strings with regex is futile, right? No."
#define IMAGE assets/paren-regex.png
#define IMAGE_ALT "A banner on light gray (smoke shade) background."
#define LIGHT_COLOR HASH()e0e0e0
#define DARK_COLOR HASH()202020
#define LIGHT_ACCENT_COLOR HASH()008700
#define DARK_ACCENT_COLOR HASH()ff8700

#include "template/header"

P()
So I'm into building languages, preprocessors, and transpilers.
With ed and regex substitution.

P()
One of the most fundamental and regex-hostile problems in programming languages is string parsing.
Especially so—strings with escaped quotes.
You need a proper parser for it, right?
As it turns out, no:

PRE(sed)
g/"\(\(\(\\"\)*[^"]*\)*\)"/s//{{{\1}}}/gp
PRECAP(Escaped string parsing regex)

P()
The way this POSIX BRE works is:

ULI Match an arbitrary number of escaped quote CD(\\") sequences.
LI  Match an arbitrary number of non-quote chars.
LI  Repeat indefinitely until the string is enclosed in non-escaped quotes.
LI  And then replace the quotes with (supposedly) more recognosable marker.
END(UL)

P()
So yes, it's possible to parse programming languages with regex.
AHERE(paren-regex, Including Lisp-likes)
and strings!

#include "template/feedback"
#include "template/footer"
