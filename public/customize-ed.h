#define TITLE Customizing ed(1)
#define DESCRIPTION "ed is too simple to be customizable, right? \
Maybe it is not customizable, but the environment around it is!"
#define IMAGE assets/customize-ed.png
#define IMAGE_ALT "A bright thumbnail with jarring colors. \
In the center, it says 'ed(1)' in bright white. \
Inside the parentheses, a barely visible dim 'custom' is appended to 1. \
There are attributions to 'aartaka' and 'Artyom B.' in the corners."
#define LIGHT_COLOR white
#define DARK_COLOR HASH()333333
#define LIGHT_ACCENT_COLOR HASH()660000
#define DARK_ACCENT_COLOR HASH()ffcccc

#include "template/header"

PIC(IMAGE, assets/customize-ed-dark.png, IMAGE_ALT)

P()
I like
A(https://www.man7.org/linux/man-pages/man1/ed.1p.html, ed(1))
for its simplicity and hidden power.
But, as an Emacs user, I miss the endless hours spent on customization of my editor.
I want to customize ed too!
A(https://bsd.network/@ed1conf/113437773862270913, Maybe I can even spend more time customizing ed than actually using it)!
This post is my attempts at making ed more personal and accomodating.


SECTION2(implementation, Picking an Implementation)

P()
BSD, GNU, Mac, Plan 9, or
A(https://github.com/slewsys/ed, one of the dozens of ed forks)?
Take your pick and enjoy some killer features these might offer!
And customize your implementation, if it gives you some customization space.

P()
(No, I'm not considering patches to ed itself.
Though that might be quite fun!)

P()
I'm using GNU ed, so my non-portable customizations will be specific to it.

SECTION2(environment, Picking an Environment)

P()
You can run ed with a line printer.
You can run it in a terminal.
You can run The Standard Editor™ in a graphical environment.
A(https://github.com/ryanprior/ed-mode, You can run it in Emacs).
You can run it on your phone!

P()
Depending on the environment you use, customizations might differ.
I'm mostly using ed inside terminal emulators, like suckless st and Emacs (e)shell.
My non-portable customizations will be specific to these.

SECTION2(flags, GNU ed flags)

P()
GNU ed provides some flags to pre-configure ed:

DL(-v or --verbose)
To toggle more CD(H)-elpful messages on.
DD(-G or --traditional)
For more conservative ed experience.
DD(-E or --extended-regexp)
To use AID(the-regex, extended-posix, POSIX EREs).
I don't use them for the sake of portability, but they are much more consistent and featureful than default BREs!
END(DL)

P()
Other options are less useful, but
A("https://www.gnu.org/software/ed/manual/ed_manual.html#Invoking-ed", do check them out anyway).
Here's how my customization started:

PRE(sh)
ed -vG file
PRECAP(GNU ed invocation)

SECTION2(prompt, The Prompt)

P()
Some people may say there's only one customization point in ed: its prompt.
You can set the prompt text to... any text!
Want a Christmas vibe? Set it to Christmas tree!
Want to show some love? Use purple heart!
Want to pretend your ed is ex? Set the prompt to a mere colon!

SECTION2(ansi, ANSI Escapes)

P()
Setting the prompt to random chars is fun and I tried many!

ULI CD(:) as an ex homage.
LI  CD(¶) for creative writing.
LI  CD(--) because it looks cool and CLI-y.
LI  CD(ë) because I'm artYOm.
END(UL)

P()
But we can use some non-portable hacks, like ANSI sequences!
I like indian red, so I colored my prompt black on red:

PRE(sh)
ed -vGp "[30;48;5;210mPROMPT[0m"
PRECAP(Indian red prompt (code might not display properly in the browser))

You can go much further than that, though!
It's just that I'm relatively boring.
Here's a comprehensive listing of
A(https://gist.github.com/fnky/458719343aabd01cfb17a3a4f7296797, ANSI Escape Codes)
for your prompt engineering 🥁

SECTION2(contextual, Contextual Prompt)

P()
When I moved to Arch, the default Bash prompt was quite nice.
User, hostname, and the current directory name: CD([aartaka@paranoidal ~]$).
I want to have this directory thing in ed too!
So I wrote an ed script (the most obvious thing to do, right?) to format the prompt:

PRE(ed)
r !pwd
HASH() Replace HOME with tilde
g|^/home/aartaka/*$|s//~/
HASH() Remove prefix dir
g|/|s/.*\///
s/$/:/
p
Q
PRECAP(Script to get a prettified directory name as a prompt)

P()
With it, my prompt looks like
EXCEPTHYPERTEXT(CD(Downloads:))
HYPERTEXTONLY(<span style="background-color: HASH()f08080; color: black; font-family: monospace;">Downloads:</span>)

SECTION2(readline, Using Readline)

P()
That's when ed fundamentalists (like me) will reach for their pitchforks.
ed should not be visual and immediate.
You're not supposed to edit your commands.
You need neither history nor completion.
Your prompt must be static.
Filtering the printouts is extraneous.
Right?

P()
Still, I like CD(rlwrap) and how it enhances everything.
So here's my incantation for Readline-enabled ed:

PRE(sh)
rlwrap -crq "/|" -H "$HOME/.ed_history" ed -vGp "[30;48;5;210m$dirname:[0m" file
PRECAP(Readline-enabled ed call with custom quotes, history, and file completion)

SECTION2(copy-paste, Copy-Paste?)

P()
GNU ed has CD(x/y) commands for line copy-pasting.
But what about copy-pasting non-line content?
We can reuse terminal emulator-provided bindings, like Shift-Insert!
Though you might say I'm cheating by extending the scope.
Still, if it works it works.

SECTION2(localization, Localization)

P()
A("https://www.man7.org/linux/man-pages/man1/ed.1p.html#ENVIRONMENT_VARIABLES", POSIX specifies that ed should pay attention to CD(LC_*) variables).
So I can make my ed output errors in Russian.
I'm not going to do that, but the mere possibility adds some customization points.

SECTION2(frontend, Using a front-end like vi)

P()
No, that's too much.
A(https://github.com/aartaka/emvi, Even though I have a Lisp-oriented ex/vi config).
Too much.
No.

P()
But!
I'm thinking of writing my own front-end for ed.
Maybe using HTML&JS, because that's the most portable GUI there is.
And ed is quite suitable for the editing back-end role!

SECTION2(reuse, Reusing My Customizations)

P()
Time to wrap this all into a script.
So that I (and you) can reuse this configuration.
And improve it.
Here's the script: A(scripts/aed, scripts/aed).
(For "aartaka's ed", if you haven't guessed already.)

#include "template/feedback"
#include "template/footer"
