#define TITLE Text UIs != Terminal UIs
#define DESCRIPTION "TUI is an ambiguous term, meaning either of 'terminal' or 'text' user interface. \
The difference is even more pronounced now, with chat interfaces and assistive tech. \
So let's stop calling TUIs 'text' and call them 'terminal'—by their real name. \
Reserving 'text' for more appropriate conversational UIs."
#define IMAGE assets/cowsay-tui-not-tui.png
#define IMAGE_ALT "An ASCII drawing of a cow saying 'TUIs Are Not Text!'"

#include "template/header"

FIG()
IMG(IMAGE, IMAGE_ALT)
FIGCAP(Listen to what the cow has to say)

P()
I'm working on an audio-only Lisp cyberdeck.
A part of this project is making Lisp REPLs audio-friendly.
The trouble with most of them is that they overabuse text graphics.
And screen readers and voice synthesizers are not good at text graphics.

P()
Debuggers are especially suffering from this pseudo-graphics frenzy.
Here's how the debugger looks on SBCL, the most popular Lisp implementation:

PRE()
debugger invoked on a SB-INT:SIMPLE-PROGRAM-ERROR @53E0C809 in thread
HASH()LT()THREAD tid=12793 "main thread" RUNNING {100AEF00A3}>:
  invalid number of arguments: 0

Type HELP for debugger help, or (SB-EXT:EXIT) to exit from SBCL.

restarts (invokable by number or by possibly-abbreviated name):
  0: [REPLACE-FUNCTION] Call a different function with the same arguments
  1: [CALL-FORM       ] Call a different form
  2: [ABORT           ] Exit debugger, returning to top level.
PRECAP(SBCL debugger listing)

ULI Hash reader syntax (built into the language).
LI Opaque hexadecimal hash strings.
LI Square brackets.
LI Indentation.
END(UL)

P()
All these make my setup with Espeak go mad:

PRE()
sb dash int colon simple dash program dash error
at fifty three ee zero cee eight hundred and nine
PRECAP(Espeak reading of text graphics in SBCL)

P()
And that's just the
CD(SB-INT:SIMPLE-PROGRAM-ERROR @53E0C809) part!

P()
Dashes/hyphens are a vital part of Lisp kebab-case naming.
And they are proper English too, so Espeak is alright with them.
But a lot of the other clutter SBCL produces makes Espeak die in agony.
This "clutter" is there for aesthetics and "graphicality" of the UI.
Compare SBCL example to ECL's debugger screen:

PRE()
Condition of type: SIMPLE-PROGRAM-ERROR
Wrong number of arguments passed to function ERROR.

Available restarts:

1. (RESTART-TOPLEVEL) Go back to Top-Level REPL.

Broken at SI:BYTECODES. [Evaluation of: (ERROR)] In: HASH()LT()process TOP-LEVEL 0x7f12eb4d7f00>.
PRECAP(Debugger screen on Embeddable Common Lisp)

P()
There still are some graphics, like parentheses and braces, true.
But most of the information is expressed via text.
The most accessible communication medium.
As a side effect, Espeak reading of ECL debugger screen takes half of SBCL reading time.
The number of characters is comparable.
ECL clearly is just more productive with the air time.
And it makes me productive too, because I can take action faster.

P()
That's why I want to emphasize this: text interfaces are not the same as terminal interfaces.
Want audio or variety of modal interfaces?
You'd better ensure that your output is conversational.
Luckily, conversational interfaces are on the rise today, with AI chat apps.

P()
Text is accessible and easy to process.
So ensure your text interfaces are this: text.
Prose.
Conversation.
Description.
No one needs text graphics anyway—there are Graphical (sic) User Interfaces for that.
So make your product readable first and foremost.

P()
Disclaimer: I don't want to impose any course of action on any product, actually.
Especially so—on Lisp implementations.
SBCL is good at what it does, deserving the status of the best FOSS implementation of Common Lisp.
It's just that my cyberdeck will use ECL instead of SBCL for readability purposes 😛

P()
EDIT: As a bit of constructive suggestion, here's how an audio-friendly debugger might look.

PRE()
SB-INT:SIMPLE-PROGRAM-ERROR: invalid number of arguments: 0

Type HELP for debugger help.

Restarts (invokable by number or by possibly-abbreviated name):
0 REPLACE-FUNCTION: Call a different function with the same arguments
1 CALL-FORM:        Call a different form
2 ABORT:            Exit debugger, returning to top level.
PRECAP(Debugger that is both audio-friendly and easy on the eye)

P()
While opinionated, it gives the necessary minimum of information to act on.
Without the visual clutter and scary hex codes.
One can further reduce the noise deleting the notes on restarts and help, like CCL does:

PRE()
Error: Too few arguments in call to HASH()LT()Compiled-function PRINT #x30000017078F>:
       0 arguments provided, at least 1 required.
While executing: PRINT, in process listener(1).
Type :POP to abort, :R for a list of available restarts.
Type :? for other options.
PRECAP(Slightly decluttered variation of CCL debugger screen)

P()
But that's probably too opinionated for a default setup.

#include "template/feedback"
#include "template/footer"
