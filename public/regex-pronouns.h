#define TITLE Regex Pronouns?
#define DESCRIPTION "Pronouns are important. They are part of one's identity, after all. But what if one also identifies as a programmer? Regexes as pronoun listing!"
#define IMAGE assets/thumbnail-F5A9B8-B7F684-FFFFFF.png

#include "template/header"

P()
I have a friend who lists
A(https://en.pronouns.page/vi/vim, vi/vim)
as vis pronouns.
It's obviously a programmer joke, but quite an illuminating one.
vi/vim pair is conventionally contracted to CD(vi(m)) shortcut pattern.
Why not apply that regex-y approach to other pronouns?

P()
The syntax I'm suggesting is not necessarily PCRE or POSIX regex.
I'm rather suggesting a sloppy soup of PCRE, UNIX wildcards, and English.
Kind of like what programmers do when they talk about *nix, GNU/Linux, or jpe?g.

P()
Binary pronouns are a bad example: CD(/h(e|im)/) might
A(https://en.wikipedia.org/wiki/Heim, sound like home,)
but it's a pain to type.
Another binary pronoun pair regex that I can come up with is a horrifying CD(/s?her?/).
Not only it looks out of this world, it's also bad at describing the structure of the pronouns.
Is it she? her? sher? he?
So messy.

P()
Other, less widespread pronouns are much better as regex pronouns:
ULI Aforementioned vi/vim as CD(/vim?/)
LI CD(/the(y|m)/), CD(/xe(y|m)/)
LI CD(/its?/)
LI or the "any pronoun" person as CD(*)
END(UL)

P()
Neat, huh?
My pronouns officially are CD(/t?he(y|m)?/) now, what are yours?

P()
Update: thanks to many people
A(https://www.reddit.com/r/programming/comments/194xlv2/regex_pronouns/, on r/programming subreddit),
This post now has better regex start/end markers and better expressions for where I had unoptimized syntax.
And to
A(https://www.reddit.com/r/programminghorror/comments/198arxb/regex_pronouns_an_utterly_wrong_application_of/, people of r/programminghorror,)
for productive feedback.

#include "template/feedback"
#include "template/footer"
