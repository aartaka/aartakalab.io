#define TITLE Tripod—the restrictively productive blog engine
#define DESCRIPTION "Okay, so you want to have your blog backed by a good blog engine?"
#define IMAGE assets/thumbnail-0047AB-CC7722-C19A6B.png

#include "template/header"

P()
Okay, so you want to have your blog backed by a good blog engine?
There is a set of things you're most probably interested in:

ULI Ditching proprietary kitchen sinks, like Medium.
LI Making your blog posts accessible to a wide range of people and
LI Multitude of formats to write your posts in.
LI Increasing your productivity as a writer/blogger.
LI Starting it all with one command and not caring about it ever after (except maybe rebuilding for security updates).
LI Minimizing the carbon footprint of your website and being ethical towards your readers.
END(UL)

If this list of problems/requirements does resonate with you,
you might like
A(https://github.com/aartaka/tripod, Tripod,)
a minimalist blog engine I've made for this website
(and for your websites, if you decide to use it too!)

SECTION2(what-s-tripod, So, What Is Tripod?)

P()
Tripod is a blog engine, for one.
It's written in Common Lisp
(which I consider the best programming language out there,
but it also is the only language I know well and am paid for programming in),
and based on a wonderful Hunchentoot web server toolkit.
Hunchentoot is biased towards HTTP(S) servers,
but it has the infrastructure that allowed me to hack Gopher and Gemini support for Tripod.
So, the first two qualities of Tripod are:

ULI Tripod is extensible: It's Lisp-based, thus you can write your own code on top of Tripod and load it at runtime.
LI Tripod is multi-protocol: is supports HTTP(S) and Gopher (Gemini backend is in the process), with more to come!
END(UL)

P()
Now, blog engines are intended to show your texts in a nice way and support the markup format you love.
Tripod starts out with Gemtext and HTML support
(I also have Markdown and Org Mode parsing prototypes, but they are not yet ready :D),
which means support for most of the formats you use.

P()
Tripod is the common denominator model for markup formats and network protocols it supports.
HTML and Gemtext are not much, but they are properly interchangeable with any other format to come,
because Tripod parses any markup it supports into format-independent common denominator AST
which is straightforward to translate into any format Tripod could potentially support.

P()
But then, why bother making a common denominator ast for all the other markup languages,
removing their fancy features and nice styling?
Because Tripod aims to make your blog posting productive and content-focused!

P()
Tripod focuses you with the limited tool set that it provides you with.
You only have headings, paragraphs, links, depth-one lists
(ouch! this one is painful even for me, a creator of Tripod),
verbatim text, and blockquotes. Which is, by a lucky coincidence, the same set of primitives as Gemtext.

P()
It's an obvious thing in the Geminispace—simpler markup opens a space for a more focused thought,
both on the side of the writer and the reader.
The former doesn't tinker with styles and formatting ad nauseam,
the latter is not distracted by lots of media and styles.
Tripod is small, Tripod is restrictive, Tripod is productive.

SECTION2(how-do-i-use-tripod, So how do I use Tripod?)

P()
Given the set of features outlined above, only one initial initial requirement left:
ease of starting up.
You don't have to worry about that,
because Tripod, in addition to being usable as Lisp library,
compiles (with a mere CD(make all)) to a standalone executable
that you can conveniently run to start a server.
As an example of how easy running Tripod is,
here's the command I use to start this website
(slightly redacted to not leak unnecessary information):

PRE(sh)
./tripod -d ~/aartaka.me/ -a aartaka.me -l ~/.tripod/log.txt -p 70 -s 443 -c /path/to/cert.pem -k /path/to/privkey.pem AMP()
PRECAP(Shell command for aartaka.me Tripod server startup)

P()
That's a lot to digest, so let me explain the flags I use:

DL(-d)
is the directory to host files in. Put your precious .gmi and .html files there!
DD(-a)
is the address your website is hosted at. Can be an IP or a hostname, required for security reasons.
DD(-l)
is the location of the log file. If you don't provide it, the logs will be printed straight to your stdout.
DD(-p)
is the port to start Gopher server at.
DD(-s)
is the port for HTTPS (the HTTP flag is -t).
DD(-c)
is the certificate for your website. Required if you use HTTPS or Gemini.
DD(-k)
is the path to the private key. Again, required for HTTPS and Gemini backends.
END(DL)

P()
Getting thise together: start HTTPS and Gopher servers
with the content from CD(~/aartaka.me/) and write logs to CD(~/.tripod/log.txt).
Which I consider just enough of capabilities for a simple blog engine
that you start and go on to forget it until your certificate expires.

P()
You can always use CD(./tripod -h) to look at what flags
(and environment variables backing those) there are.
But it all usually boils down to running one command,
copying it and running the copied one again
after you renew the certificate or recompile Tripod for new features.

SECTION2(why-not-use-x, Why not use X technology?)

P()
A friend of mine has been constantly asking me
about different blog engines and their suitability for my use-cases,
only to be rejected with yet another set of requirements I have for a blog engine.
The blog engines I was suggested, together with things I don't like about them:

DL(A(https://jekyllrb.com/, Jekyll))
Cool, but it does not support Gopher and Gemini :P
DD(A(https://gohugo.io/, Hugo))
Themable and static, which is a plus.
But then, themes SHOULD NOT be part of a blog engine,
they should be chosen by the user or their user agent for the maximum convenience and accessibility!
DD(A(https://blot.im/, Blot))
Too much reliance on templates, implies Markdown or MS Word (what?!)
as the post format, no support for anything but HTTP(S).
DD(A(https://reactjs.org/, React.js))
It implies that you load JavaScript.
Lots of JavaScript.
Your whole website is JavaScript.
Run away while you can, JavaScript's eating your sister!
And it's not a blog engine, actually.
END(DL)

SECTION2(what-next, What next?)

P()
If you're interested in the ideas behind Tripod
(supporting as much formats and protocols as possible),
you might be thinking to yourself:
"can I write posts in Y and make Tripod support it?"
Yes you can!
I'll be glad to accept your contributions
and include them into Tripod (given that the code is sane).
My personal wish-list is to support more formats:

ULI Org Mode
LI Markdown
LI Plaintext
END(UL)

P()
And more protocols/ways of delivery:

ULI RSS
LI IPFS
LI JSON
END(UL)

P()
These are what I myself would want to have in the blogs I'm reading, at least.
I hope that Tripod strikes your fancy and you use it for your website.
Otherwise, you can always come here and adore this newborn blog engine in action 😉

P()
Update 2023: I no longer use Tripod, switching to a
AHERE(digital-bum, Digital Bum)
stack instead.

#include "template/feedback"
#include "template/footer"
