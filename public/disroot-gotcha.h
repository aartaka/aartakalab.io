#define TITLE Disroot Custom Domain Email Gotcha: Do Not Change the Settings!
#define DESCRIPTION "I'm a happy Disroot custom domain email user now! Here's a small tip that helped me set things up after linking."
#define LIGHT_COLOR white
#define LIGHT_ACCENT_COLOR HASH()50162d
#define DARK_ACCENT_COLOR HASH()719a38

#include "template/header"

P()
I finally switched to Disroot as my mail provider.
Freeing myself from the "Private" Email by Namecheap.
The whole process was somewhat chaotic, but I don't regret it.
Now my email is managed by an ethical privacy-conscious group of volunteers.
What else can one dream of?

P()
One thing that wasn't immediately obvious, though:
How do I use my new email?

P()
The onboarding message after domain linking is quite concise.
I've already said about it to Disroot people, and they will likely fix it.
But I had no luxury of detailed instructions and thus I want to help you.
So here goes my only advice:
Don't change the settings.

P()
In case you already have a Disroot email—you likely do, because how else would you request domain linking?—your email client is set up to connect to Disroot IMAP/SMTP servers.
These settings allow you to get and send emails at/from an address like CD(xxx@disroot.org).
The only thing that changes after domain linking is your email address.
In my case, I just set my email client address to CD(xxx@aartaka.me), without changing any other settings.
And now I can send and receive emails from my own domain—thanks again to Disroot people!


P()
P.S. Here's Emacs (Gnus, in particular) config snippet that does the magic:

PRE(elisp)
(setq user-mail-address    "xxx@aartaka.me" ;; Used to be "xxx@disroot.org"
      user-full-name       "Artyom Bologov"
      smtpmail-smtp-server "disroot.org"
      send-mail-function   'smtpmail-send-it
      smtpmail-smtp-service 587
      gnus-select-method '(nnimap "disroot.org"))
PRECAP(My Emacs/Gnus disroot email configuration)

P()
Implying that you have password and CD(xxx@disroot.org) username as server credentials.

#include "template/feedback"
#include "template/footer"
