#define TITLE Welcome!
#define DESCRIPTION "A blog of Artyom Bologov, programmer-poet, privacy freak, and eco-activist. Programming, art, F/L/OSS, and other random stuff."
#define HTMLMETA <style>		\
	HASH()container {		\
	display: flex;			\
		flex-flow: row wrap;	\
		justify-content: left;	\
	}				\
	figure {					\
	margin: 1em;					\
	}						\
	@media (min-device-width: 500px) {	\
		figure {			\
			max-width: 25vw;	\
		}				\
	}					\
</style>

#include "template/header"

P()
Hi! I'm Artyom,
an ironic bipolar programmer trying to make my environment
(and, consequently, the Internet) slightly better and weirder.
Here are some reviews on what I'm doing/writing:

<div id=container>
BQ()Stop with the blogs
BQCAP(u/TysonPeaksTech (Reddit) on "There Is No Such Thing As The Regex")

BQ()Utterly sick and downright twisted, but hilarious. Thanks.
BQCAP(mrlonglong (Hacker News) on ed.bf)

BQ()Funny thing. Hope no one gets the idea to actually use this
BQCAP(u/DocEyss (Reddit) on "Object-Oriented C: A Primer")

BQ()Thanks I hate it
BQCAP(u/terablas (Reddit) on "Regex pronouns?" and u/matjam on "Making C Uglier")

BQ()im pretty sure that isn't serious
BQCAP(u/beephod_zabblebrox (Reddit) on "Making C Prettier")

BQ()they rejected your application because of typos
BQCAP(A recruiter from Yandex)
</div>

And here are my posts (from newest to oldest):

DL(AHERE(procnames, Procnames Start Lines. But Why?) (21 Dec 2024))
Putting procedure names at column 0 is a widespread convention in C codebases. \
But the exact reasons are not really elaborated anywhere. \
We just do that. \
So I decided to make my own summary for why it's a thing.

DD(AHERE(this-post-is-ed, Using ed(1) as My Static Site Generator) (11 Dec 2024))
You can tell I'm an ed fan. Not only do I use it for esoteric challenges or compiler building, it also is my new SSG (Static Site Generator.) So here's how it works:

DD(AHERE(3mins, Three Minutes Writing) (1 Dec 2024))
We're all suffering with attention deficit. So let's finally write like we do.

DD(AHERE(generated, Generating This Post Without LLMs) (26 Nov 2024))
Text generation was a lot of fun before ChatGPT—it was chaotic and deranged. I written this post using the old text generation techniques. You'll like how absurd it turned out, I promise!

DD(AHERE(printf, Printf Is Useless) (17 Nov 2024))
Printf (and the derivatives every language has) are a bane that should only be tolerated when printing floats. Otherwise, I beg you, use string interpolation or structured output.

DD(AHERE(sed-ed, s/sed/ed) (13 Nov 2024))
ed is a stupid simple text editor. sed is a nice streaming text processing tool. Why would one even want to use ed for anything, let alone for text processing if there's sed?

DD(AHERE(paren-regex, 5 *Wrong* Regex To Parse Parentheses) (3 Nov 2024))
Regex are powerful. To the point you may try to parse HTML or Lisp with it. A doomed enterprise, right? But it's possible, actually.

DD(AHERE(parameterized, Parameterized Procedures for Testing, Mocking, Plumbing) (25 Oct 2024))
It's often the case that a functional (Scheme?) codebase needs to plug something into the computation. Here's one way to do that with almost no syntactic overhead.

DD(AHERE(c-not-c, C Until It Is No Longer C) (23 Sep 2024))
Get horrified by how pretty I'm making my C code!

DD(AHERE(the-regex, There Is No Such Thing As The Regex) (11 Sep 2024))
Regular expressions seem to be quite coherent, right? This scary (.*[^}(]) symbol soup that only the select few can master. Except that there are at least half a dozen (slightly incompatible) varieties of this soup. Bon Appétit!

DD(AHERE(commitmsg, A&#39;s Commit Messages Guide: Location, Action, Rationale) (01 Sep 2024))
Commit messages are the critical thing you stare at often enough. So better make them informational and readable.

DD(AHERE(not-ai, I Am Not an AI—My Writing Is Terible) (27 Aug 2024))
My writing is imperfect. "AI" could help. But, you know, I no longer want to hide myself. My terible writing is the only things that differentiates me from the sterile AI slop you find everywhere now.

DD(AHERE(wisp, Explaining Wisp Without Parentheses) (19 Aug 2024))
Wisp is an indentation-sensitive syntax for Scheme. Its semantics are defined in terms of nested parentheses, though. But what if it was defined in other terms?

DD(AHERE(disroot-gotcha, Disroot Custom Domain Email Gotcha: Do Not Change the Settings!) (31 Jul 2024))
I'm a happy Disroot custom domain email user now! Here's a small tip that helped me set things up after linking.

DD(AHERE(scrollbar, I Want My Scrollbar Back (A 2-Minute Read)) (12 Jul 2024))
The modern web is empowered by dynamic content loading. The modern reading is impoverished by it.

DD(AHERE(prose-vs-tweet, Prose vs. Tweet: How We Tell Stories) (21 Jun 2024))
We always told stories. We always will. Though our stories fit into 140 chars now, which is both a blessing and a curse.

DD(AHERE(lambda-3, Making Sense of Lambda Calculus 3: Truth or Dare With Booleans) (12 Jun 2024))
Booleans are simple and elegant in Lambda Calculus, but they take some getting used to. This post tries to explain LC booleans to at least myself.

DD(AHERE(gmi-a11y, Gemtext Is Not Accessible) (4 Jun 2024))
Gemtext is a minimalist markup/hypertext format initially from Gemini network. It's intended to be a lightweight, easy to learn, and accessible language... But at least the latter is not true with the current state of things.

DD(AHERE(cl-is-lots, Common Lisp Is Not a Single Language, It Is Lots) (23 May 2024))
Lisp is an ambiguous category. But Common Lisp isn't, right? It's a restricted self-sufficient language, after all.

DD(AHERE(lambda-2, Making Sense of Lambda Calculus 2: Numerous Quirks of Numbers) (10 May 2024))
Lambda Calculus is extremely elegant, including in how it handles numbers. But this elegance often comes at the cost of understandability. This post goes through examples of arithmetics in Lambda Calculus to understand how they work.

DD(AHERE(guile-optimization, Guile Optimization Gotchas: There Is No Free Beer, Only Cheap) (23 Apr 2024))
Optimizing Guile Scheme is not always obvious. This post is a collection of takeaways from optimizing a heavily numeric piece of code.

DD(AHERE(oop-c, Object-Oriented C: A Primer) (8 Apr 2024))
One can go Object-Oriented in C, and do so with decent success. Although it's definitely not quite the popular version of OOP everyone is conditioned to.

DD(AHERE(falsehoods-html, Falsehoods Programmers Believe About HTML) (29 Mar 2024))
There are many things programmers believe about HTML that are not necessarily true. Here're some.

DD(AHERE(tui-not-tui, Text UIs != Terminal UIs) (1 Mar 2024))
TUI is an ambiguous term, meaning either of 'terminal' or 'text' user interface. The difference is even more pronounced now, with chat interfaces and assistive tech. So let's stop calling TUIs 'text' and call them 'terminal'—by their real name. Reserving 'text' for more appropriate conversational UIs.

DD(AHERE(nyxt-to-surf, Moving From Nyxt To Surf) (14 Feb 2024))
I switched to Surf because of habit switch from distracted kitchen sinks to focused app-like web browsing.

DD(AHERE(lisp-design-patterns, Lisp Design Patterns) (25 Jan 2024))
Lisp projects might be smaller and neater that other tech. But still, there are emergent patterns in any software. So here's an arbitrary list of design patterns I found in Lisp codebases.

DD(AHERE(regex-pronouns, Regex Pronouns?) (12 Dec 2023))
Pronouns are important. They are part of one's identity, after all. But what if one also identifies as a programmer? Regexes as pronoun listing!

DD(AHERE(lambda-1, Making Sense of Lambda Calculus 1: Ignorant, Lazy, and Greedy Evaluation) (30 Dec 2023))
Lambda Calculus is a strong model for computation, but computing examples in it often feels off. In this post, I'm trying to understand how Lambda Calculus evaluation/reduction actually works.

DD(AHERE(brainfuck-lessons, What Writing 2K+ Lines of Brainfuck Taught Me) (21 Dec 2023))
I am a Brainfuck programmer. I made some projects that caused me immence pai... joy. I learned something from these, and you can too!

DD(AHERE(lambda-0, Making Sense of Lambda Calculus 0: Abstration, Reduction, Substitution?) (20 Dec 2023))
Lambda Calculus is a fascinating idea, but it's not immediately obvious. In this post, I'm trying to understand and explain the basic terms used in Lambda Calculus.

DD(AHERE(this-post-is-cpp, I Generated This Post With C Preprocessor) (3 Dec 2023))
Yes, you can use C preprocessor as a website generator.

DD(AHERE(making-c-prettier, Making C Code Prettier) (25 Nov 2023))
C code might be ugly at times. But it can also be pretty! Here are some features and snippets with pretty C code.

DD(AHERE(making-c-uglier, Making C Code Uglier) (4 Nov 2023))
C code is scary. It's often hard to read and has lots of footguns. But it can get even uglier than you imagine. Lo and behold!

DD(AHERE(hypertext, Write Hypertext, not Plaintext) (4 Sep 2023))
Living a plaintext-only life is tempting. But the further one goes with plaintext, the more they re-invent Markdown or HTML. Let's just give up and live hypertext life instead.

DD(AHERE(digital-bum, Digital Bum: Finding a Home/lessness on the Internet) (29 Jul 2023))
Internet grew out of a non-commercial academic network with free resources for everyone. Can one get back to this dream of free Internet and build a lifestyle out of it?

DD(AHERE(this-post-is-lisp, This Post is Written in Lisp) (7 Jul 2023))
This is probably some fanatic with weird AST magic, you say. But if this magic allows one writing with tab-completion, automatic HTML boilerplate, and other goodies—then why not try it? 😉

DD(AHERE(design-for-exploitation, Designing for Exploitation: How Meta-Programming Leads to Safer Code) (14 Sep 2022))
Security tip: You should use meta-programming abilities of your technology (I mostly mean programming languages there) as much as possible, and you should allow your users to program your program by exposing programming languages to them.

DD(AHERE(tripod, Tripod, the restrictively productive blog engine) (4 Sep 2022))
Okay, so you want to have your blog backed by a good blog engine?
END(DL)

#include "template/footer"
