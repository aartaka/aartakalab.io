#define TITLE Artyom Bologov, A Turing-complete Programmer
#define DESCRIPTION "I'm into Brainfuck, Lisp, and researching complex problems. Nice to meet you too!"

#include "template/header"

P()
My story starts with implementing CD(sqrt) in Brainfuck—university assignment on Turing-completeness.
I haven't succeeded at
CD(sqrt), but I've tasted the forbidden fruit of Turing tarpits.
Be it
AHERE(brainfuck-lessons, Brainfuck),
AHERE(lisp-design-patterns, Lisp)
(I taught myself to Lisp by reading
A(https://www.reddit.com/r/lisp/, r/lisp)),
AHERE(lambda-0, Lambda Calculus),
AHERE(paren-regex, or POSIX Regex.)
I also dug into
Complexity Science, Systems Analysis, Game Theory, Behavioral Economics, Information Theory,
Agent-based Simulation, Game-Theoretic models, and Business Process Analysis,
which is all around my interest in Complexity, Decision Making,  and Computation.

P()
In parallel to university, I worked on
A(https://nyxt-browser.com/, Nyxt browser).
Both in the
A(https://github.com/atlas-engineer/nyxt/pull/2024, network)/C/renderer/
A("projects#atlas-nyxt", back-end libraries)
and in
A(https://github.com/atlas-engineer/nyxt/pull/1889, Web-design)/UX/HTML/CSS/JavaScript/front-end.
Thus my interest in
AHERE(hypertext, semantic Web Design),
AHERE(gmi-a11y, Accessibility), and...
AHERE(making-c-uglier, C.)

P()
Just in time for my graduation, war in Ukraine started.
I defended my thesis on
A(https://nyxt-browser.com/article/global-history-tree.org, tree-based browser history),
made in collaboration with Atlas Engineer (a foreign company, likely European spies!)
And left the fuck out for Armenia.
I don't want to support the state regime killing people of another sovereign state (Слава Україні!)
Let alone for the oil money or some delusional small dick person we should get rid of (Россия Будет Свободной!)

P()
Moving to a different state and hopping over the job market is not fun.
But I find solace in programming and computer art.
A(projects, I have lots of projects) on

ULI Turing completeness.
LI  Mundane/stuplime/meaningless computing.
LI  And multi-modal/flexible interfaces.
END(UL)

P()
So yes, I'm spending most of my time somewhere in (gorgeous!) mountains of Armenia.
Mostly locked in a flat and jumping from one nerd snipe to another.
A(https://github.com/bf-enterprise-solutions/, Brainfuck),
AHERE(lisp-design-patterns, Lisp),
A(https://github.com/aartaka/pretty.c, C programming language),
AHERE(the-regex, regular expressions),
AHERE(https://rosettacode.org/wiki/Category:Ed, ed, the standard text editor), or
AHERE(this-post-is-cpp, doing Web dev with extremely minimalist tech stack).

SECTION2(contacts, Contacts)

P()
Here are some links for where you can find me:

DL(A(https://github.com/aartaka, GitHub))
Most of the code I do. I hope to move it all somewhere else (CL Foundation Gitlab, Gitlab, Sourcehut, Codeberg?)
DD(A(https://linkedin.com/in/artyom-bologov, LinkedIn))
In case you wanna try to hunt me 😛
DD(A(https://merveilles.town/@aartaka, Mastodon (merveilles.town)))
To read my everyday things.
DD(A(mailto:mail@aartaka.me, Email))
Contact me there.
DD(bc1qaxua3gqfkpk24g7dggkx7w06q24602hauas47m)
Bitcoin address for you to donate to me 😉
END(DL)

#include "template/footer"
