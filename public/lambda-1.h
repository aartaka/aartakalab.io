#define TITLE Making Sense of Lambda Calculus 1: Ignorant, Lazy, and Greedy Evaluation
#define DESCRIPTION "Lambda Calculus is a strong model for computation, but computing examples in it often feels off. In this post, I'm trying to understand how Lambda Calculus evaluation/reduction actually works."
#define IMAGE "assets/thumbnail-CD7F32-FFFFFF-5E5184.png"
#define LIGHT_COLOR HASH()ffffff
#define DARK_ACCENT_COLOR HASH()cd7f32
#define LIGHT_ACCENT_COLOR HASH()5e5184

#include "template/header"

#define LC Lambda Calculus

P()
Lambda Calculus is not hard, but some parts of it are confusing.
While I've
AHERE(lambda-0, sorted out the confusing terms),
I'm still getting tripped over

ULI missing parentheses,
LI unclear lambda scope,
LI and general computation/definition order.
END(UL)

P()
This post is me trying to clear up Lambda Calculus evaluation properties.

SECTION2(ignorant, Being Ignorant)

P()
My programmer background doesn't make it easier.
I'm used to the fact that arguments are evaluated before the function call.

PRE(c)
int i = 0;
f(i++, i++, i);
// equivalent to f(0, 1, 2)
PRECAP(Order of evaluation in most programming language)

P()
Lambda Calculus is working the other way around.
Arguments are evaluated only when the function is applied to them.
And the function is evaluated before the arguments.
You can have a looped computation as argument to your function.
And that doesn't necessarily makes your function stuck.
Depends on what is the order of computation.
Here's an example:

PRE(c)
int i = 0;
f(i++, i++, i);
// equivalent to
// f1 = f
// f2 = f1(i++)
// f3 = f2(i++)
// result = f3(i)
PRECAP(Order of evaluation in Lambda Calculus)

P()
So there actually is computation in between argument evaluation.
Scary, right?
But that's how it works.

SECTION2(lazy, Being Lazy)

P()
It's often the case with LC examples that they omit parentheses.
Some of these are uncertain in what to do first.

PRE()
f g h
PRECAP(Example of confusing application order)

P()
This one first applies CD(f) to CD(g) and then applies the result of that to CD(h).
Spelling that out doesn't make it clearer for me.
But here's the point: the argument are lazily collected.
So you only take an argument when you need one.
And, again, you don't evaluate arguments until you need to use them.

SECTION2(greedy, Being Greedy)

P()
Okay, so the order of application is lazy and minimalist.
It only takes arguments when it needs them.
If a function needs only one argument, it only takes one.

PRE()
(λx.5) y z // only consumes y and returns 5 (to be applied to z later)
PRECAP(Example of unused argument computation)

P()
What's extremely inconsistent is that abstraction is greedy.
It extends as far to the right as possible.
The previous example would be a single lambda if we remove parentheses

PRE()
λx.5 y z // a single huge lambda.
PRECAP(Example of abstraction greediness)

P()
That's why most examples you see out in the wild parenthesize the function:
they need to restrict the abstraction from eating up too much expressions.

SECTION2(up-next, Up next: Numerous Quirks of Numbers)

P()
Lambda Calculus might seem really nasty at this point: it's Ignorant, Lazy, and Greedy.
But I'm being misleading here: LC is still a powerful idea.
And my negative impressions might be a consequence of getting repulsed by its specificities.
So let's bear with it and get to appreciate it while diving deeper.

P()
We've established the order
of typical LC operations
and
AHERE(lambda-0, understood basic terms)
used in LC.
Now to the actual practice: Church Numerals encoding numbers!
Arithmetic, numbers as function applications, elegant multiplication and exponentiation, and... ugly subtraction.

#include "template/feedback"
#include "template/footer"
