#define TITLE Lisp Design Patterns
#define DESCRIPTION "Lisp projects might be smaller and neater that other tech. \
But still, there are emergent patterns in any software.\
So here's an arbitrary list of design patterns I found in Lisp codebases."
#define IMAGE assets/lizard-tail.jpg
#define IMAGE_ALT "Scaly lizard tail wrapped into a spiral."

#include "template/header"

FIG()
IMG(IMAGE, IMAGE_ALT)
FIGCAP(Fractal lizard showing us a proper way to design layered software.)

P()
There was
A(https://www.reddit.com/r/lisp/comments/19c9vn6/how_to_architect_large_lisp_codebase/, this question on how to design Lisp software)
on r/lisp.
The answers mostly falling into one of

ULI "Try and see."
LI "Use a general approach applicable to any language."
LI "Do a REPL-driven development."
LI "Build a functional style layered architecture."
END(UL)

P()
The last response is quite close to acceptable architecture advice.
But, I believe, neither (except for maybe the REPL answer) does justice to Lisp.
The Lisp family of languages have many features and architectural approaches.
Even though these patterns differ from the normie ones, they still are this: patterns.
In this post, I'm listing some of the general patterns that I've seen in mine and others' code.
From more generic (pun intended) to the local ones.

P()
I'm deliberately ignoring OOP patterns.
Lispy Object-Oriented systems (CLOS, EIEIO, GOOPS) might be strange and powerful.
But most of the techniques from other OOP languages apply to them nonetheless.
So let's leave the space for more Lisp-specific patterns!

P()
But to tip my hat to the OOP/Gang of Four people, I'll start from one of their ideas:

SECTION2(extension-points, Extension Points)

P()
There's an odd pattern among the ones Java programmers preach:
A(https://medium.com/leadpixel/extension-points-306ecd271a5, Extension Point).
Most likely no one understands what exactly that pattern means.
It's just "a part of your design open to later extension."
But if anything suits the "Extension Point" name, it's Lisp code.

P()
All the patterns below are a certain kind of extension point.
Varying levels of complexity, assorted dialects, diverse features.
But still, these are some of the things that make Lisp software flexible and loveable.

SECTION2(protocols, By Contract: Protocols and Back-ends (CL, Clojure))

P()
This top-level pattern is quite universal.
My friend recently raged about the overabundance of meaningless interfaces in Java and C NUM().
Golang is built around the idea of interfaces.
C++ boasts virtual classes.
Clojure allows to define CD(defprotocol)-s
or multiple versions of
A("https://clojure.org/reference/special_forms#def", CD(defn))s
and ensure their plasticity.

P()
Common Lisp protocols usually come in the form of CD(defgeneric) declarations.

ULI One defines a generic function.
LI But they don't define any implementation.
LI Down the line, they write code conforming to the contract this generic function established.
LI Further down the line, they can write another implementation.
LI And either of implementations might be drop-in alternatives for the downstream user.
END(UL)

P()
A perfect example of this approach is Shinmera's work
like
A(https://github.com/Shirakumo/cl-mixed, cl-mixed) with its heaps of back-ends.
Another example is JSON-parser-agnostic
A(https://nyxt.atlas.engineer/article/njson.org, NJSON)
(shameless plug: I designed it.)
Here's how a protocol definition looks there:
PRE(lisp)
(defgeneric decode-from-stream (stream)
  (:method (stream)
    (signal 'decode-from-stream-not-implemented))
  (:documentation "Decode JSON from STREAM.
Specialize on `stream' to make NJSON decode JSON."))
PRECAP(Protocol generic requiring at least one implementation)

P()
This type of architecture enforces a structure on the back-ends, while being adaptable to back-end swapping.
And this type of generalization also allows for...

SECTION2(monkey-patching, Monkey Patching (Emacs Lisp, CL))

P()
If one can write a back-end and plug it into the software...
Then, well, anyone can do that.
Even the user.
Even if the software is quite intricate.
If it's a mature enough Lisp, it likely has
ULI Function overrides (any Lisp),
LI Method overrides (any OOP Lisp),
LI Advices (Emacs, some CL implementations, Clojure),
LI :around methods (CL),
LI Slot listeners (CL and some other OOP Lisps, likely),
LI <i>et cetera</i>, <i>et cetera</i>
END(UL)

P()
This downstream-override approach is colloquially known as "monkey patching."
It's a basis for Aspect-Oriented Design, suggested by Gregor Kiczales.
The idiomatic example of that design is exactly what the OP of the Reddit post was asking about: logging.
In the the Aspect-Oriented-friendly application, logging can be added at any moment, just as an advice/override/CD(:around) function.
No need to put CD(console.log) all over the place, just make a special method and wrap your code into it!

P()
Another example of this approach might be a home-brewed event system in Nyxt
(even though I left the team, I still love the code we've done together),
based on CD((setf slot) :around) methods in Common Lisp.
The logic is: once the slot is CD(setf)-ed, invoke a function updating the UI:

PRE(lisp)
(defmethod (setf url) :around (value (buffer document-buffer))
  (call-next-method)
  (set-window-title))
PRECAP(Example of event-invoking setter method)

SECTION3(hooks, Hooks (Emacs, CL, and others))

P()
Thanks to
A(https://www.reddit.com/user/arthurno1/, u/arthurno1)
for
A(https://www.reddit.com/r/lisp/comments/19fh0xx/comment/kjmuwt3/?context=3, mentioning this!)

P()
Hooks are variables/slots/objects effectively storing a list of functions (usually called "handlers").
Once something happens, the functions stored inside the hook are called.
Emacs utilizes hooks as one of the main extension points, having hooks for:

ULI Emacs initialization, closing, saving and other editor-specific actions.
LI Events.
LI Mode toggling.
LI And likely some other purposes I can't recall.
END(UL)

P()
Hooks may be said to be a subset/superset of the monkey-patched listeners.
But they are too conventional and useful to ignore.

SECTION2(conditions, Fail Fast, Fail Often: Conditions (CL))

P()
I always CD(panic!) when I see an error in a non-Lisp language.
Because this error usually means stack unwinding at best or program abort at worst.
So there's a reason to fear errors and
CD(try) to prevent them in any way possible.

P()
Not in Lisp.
Not in Common Lisp, in particular.
CL has this Condition System thingie that errors are part of.
Restarts are a notable feature of CL conditions that most other exception/error/condition systems lack.
Restarts are basically pieces of code you can invoke from the debugger to fix the error.

ULI Server returned an error code?
Just CD(restart) to try again!
LI Something broke the code, but you don't want to deal with it now?
Just CD(continue) to ignore it.
LI A typo made a wrong value to be passed to a function?
Just CD(use-value) and replace the faulty argument.
END(UL)

P()
All the computation is effectively paused until the user chooses a restart to resume it.
Which can happen at an arbitrary moment or not even happen at all.

P()
The point partially applies to other Lisps, because of REPL-driven development.
Even if something goes wrong, one's just dropped into another level of debug loop.
While in there, one can redefine the broken functions and restart the computation.

P()
Or ignore the error and CD(abort) out into toplevel REPL.

SECTION2(dynamic-variables, Shared State: Dynamic Variables (Emacs Lisp, CL))

P()
Ah, the dreaded global thread-unsafe state!
Even though most Lisps left the idea of dynamic scope behind, some still have it.
Common Lisp and Emacs Lisp both utilize dynamic variables... productively.
One particular example might be CL's printer system.
There are
A(https://www.lispworks.com/documentation/HyperSpec/Body/f_wr_pr.htm, more than 15 globals)
which influence printing.

P()
Which sounds like a disaster, right?
Modifying global state just to change local logic sounds painful.

P()
Luckily, the main use for dynamic variables is not setting them directly.
Binding them lexically with CD(let) is a much more widespread pattern.
It allows one to override the behavior these variables define, without making risky modifications of the shared state!

  P()
Shameless plug again: my
A(https://github.com/aartaka/graven-image, Graven Image) library.
There's no custom printing in it, and all the APIs rely on the built-in functions like
CD(print) for it.
At times, this makes the output look extremely inconsistent or outright scary.
As an intended use-case, the output of Graven Image functions is intended to be altered via printer variables:

PRE(lisp)
(defmethod gimage:apropos* :around (string AMP()optional package external-only docs-too)
  (let ((*print-case* :downcase)
        (*print-level* 2)
        (*print-lines* 2)
        (*print-length* 10))
    (call-next-method)))
PRECAP(An example method overriding (see previous sections) printer variables)

SECTION2(keywords, Pythonique: Keywords and Destructuring (CL, Guile, Clojure))

P()
I'm not even sure whether I should include that: it's too low-level.
But dynamic variables are quite atomic (pun not intended) too, so here goes nothing.

P()
Programming in Guile, I often miss CL's function keyword.
There's
CD(lambda*)/CD(define*) for that, but it sucks that keywords are not enabled by default.

PRE(scheme)
(define* (jsc-class-make-constructor
          class NUM():key (name %null-pointer) (callback #f)
          (number-of-args (if callback (procedure-maximum-arity callback) 0)))
  "Create a constructor for CLASS with CALLBACK called on object initialization..."
  ...)
PRECAP(Use of CD(define*) to make functions with keywords in Guile)

P()
There are several places keywords are vital:
ULI Non-trivial function behavior: CD(dex:get) has 21 keyword arguments because HTTP is hard
and CD(AMP()optional) arguments just don't cut it.
LI  Heavily Object-oriented code: CD(apply) over CD(make-instance) is one's best friend.
LI  Structure constructors are using keywords for arguments.
LI  Other side of the OOP-FP spectrum: destructuring of arguments/EDN/data in Clojure.
There's even a dedicated destructuring syntax in
CD(let)!
END(UL)

P()
Keywords might be passed real deep in the code paths.
That's why CL's
CD(declare (ignore ...)) and CD(AMP()allow-other-keys) pattern is important.
If one can ignore some keys instead of mandating them, their code can scale gracefully.
And that's the whole point of design patterns, right?

P()
Update Apr 2024: I now understand and love Clojure map/sequence destructuring.
And Scheme CD(match) macro.
So this point really goes beyond keywords: it's about getting the data out of inputs.
Flexibly and within a hand's reach at all times.

SECTION2(conclusion, Concluding)

P()
This is not the final list!
You can probably recall some other patterns better than my memory allows me to.
So feel free to reach out via
A(https://www.reddit.com/r/lisp/comments/19fh0xx/lisp_design_patterns_overview/, Reddit),
A(https://news.ycombinator.com/item?id=39140770, Hacker News comments),
or
A(mailto:mail@aartaka.me, email).

P()
See also:
A(https://clojurepatterns.com/, Clojure Design Patterns).
A(https://nyxt.atlas.engineer/article/lisp-documentation-patterns.org, And my listing of Lisp documentation patterns) on Nyxt website.

P()
To use the air time productively: I'm searching for new projects/positions!
If your team looks for a CL/Clojure dev with a good knowledge of design patterns (duh), C, and JavaScript,
then we might be a good match!
Shoot me
A(mailto:mail@aartaka.me, an email)
sometime 😉

#include "template/feedback"
#include "template/footer"
