#define TITLE Explaining Wisp Without Parentheses
#define DESCRIPTION "Wisp is an indentation-sensitive syntax for Scheme. \
Its semantics are defined in terms of nested parentheses, though. \
But what if it was defined in other terms?"
#define IMAGE "assets/wisp.png"
#define IMAGE_ALT "Hand-drawn thumbnail with 'Explaining Wisp (w/o parentheses)' text in the center\
and 'Artyom Bologov'/'aartaka.me' in the corners"
#define LIGHT_COLOR HASH()FFF3B0
#define DARK_COLOR HASH()1A181B
#define LIGHT_ACCENT_COLOR HASH()4357AD
#define DARK_ACCENT_COLOR HASH()48ACF0

#include "template/header"

P()
Many people—including even some Lispers!—dislike Lisp's
overabundance of parentheses—called P-WORD from now on.
Looking at
A(https://www.reddit.com/r/lisp/comments/b2784p/is_there_a_word_for_that_fat_stack_of_parens_at/, overly friendly tiger tails)
all day might be somewhat tiring.
Many Lisp programmers rely on indentation as a marker of code structure/nesting instead.
There's a niche for indentation-based syntax,
cleaner and more readable than all these P-WORD.

P()
Enter Wisp.
A(https://srfi.schemers.org/srfi-119/, SRFI-119), a Scheme Request for Implementation from 2015.
It's super minimalist with only two special syntax markers and indentation as the only nesting indicator.
It also supports the P-WORD syntax, so that one can always fall back to the comfort of regular Scheme.

P()
One problem with Wisp, though—it's
defined in terms of P-WORD syntax.
It's not self-sufficient and only makes sense to the ones already into Lisp.
Wisp is not suitable for Lisp propaganda aiming at those disliking the P-WORD.
And that propaganda,
A(https://youtu.be/cI1sltJMVuM?si=N5AP3yvzaTRX5MQc&t=1034, according to Arne Babenhauserheide),
the author of SRFI-119, is one of the goals for the syntax.

P()
This post tries to explain Wisp as a self-sufficient language.
No mention of the P-WORD.
Let's see whether it's even possible.

SECTION2(blocks, A Bit of Cheating: Blocks)

P()
P-WORD is a marker of s-expressions in Lisps.
So one needs an alternative word/concept for... everything Lisp has?
I suggest "block", as in Python blocks.
Blocks are numerous, almost everything in Wisp is blocks.
Blocks usually occupy a line of their own.
Procedure calls are blocks:

PRE()
procedure arg1 arg2
PRECAP(Procedure call as a block)


P()
Procedure call might be split over multiple lines.
Arguments to the procedure—often blocks themselves—are marked by deeper indentation level:

PRE()
procedure arg
          another-procedure arg1 arg2
PRECAP(Procedure call as a multiline indented block)

P()
Special forms with bodies are blocks too:

PRE()
begin
 procedure arg
 another-procedure arg1 arg2
PRECAP(Begin form as a nested multiline block)

P()
Wisp concerns itself with blocks and how they wrap each other.
You'll see how both Wisp operators tie into this.

SECTION2(period, Period: No Blocks!)

P()
So I said that almost everything in Wisp is a block, didn't I?
Almost.
In case there's something that isn't a block, it needs to be delineated.

P()
Say you want to return CD(HASH()t) from your function.
It's not a block, it calls no function.
That's why it must be marked with the period—to be returned verbatim:

PRE()
lambda :
  . HASH()t
PRECAP(Returning a value instead of interpreting it as a block)

SECTION2(inline-colon, Inline Colon as an Application Marker)

P()
Colon is the second operator in Wisp, and it's also concerned with blocks.
But it has two different uses depending on where in the code it is found.
One of the uses is inline.
Colon between two things.
When positioned like that, the colon means nested procedure applications/block.
Making the right part up until newline an argument to the left part.
Something like Haskell CD($) operator.
Examples:

PRE()
;; display a sum of 1 and 2
display : + 1 2
;; display a sum of 1 with a difference of 2 and 3
display : + 1 : - 2 3
PRECAP(Examples of colon as procedure application operator)

P()
You can also see inline colon in some special forms.
Procedure creation depends on colons, for example.

PRE()
define : a b c
  . HASH()t
;; Equivalent
define a
  lambda : b c
    . HASH()t
PRECAP(Defining/creating a new procedure using the inline colon)

P()
It definitely is not a procedure application operator in this context.
But it's still used this way—as
a block that CD(define) requires for procedure prototype.
Colon is a mere syntactic marker, not a semantic one.
You just need to memorize the use of the colon in CD(define)/CD(lambda).
Much like in any language with syntax.

SECTION2(leading-colon, Leading Colon: Wrapping Block)

P()
Another use of the colon is the leading colon case.
It introduces a new block.
A new layer of nesting for things.
Even if the thing after it is a block—like procedure call—already.
It's useful for Scheme special forms
like CD(let):

PRE()
let
  :
    a 3
    b 4
;; Or the form I like more
let
  : a 3
    b 4
PRECAP(Use of the leading colon in let-bindings)

P()
Both of the cases above involve multi-line blocks.
The indentation makes both lines a part of the colon-introduced block.
But what if the leading colon doesn't wrap multiple lines?
What if there are no lines indented below it?
Then it just wraps whatever follows:

PRE()
do
  ;; Using inline colon for procedure application too
  : i 0 : + 1 i
  : = 10 i
 display i
;; 0123456789
PRECAP(Use of the leading colon in do)

P()
The only use for the leading colon is macros, really.
There are not a lot of macros in Scheme, so you can easily memorize the patterns.
Like with CD(lambda), CD(define), CD(let), and CD(do).

SECTION2(curly-infix, Curly-infix Notation)

P()
We've got rid of
P-WORD, but what about prefix math?
It hurts.
Any way to replace it with something more intuitive/familiar?
A(https://srfi.schemers.org/srfi-105/, Like SRFI-105), curly-infix expressions.

P()
Wisp SRFI requires curly-infix expressions as part of the dialect.
So one can freely do

PRE()
display {1 + 2}
PRECAP(Use of curly-infix expression in Wisp)

P()
Missed weird precedence rules and duplicated signs?
Here you go!

SECTION2(applying, Applying The Takeaways)

ULI Blocks are mere lines.
LI  Blocks can be nested, with inner blocks indented deeper than the outer block.
LI  Non-blocks should be marked with the period CD(.).
LI  Blocks can be nested inline with the colon CD(:).
LI  Blocks can be wrapped in the extra block with the colon too. It's usually important for macros.
LI  Curly-infix notation should be available on Wisp systems too.
END(UL)

P()
How about applying it to the ultimate syntactic monstrosity, CD(case-lambda)?

PRE()
define my-plus
 case-lambda
  ;; Wrapping the thing into a block.
  : a
    ;; Returning the value verbatim
    . a
  : a b
    ;; Curly-infix use
    {a + b}
  : a b c
    ;; Inline colon use
    + a : + b c
  : a b . rest
    ;; A multiline procedure application
    apply + a b
          . rest
PRECAP(case-lambda in Wisp)

P()
I like how it looks!
Despite inconsistencies and being absolutely useless, of course.
An ultimate test and result of our newly acquired understanding of Wisp!

P()
The whole reason for me exploring Wisp is the news of it being
A(https://www.gnu.org/software/guile/news/gnu-guile-3010-released.html, added to Guile 3.0.10)!
A(https://www.gnu.org/software/guile/manual/html_node/SRFI_002d119.html, The manual section is quite frugal, though).
So let this post be an introduction to Wisp syntax,
with the added benefit of self-sufficiency and absence of paren... the P-WORD, sorry.
Go try Wisp, you might end up liking it as much as I did!

P()
P.S. I was irritated by Guix not shipping Guile 3.0.10 for so long.
And using the SRFI-provided parser (either Python or Guile-specific one) wasn't cool.
So I made my own... as an CD(ed) script.
Find it at A(scripts/wisp.ed, scripts/wisp.ed)!
It produces correct results in 99% of cases and works everywhere CD(ed) does.
So... Can I call it the most portable Wisp implementation?

#include "template/feedback"
#include "template/footer"
