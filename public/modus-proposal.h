#define TITLE Modus: Proposal For a UNIX-y Text-mode Web Browser
#define DESCRIPTION "Bringing back text browsers, if that ever made sense."
#define IMAGE ""
#define IMAGE_ALT ""

#include "template/header"

P()
I'm dreaming of the browser.
The browser that's not gonna show me ads.
The browser that doesn't collect data on me.
The browser that's easy to use, and easy to use right.
The browser that turns pages more into more readable ones.
The browser that makes me more focused and productive.

P()
That's why I'm making Modus: I want such a browser to happen.

P()
Modus is an idea for a browser as a toolkit.
You have a browser core that

ULI Manages requests to remote parties.
LI  Processes the pages to readable representation.
LI  Exposes events and entities to the front-end.
END(UL)

P()
And then there is an arbitrary number of front-ends connectible to Modus:

ULI Ncurses-based terminal pager akin to w3m.
LI  GUI rich text-mode browser.
LI  Interactive REPL allowing you to talk to Modus, like you would with Connman.
LI  Modus as a web proxy, serving you minimalist versions of the pages you want. Like Gopher and many many many Gemini proxies.
END(UL)

P()
Modus is also a set of conventions, tightly integrated into *nix systems:

ULI Everything is overridable with environment variables.
LI  Any part of Modus can be replaced by configuring a shell command or arbitrary executable doing the work instead of it.
LI  The data Modus produces is easy to parse and page, so bring your own pager if you want to.
LI  Input and output happen to standard input/output, so Modus can dump to files or work for side effects if you make it to.
LI  Finally, Modus is a server, so you can connect to its socket and command it through there.
END(UL)

P()
Now, this sounds like a dream alright.
But I have a plan for how to make it work.
They only question: are you with me?
Ping me using the contacts from
AHERE(about, About AMP() Contacts page)
to tell me if you want to see such a browser?
I'm basically screaming into the void here, hoping to find the like-minded people.
And the application for my ideas, of course.

#include "template/feedback"
#include "template/footer"
