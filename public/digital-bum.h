#define TITLE Digital Bum: Finding a Home/lessness on the Internet
#define DESCRIPTION "Internet grew out of a non-commercial academic network with free resources for everyone. Can one get back to this dream of free Internet and build a lifestyle out of it? Well yeah I guess so, kinda worked for me."
#define IMAGE assets/tom-hanks-terminal.jpg
#define IMAGE_ALT "A middle-aged man uncomfortably sleeping on airport chairs with a coat under his head."

#include "template/header"

<figure>
IMG(IMAGE, IMAGE_ALT)
<figcaption>
Tom Hanks in The Terminal (2004, Steven Spielberg), playing a role of
the traveler stuck in the airport, trying to survive on nothing but what
he can attain in the terminal, and, eventually, get out.
</figcaption>
</figure>

P()
A month-ish ago, Namecheap shut down my VPS because I forgot to pay for it.
This VPS ran my
A(tripod, Tripod blog server).
Which means: my website has gone down for a quick minute.

P()
Having this moment allowed me to re-evaluate what I want from my website
and how I want to run it. I decided that

ULI I want no VPS for my website, just a static HTML hosting to push my files to and see the nice pages coming out of.
LI I don't need no analytics or even access logs—as long as I reach someone and am reachable by someone, I don't care about numbers.
LI I want an identity that'd be enough for me to be found, while not requiring me to sweat for even having it.
LI I need no fancy CDNs or some global caches. I'm not a bank to need nano-seconds access time. 14 KB would be more than enough for my HTML files.
LI EM(I don't want to pay anyone for what feels like a basic human right: being on the Internet and having a place there.)
END(UL)

P()
This small website outage inspired me to imagine myself a homeless Internet citizen
owning no estate and living off the public services.
Digital Bum, so to say.
Even though it kinda diminishes the seriousness of the real-world bums,
it is a good description of the free lunch lifestyle that I'm planning to have.

P()
So I've set out to outline the ways Digital Bum can exist on the Internet,
and what types of lifestyles they could have.

SECTION2(unconscious, Unconscious Bum: Don't Know, Don't Care)

P()
You don't want to pay for anything—it's free and fully usable already.

PRE()
Forget your lust for the rich man's gold
All that you need is in your soul
And you can do this, oh baby, if you try
All that I want for you, my son, is to be satisfied

And be a simple kind of man
Oh, be something you love and understand
Baby be a simple kind of man
Oh, won't you do this for me, son, if you can</pre>
PRECAP(Lynyrd Skynyrd, Simple Man)

P()
You're a normal person.
You're not pretentious: if there's an easy way to do something,
then it's the way you pick.
You use Gmail because it works just fine and the interface is okay-ish.
And Google account also comes with a productivity suite,
so why not use their Docs, Spreadsheets, or whatever other things they provide?

P()
If you need to create a website (usually just a landing page or portfolio)
or write a long read—you reach for something simple,
like Google Sites, WordPress, Blogspot, or Medium.

P()
Your page is prefixed and postfixed by some CD(sites.google.com)
or CD(facebook.com/post/WHaTeVerHaSH/), but who cares?
If anything, that's a sign of safety
and quality—you're a real person with a real story on a real platform.

P()
Your existence is effortless, and you believe that
A(https://killedbygoogle.com/, your web presence will last forever).

SECTION2(crafty, Crafty Bum: Will Fix… Hopefully)

P()
You don't want to pay for anything—you can make it fully usable yourself.

BQ() The only piece of technology in my house is a printer and I keep a gun next to it so can shoot it if it makes a noise I don't recognize.
BQCAP(Anecdotal saying about tech workers)

P()
You're a programmer or some other tech-savvy person.
You know that Google and Facebook are unreliable and too slow to be of any use.
You trust the products that value your productivity and give you control.
It's cool if your tools also have some privacy guarantees, but that's not critical.

P()
Gmail is okay, but
A(https://proton.me/,Protonmail)
and
A(https://tuta.com/, Tuta(nota)) privacy stance sounds a bit better to you.
They have well-designed web interfaces and snappy native apps for your iPhone, too.

P()
Your website on Github/Cloudflare/Gitlab Pages—it's simple and effective.
You wrap your website into a cozy Cloudflare DDOS protection and CDN.
Too convenient to refuse.
The domain is okay too—this github.io prefix means you set up CI
and have mastered Git to push your content to the Web.

P()
You often need to compute something,
but Google Collab and other cloud computing tools by big reliable companies
work just fine for you.

P()
Your existence is meaningful and productive.
Your content reaches your audience, especially your Twitter followers.
Some CEO dick tries to break one of your tools once every while,
but there are good drop-in alternatives, so you're absolutely safe.

SECTION2(political, Political Bum: In God We Do Not Trust)

P()
You don't want to pay for anything—you trust neither money nor institutions.

PRE()
I don't know why.
They "trust me"
Dumb fucks.
PRECAP(Mark Zuckerberg, leaked personal communication)

P()
You've seen the
A("https://pluralistic.net/2023/01/21/potemkin-ai/#hey-guys", enshittification) of Google, Facebook,
and Twitter (which you've been part of the Exodus from.)
Mastodon is much better—no ads, no algorithms, and no nazis on the prime time.
A(https://www.wired.com/story/the-man-behind-mastodon-eugen-rochko-built-it-for-this-moment/, This Rochko guy is shady),
but there's not much he can enshittify on the Fediverse, right?

P()
There are free (as in speech, but as in beer too!),
ethical, decentralized/federated, and community-run alternatives to all the vital services you need:

DL(Google Suite)
A(https://cryptpad.fr/, Cryptpad)

DD(Github/Cloudflare Pages)
A(https://tildeverse.org/, Toldeverse),
A(https://heliohost.org/, HelioHost),
A(https://ipfs.tech/, IPFS),
A(https://codeberg.page/, Codeberg Pages),
A(https://srht.site/, Sourcehut Pages)

DD(Gmail)
A(https://disroot.org/, Disroot),
HelioHost email,
A(https://riseup.net/, Riseup)
A(https://systemli.org/, systemli.org)

DD(Github)
A(https://gitea.io/, Gitea),
A(https://codeberg.org/, Codeberg), and
A(https://forgejo.org/, Forgejo).

DD(Messengers)
Matrix, XMPP, Signal.
END(DL)

P()
Your website runs on IPFS, and you got your domain from
A(https://nic.eu.org/, eu.org) or
A(https://eurid.eu/, EURid).

P()
Your life is somewhat clunky because the UX of all these services you use is not perfect.
But you're ready to bear with minor inconvenience
for the privacy and reliability your tools provide.
You're certain that your setup will last for long.
At least until
A(https://en.wikipedia.org/wiki/Carrington_Event, some major Internet catastrophe).

SECTION2(apocalyptic, Apocalyptic Bum: Watching Your Back)

P()
You don't want to pay for anything—everyone
will pay their share of blood and tears soon, so why settle for money?


PRE()
Danger angel
Black circles in the sky
There's nowhere left to run now
There's no place left to hide
With grim anticipation
The shadow bends behind
Descending like a sundown
And the night will leave you blind,
Look out
PRECAP(Larkin Poe, Danger Angel)

P()
You've seen shit.
Solar storms, Atlantic cable, Y2K bug, plastic islands.
Humanity's communication (and survival, but hush!) facilities are severely limited.
Any minor disruption—war, electricity outage, extremely bad weather—and
everyone's involuntarily off the grid.

P()
No technology is reliable, so you rely on as little of it as it's even possible:

ULI You use temporary email addresses when you need to communicate with someone.
Obviously, you encode your messages with a certain cipher agreed on beforehand.
But it's better to meet in person anyway.
LI You don't use phones—the radio is much more reliable.
(And you can occasionally peek into military freqs—these peeps might spill some important info on upcoming events.)
LI Your website is set up on Tor and served from a repaired 2003 laptop
refurbished with components from the garbage bins all around the town.
Obviously, with several layers of randomized proxies
based on the botnet you discovered last week.
LI When you need to compute something big, you just use the bandwidth and CPU resources
of those visiting your website with JS enabled,
or those users you can intercept the packages of.
In an especially daring case, you can proxy the results from
A(https://play.rust-lang.org/, Rust Playground) or some other unrestricted cloud computing thingie.
LI You have several friends (that you communicate with exclusively via
A(https://briarproject.org/, Briar))
with whom you plan on setting up a local alternative GPRS towers infra.
END(UL)

P()
Your life is continuously endangered, but so it everyone else's.
The only difference is: you're aware.

SECTION2(slow-down, Whoa, whoa, slow down maybe?)

P()
Okay, this last one was a bit over the top.
But still, the life of a Digital Bum is a point on the spectrum outlined above.

P()
I myself stopped on a paid domain (I'm still waiting for my nic.eu.org one),
A(https://docs.gitlab.com/ee/user/project/pages/, GitLab Pages) hosting, and
A(https://disroot.org/en/services/email, Disroot email).

P()
My journey got me throught the deserted lands of paid VPS-es,
privacy-endangering productivity suites, and mail hostings—to
a (potentially) free lifetime European domain,
community-maintained email server,
and a static hosting for my HTML
AHERE(this-post-is-lisp, compiled from Lisp). No,
AHERE(this-post-is-cpp, from C preprocessor), actually.

P()
What'll your journey be?

#include "template/feedback"
#include "template/footer"
