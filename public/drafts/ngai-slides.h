#define TITLE Anxiety (Sianne Ngai's Ugly Feelings)
#define SUBTITLE Male Intellectual's Fantazy
#define DESCRIPTION ""

#include "template/slide"

SECTION2(identity, Identity)

P()
Who am I?

ULI Male.
LI Feeling uneasy, if not anxious
LI Obsessed with cathartic expreriences.
LI Finding philosophers like Heidegger and Kierkegaard confusingly complex.
END(UL)

SECTION2(why, Why am I reading Sianne Ngai?)

ULI Stumbled upon her writing during The Sense of Humour (inst. Peter Jones).
LI Read several chapter of Ugly Feelings and got excited.
LI Forgotten about Ugly Feelings for a year.
LI And here am I, listening to lecture about Anxiety and remembering that Ngai had a chapter on it.
END(UL)

SECTION2(history, Ngai’s historic perspective on anxiety.)

ULI Negative affect?
LI Projection-related.
LI Spatial (<q>being thrown</q>).
LI Gendered: <q>We can hardly speak with propriety of castration anxiety where castration has already occurred.</q> (Freud as cited in Ngai).
LI Male intellectuals only.
END(UL)

SECTION2(detective, The Detective)

ULI Intellectual detective Scottie investigating cryptic women-related mystery.
LI Acrophobia & falling scenes.
LI Oscillating between 2+ dead/non-existent/projected-on females.
LI The corset and “a free man” fantasy.
LI A “do-it-yourself” ideology of entrepreneurship.
LI Thrown “objects”/”projections” (things, women and Scottie).
LI Scottie’s dream of being thrown into/from/as nothingness.
LI “sick” spectator becomes “healthy” male viewer/creator of female projection.
END(UL)

SECTION2(phenomenologist, The Phenomenologist)

ULI Heidegger.
LI Dasein, “being there”, “being thrown” as distanciation from one’s self.
ULI Distanciation = projecting the world state on oneself.
END(UL)
LI Mood as the disclosure of thrownness.
LI Dasein as ”a form of surrender to the holistic complex of ‘the world,’…” (Heidegger as cited in Ngai, italics by Ngai).
LI Understanding as projection, a possibility.
LI Falling (downward plunge) as everyday existence.
END(UL)

SECTION2(anxiety, Anxiety)

UL() Anxiety as “an understanding state-of-mind in which Dasein has been disclosed to itself in some distinctive way” (Heidegger as cited in Ngai.)
LI Anxiety as essential Dasein-based turning away from nothing/something.
ULI Different from fear because fear is merely stepping back from definite object.
END(UL)
LI Anxiety – nothing + nowhere. Meaninglessness of the world.
END(UL)

BQ()Anxiety not only saves Dasein from falling, but restores its individuality and capacity for projection (i.e., its capacity for understanding and interpretation), which is precisely the outcome of the anxious detective’s trajectory in /Vertigo/.
BQCAP(The saving anxiety (Heidegger, as cited in Ngai))

SECTION2(novelist, The Metaphysical Novelist)

ULI Text ~ Gender. ‘“sudden tremble” exactly mirroring the convulsions accompanying the “terrible vertigo” he feels while attempting to write.’
LI Male “Speechless though” vs Female “thoughtless speech”.
LI Speech of women as projection of male fantasy.
LI Writing and romantic relationships as Being to turn away from.
END(UL)

SECTION2(privilege, Anxiety as privilege)

ULI Redoubling the projection (“restoring the masculinity”), by projecting onto the “unknown, foreign feminineness” (someone cited by Ngai).
LI Restoration of individuality through this projection redoubling.
LI The exact character of aversion to femininity/uncertainty is what makes anxiety the main state of mind of the male intellectual.
END(UL)

SECTION2(therapeutic-effect, Therapeutic effect of Anxiety)

P()
The restoration of individuality of Dasein by anxiety is exactly its therapeutic effect of it. One simply moves from being anxious to a better understanding and straighter facing with Dasein.

SECTION2(problems, The problems are…)

ULI It’s implied as a male-only enterprise.
LI It still has objects—females and unknown. Is it actually this nothingness and object-less-ness?
END(UL)

</section>

#include "template/footer"
