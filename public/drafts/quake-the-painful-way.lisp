(:with-page ("Learning Quake 3 the Painful Way"
             "")
  (:p* *Given that my *Guix setup doesn't allow for much games out of the box\,
    *I'm trying to get all *I can from the games that (:em "are") available.
    *One of these games is "ioQuake"\, a free re-implementation of *Quake 3.)
  (:p* *I've never played *Quake\,
       so my experience here is clear\, unbiased\, and easy to reflect on.
       *Even though my journey is far from complete\,
       there are things that *I can't help noticing already.)
  (:section*
   :title "Non-vermin sensitivity"
   (:p* *I hate using mouse.
        *Be it in text editor "(I obviously use Emacs),"
        browser "(Nyxt)," or in videogames.
        *The last one is painful
        —there are not much games that are playable without mouse.
        *Some do support controllers as a keyboard/mouse alternative\,
        but that's usually the best it gets.
        *I've only a handful of games that are playable without mouse\:)
   (:ul*
    "Doom"
    "Gothic"
    (:li* "Hollow Knight"
          (:ul* "And platformers in general—the genre is slightly friendlier to
keyboard-only gamers than the gaming industry in general."))
    "And now, Quake III Arena.")
   (:p* *Yes\, you've heard it right\: *Quake is keyboard-only playable.
        *Quake 1 was commonly played without mouse
        "(these vermin weren't widespread at the time),"
        so *Arena should've retained some keyboard-friendliness
        by the virtue of having the same engine.)
   (:p* *The first bump\: camera controls are unbearably slow\,
        so one has to sustain two deaths before actually aiming at the enemy.
        *Built-in configuration interface doesn't allow configuring camera
        sensitivity for keyboard controls.
        *So one has to resort to dirty tricks\:)
   (:section*
    :title "Console and Autoexec"
    (:p* *A joy of *Emacs user\: there are config files "(autoexec.cfg, for one)" in *Quake\,
         and there's a "REP…" ahem! console to interactively act on the game state with text commands.
         *The number of commands and variables is mouth-watering too.
         *Take the (:a* "https://www.quakewiki.net/console/console-commands/quake-console-commands/"
                        "list of commands and variables on QuakeWiki")
         \, of use (:code "cmdlist") and (:code "cvarlist") commands in the console.)
    (:p* *The ones *I've been looking for were\:)
    (:ul*
     (:li* (:code "cl_pitchspeed") " for vertical camera")
     (:li* "And " (:code "cl_yawspeed") " for horisontal camera speed."))
    (:p* *So *I was just a couple of commands away from a perfect high-speed keyboard controls\:)
    (:pre*
     :code-p t
     :caption "Quake III Arena console commands to speed up keyboard camera controls"
     "seta cl_yawspeed \"350\"
seta cl_pitchspeed \"300\"")
    (:p* *And that's it—my game is fixed and *I can startle
         and panicky look around on any suspicious sound.
         *But wait\, how do I actually aim with these?))
   (:section*
    :title "Shotgun Sniper"
    (:p* *After setting the speed variables\,
         *I can try to rival mouse camera controls.
         *But! I cannot have precise aiming\,
         because keyboard camera step is so big
         it becomes a couple of meters wide on a long range.)
    (:p* *So any open area becomes a grave\, because any bot aims better than you do.
         *Most precise weapons "(like railgun or lightning gun)" become unusable\,
         because one'll die thrise before actually landing a hit on a moving target.)
    (:p* *This one is unfixable\, so *I adjusted\:
         *I avoid open spaces and ambush my enemies in the narrow passages.
         *Works alright\, but takes a bit of reflex to shotgun the enemy dead.)
    (:p* *Yes\, shotgun is my favorite weapon now\, because it's)
    (:ul*
     "Short-range."
     "Wide area."
     "And single key press to kill an average enemy."))))



;; Q3DM0 Introduction
;; - Weapons: machinegun, gauntlet, plasma gun, shotgun.
;; - Shooting
;; - Armor and health
;; Q3DM1 Arena Gate
;; - Weapons: rocket launcher
;; - Strafing
;; - Open spaces, closed spaces
;; Q3DM2 House of Pain
;; - Roaming the level
;; - Powerups: Haste
;; Q3DM3 Arena of Death
;; - Vertical gameplay
;; - Multiple enemies
;; Q3TOURNEY1: Power Station 0218
;; - Powerups: Regeneration, Quad Damage
;; - Dead ends and ambushes
