#define TITLE Ex Editor Cheat Sheet
#define DESCRIPTION "Ex is documented much less than ed is, so it's even harder to pick up. But it's also more powerful. Thus this cheat sheet: to make me (and you) more productive than ed allows, in ex."

#include "template/header"

<p>
This cheat sheet is heavily inspired by
A("https://catonmat.net/ed-unix-text-editor-cheat-sheet", ed cheat sheet),
but adapted to CD(ex), an editor more powerful than CD(ed), and installed everywhere as part of vi.
Writing this cheat sheet is an exercise for me to understand CD(ex).
But the supposed side-effect is that it might help you pick up CD(ex) too!

SECTION2(addressing, Line Addressing)

<p>
CD(ex)'s line addressing is wildly simplified compared to
CD(ed), so there are less special addresses and less fancy syntax.

TAB2(Address, Description)
TRO2(CD(.), The current line)
TRO2(CD(n), Nth line)
TRO2(CD($), The last line)
TRO2(CD(%), All the lines)
TRO2(CD(/re/), The next line matching CD(re))
TRO2(CD(?re?), The previous line matching CD(re))
TRO2(CD(SQUOTE()letter), The line previously
AID(,mark, mark)ed
with letter CD(letter))
TRO2(CD(''), The previous address used)
TABCAP(All the addressing symbols used by ex)

<p>
Addresses might come in pair separated by CD(,) or CD(;).

ULI In case of comma, both addresses are interpreted relative to the current address.
LI In case of semicolon,
 ULI first address is interpreted relative to the current one,
  LI while the second is interpreted relative to the first one.
 END(UL)
END(UL)

SECTION2(commands, Commands)

P()
Given that CD(ex) shares a lot of features with CD(vi),
its manuals are too cluttered with details on CD(vi)sual mode.
I've tried to retain only the
CD(ex)-specific parts.

P()
Note that most commands have their CD(!)-suffixed versions doing an alternative action.
I've listed these too.

TAB5(Command, Abbreviations, Arguments, Description, Bang-suffixed description)
#define TROC(COMMAND, ABBRS, ARGS, BANG, ...) <tr id=COMMAND><td>COMMAND<td>ABBRS<td>ARGS<td>__VA_ARGS__<td>BANG
TROC(abbreviate, ab, word expansion,, Add a new abbreviation, expanding to expansion when word is typed in input mode)
TROC(append, a,,Toggles AID(,autoindent, autoindent), Add new lines after the addressed one)
TROC(args,,,,List the provided files and CLI arguments)
TROC(change, c, count, Toggles AID(,autoindent, autoindent), Replace the addressed lines with new text)
TROC(chdir, cd, directory,, Change to directory or HOMEdir when not supplied)
TROC(copy, co/t, addr flags,, Copy the addressed lines after addr)
TROC(delete, d, buffer count flags,, Remove addressed lines and save them to buffer)
TROC(edit, e/ex, file, Edit even when there are unsaved changes, Edit a new file, cleaning the buffer)
TROC(file, \f,file,,Print the current edited file name or set it to file, when supplied)
TROC(global, g, /pat/cmds, Runs on unmatched lines instead (AID(,v, CD(v))), Run cmds on lines matching the pat.)
TROC(help, he,,, Show help message or open help buffer)
TROC(insert, i,, Toggles autoindent, Insert the inputed text before the addressed line)
TROC(join, j,count flags, Forces no-whitespace join, Joins the addressed lines to one line, and adds/collapses whitespace after every line.)
TROC(list, , count flags,, Print theaddressed lines in a more unambiguous way)
TROC(mark, k, letter,, Mark the addressed line with letter tag)
TROC(move, m, addr,, Move the addressed lines after addr)
TROC(mkexrc, mk, file, Overwrite the existent file, Write current setings to exrc file (default ~/.exrc))
TROC(next, \n, filelist, Supress warnings and discard unsaved changes, Edit next file provided via CLI, or replace CLI-provided list with filelist)
TROC(number, &num;/nu, count flags,, print addressed lines prefixed by line numbers)
TROC(preserve, pre,,,Preserve the file for later AID(,restore, restore))
TROC(previous, prev,, Supress warnings and discard usaved changes, Edit previous file provided via CLI)
TROC(print, p/P, count,, Print the addressed lines)
TROC(put, pu, buffer,, Puts AID(,delete, delete)d/AID(,yank, yank)ed lines after addressed one)
TROC(quit, q,, Quit discarding unsaved changes, Quit ex)
TROC(read, \r, file,, Copy the file text after the addressed line)
TROC(recover,, file,,Recover the previously preserved (due to crash of hangup) file)
TROC(rewind, rew,, Discard changes before rewinding, Rewind arglist and edit the first provided file)
TROC(set,, option[=value],, Print changed options or set option value)
TROC(shell, sh,,, Start a shell and continue editing after it is done)
TROC(source, so, file,, Reads and executes command from file)
TROC(substitute, s, /pat/repl/ options count flags,, Replace the pat-matched text with repl-acement)
TROC(suspend/stop, su/st,, Skip any saving and exit right away, Suspend the editor and back to the shell)
TROC(tag, ta, tag,, Switch to the tagged (CTAGS?) line)
TROC(tagnext, tagn,,, Edit the file with next context for tag)
TROC(tagprev, tagp,,, Edit the file with previous context for tag)
TROC(unabbreviate, una, word,, Delete abbreviation)
TROC(undo, \u,,, Undo the last command, except for file-related ones)
TROC(v,, /pat/ cmds,, Run cmds for lines not matching pat)
TROC(write, \w, file, Write to any file provided and ignore errors, Write to current file or provided file)
TROC(write>>, w>>, file,, Append buffer to the end of the file)
TROC(wq,,file, Like \w! and q!, Like wq)
TROC(xit,, file,, Writes any changes to buffer to respective file and exit)
TROC(yank, ya, buffer count,, Stores the addressed lines for further AID(,put, put))
TROC(z,, type count,, Print the count lines before (type="-"), around ('.'), or after (nothing))
TROC(!,,command, Repeat the last shell command, Send command to the shell, use addressed line as stdin)
TROC(=,,,,Print the addressed (defaults to $!) line number)
TROC(&,,options count flags,, Repeat the previous AID(,subtitute, subtitute))
TROC(~,,options count flags,, Replace the previous CD(/re/) with last AID(,subtitute, subtitute) replacement)
TABCAP(ex Commands)

SECTION2(options, Configuration Options)

<p>
One can set these using AID(,set, set) command from above.
For boolean ones, CD(set name) is enough to toggle the option on and CD(set noname) to turn it off.
For non-boolean the form is CD(set name=value)

TAB4(Option, Abbreviation, Default, Description)
TRO4(autoindent, ai, false, )
TRO4(autoprint, ap, true, Print the current line after most editing commands)
TRO4(autowrite, aw, false, Auto-save the current file when moving between files)
TRO4(backup,, "", Backup files before overwrite)
TRO4(beautify, bf, false, Strips all non-graphical control characters from the user input)
TRO4(cdpath,, CDPATH, Directory used as prefix for AID(,cd, cd) command)
TRO4(directory, dir, TMPDIR or /tmp, Directory to store buffer files)
TRO4(errorbells, eb, false, Precede error messages with the bell character)
TRO4(exrc,, false, Search the current directory for an .exrc file in addition to HOME/.exrc)
TRO4(extended,, false, Use egrep-like extended regex)
TRO4(flash, fl, true, Use visual bell instead of the audible bell)
TRO4(hardtabs, ht, 8, Boundaries for tabs)
TRO4(iclower,, false, Make regex case-insensitive, unless there are upper-case letters)
TRO4(ignorecase, ic, false, Make regex ignore the case of text)
TRO4(list,, false, Make default printing more unambiguous, as per CD(list) command)
TRO4(lock,, true, Try to lock the edited files)
TRO4(modelines, ml, false, Check first and last 5 lines for ex commands with CD(ex:) prefix and evaluate these)
TRO4(noprint,, "", Characters never handled as printable)
TRO4(number, nu, false, Default printing to print line numbers)
TRO4(octal,, false, Display special/unknown chars as octal, not hex)
TRO4(open,, true, Allow open and visual modes)
TRO4(print,, "", Characters always handled as printable)
TRO4(prompt,, true, Whether to show the CD(:) prompt)
TRO4(readonly, ro, false, Mark the session as read-only)
TRO4(remap,, true, Make macro expansion recursive, until there is nothing to expand)
TRO4(report,, 5, Any command modifying more than CD(report) lines must report the extent of changes)
TRO4(scroll, scr, 1/2 of the window, How much to scroll with AID(,z, z))
TRO4(secure,, false, Turn off access to external programs)
TRO4(shell, sh, /bin/sh, Shell for AID(,!, !))
TRO4(tabstop, \ts, 8, Tab width)
TRO4(taglenth, tl, 0, Maximum tag length, unlimited if 0)
TRO4(tags, tag, tags /usr/lib/tags, Paths to tag files)
TRO4(terse,, false, Shorter errors)
TRO4(warn,, false, Warn on unsaved changes)
TRO4(wrapscan, ws, true, Make regex search wrap around the end of file)
TRO4(writeany, wa, false, Disable checks for AID(,write, write))
TABCAP(ex Options)

#include "template/footer"
