(:with-page ("Comparing Common Lisp Debugging Utilities—Any Use?"
             "Common Lisp standard specifies a dozen of utilities to inspect,
debug, pull in, and tear apart any object inside the running
image. But what about implementations supposed to provide these
facilities?")
  (:p* *I can\'t stop being amazed at how much stuff there is in *Common *Lisp.
       *It basically is a proper "IDE" on its own.
       *Unless implementations are not slacking off in implementing all the standard "APIs."
       *This post goes through standard debugging utilities and how implementations implement these.)

  (:section*
   :title "So what is it? Yes of no?"
   (:p* *Cancel. "OK." *Cancel. *Cancel. "OK." — dialogs on top of dialogs\,
        all with the same two buttons\, same idea—you either agree or disagree.
        (:a* "JavaScript has it"
             "https://developer.mozilla.org/en-US/docs/Web/API/Window/confirm")
        \, any "GUI" toolkit tutorial starts with a small dialog\,
        lots of survey forms jump you with " \"yes\" or \"no\"" types of questions!
        "—Because" binary choice is useful)
   (:ul*
    "to cancel the operation,"
    "to ensure the conscious choice for some irreversible action,"
    (:a* "or just because you like \"Oui ou Non\" by Angèle"
         "https://www.youtube.com/watch?v=XqAiGeEzctQ"))
   (:p* *Which means\: if this idea exists—then "it's" there in "CL" standard too\:
        (:a* "https://cl-community-spec.github.io/pages/y_002dor_002dn_002dp.html"
             (:code "yes-or-no-p/y-or-n-p")) ! )
   (:section*
    :title "yes-or-no-p/y-or-n-p implementation support"
    (:p* *There are several features that the standard requires from these "\"Oui ou Non\"" functions\:)
    (:dl*
     "both"
     (:ul*
      "print a prompt, if any"
      "print out the \"(Y or N)\"/\"(Yes or No)\"-style message")
     "y-or-n-p"
     (:ul*
      "require the user to answer a yes-or-no question with a single character")
     "yes-or-no-p"
     (:ul*
      "require the user to take more action than just a single keystroke"
      "attract the user's attention (for example, by ringing the terminal's bell)"))
    (:p* *The stuff's pretty basic\: print the prompt\,
         print the yes-or-no prompt\,
         attract attention\,
         and read the response.
         *Preferably\, handle malformed responses.
         *Here's how implementations support these simple requirements\:)
    (:table*
     :width 7
     :caption "Implementation support for y-or-n-p/yes-or-no-p features"
     "Implementation" "Legend" "Prompt not provided" "Prompt" "Attracts attention" "Malformed: message" "Malformed: repeats prompt?"
     "SBCL" "(y or n)/(yes or no)" "Legend" (:td* "Good:" (:code "PROMPT (LEGEND) INPUT")) "Yes (bell)" "Please type \"y\" for yes or \"n\" for no." "Yes"
     "CCL" "(y or n)/(yes or no)" "Legend" "Good (ditto)" "N/A" "Please answer y or n." "Yes"
     "ECL/GCL" "(Y or N)/(Yes or No)" "Nothing" "Good (ditto)" "N/A" "None" "Yes"
     "CLISP" "(y/n)/(yes/no)" "Nothing" "Good (ditto)" "N/A" "Please answer with y or n :" "No"
     "Allegro" "(Y or N)/(Yes or No)" "Legend" (:code "PROMPTINPUT") "N/A" "Type \"y\" for yes or \"n\" for no. " "Yes"))
   (:section*
    :title "Graven Image's y-or-n-p/yes-or-no-p"
    (:p* "Yes/no" prompting is not uniformly usable across implementation.
         *It would benefit from some polish\, for sure.
         *Thus the *Graven *Image (:code "y-or-n-p*") and (:code "yes-or-no-p*") \:)
    (:dl*
     "Legend:"
     "(y[es] or n[o]), because it's annoying to type in full \"yes\""
     "Empty prompt:"
     "Same as legend."
     "Prompt:"
     (:code "PROMPT (LEGEND) INPUT")
     "Attracts attention"
     "No, because it's annoying. You can re-configure it, though."
     "Malformed: message"
     "None, but..."
     "Malformed: repeats prompt"
     "Yes")
    (:p* *And\, as a bonus\:)
    (:ul*
     "They're re-implemented as generic functions (thus the point on re-configuring beeps.)"
     (:li* "Less conventional answers, like " (:q "ay") " or " (:q "nope")
           " are supported and customizable."))))

  (:section*
   :title "Lambda Expression"
   (:/ function-lambda-expression is probably the most introspective part of the standard\:)
   (:ul*
    "it allows to get the name of the function,"
    "closed-over values,"
    "and, potentially, the source expresion")
   (:/ *Well\, given that it's implemented properly.
       *Most implementations don't try very hard at returning the useful values.
       *This simplest query for a defined function with obvious source expression returns\:)
   (:pre*
    :caption "Example of running the simplest function-lambda-expression on most implementations"
    '(function-lambda-expression #'identity)
    ";; => NIL
;; NIL
;; IDENTITY")
   (:/ *Here's how it fairs\:)
   (:section*
    :title "Lambda fetching"
    (:/ *Making it a nice table comparing implementation support.
        *Lambda expression fetching "(mostly taken from the "
        (:a* "https://cl-community-spec.github.io/pages/function_002dlambda_002dexpression.html"
             "spec page for" (:code "function-lambda-expression")) ")" \:)
    (:table*
     :caption "Lambda fetching"
     :width 6
     "Implementation" "Literal lambda" "Closed-over lambda" "Flet" "Built-in (IDENTITY)" "REPL-compiled function"
     "SBCL"           "Yes"            "Yes"                "Yes"  "No"                  "Yes"
     "CCL"            "No"             "No"                 "No"   "No"                  "No"
     "ECL"            "Yes"            "Yes"                "Yes"  "No"                  "Yes"
     "ABCL"           "Yes"            "Yes"                "Yes"  "No"                  "Yes"
     "CLISP"          "Yes"            "Yes"                "Yes"  "No"                  "Yes"
     "Allegro"        "Yes"            "Yes"                "Yes"  "No (obviously)"      "Yes")
    (:/ *So far\, so good.
        *No built-in function inspection anywhere.
        *And nothing on "CCL."))
   (:section*
    :title "Closure inspection"
    (:/ *Enter closures\:)
    (:table*
     :caption "Closure inspection return values"
     :width 6
     "Implementation" "Literal lambda" "Closed-over lambda" "Flet"    "Built-in (IDENTITY)" "REPL-compiled function"
     "SBCL"           "T"              "T"                  "T"       "T"                   "T"
     "CCL"            "NIL"            "T"                  "T"       "NIL"                 "NIL"
     "ECL"            "NIL"            "Value alist"        "NIL"     "NIL"                 "NIL"
     "ABCL"           "NIL"            "Opaque env"         "NIL"     "T"                   "NIL"
     "CLISP"          "Vector*"        "Vector*"            "Vector*" "NIL"                 "Vector*"
     "Allegro"        "NIL"            "Opaque env"         "NIL"     "NIL"                 "NIL")
    (:/ "SBCL" seems to slack off and always return *T.
        *It's a behavior that standard allows\, but one can do better.
        *Other implementations are more honest\,
        but often have their opaque and not-immediately-useful closure formats.
        *The asterisk on "CLISP" means that it returns a huge vector for the actual environment.
        *One has to know what this vector means
        and the vector itself is often filled with meaningless "NILs,"
        but it is nice to have a full env at hand.))
   (:/ *Names are all portable and meaningful\, only differing in block/flet name syntax.)
   (:/ *Simple examples from the standard work alright\,
       but once it gets to real and useful functions\,
       (:code "function-lambda-expression") goes silent.
       *It can do better.
       *Here are some ideas for how exactly\:)
   (:ul*
    "Handling fbound symbols in addition to functions."
    "Handling generic function."
    "And methods."
    "And funcallable instances."
    "And macros?"
    "Returning closures in a portable format."
    "Constructing lambdas from reliable source."
    "Getting the function type (not really standard, but one can dream...)"))
  ;; TODO:
  ;; - apropos
  ;; - break, *debugger-hook*, invoke-debugger
  ;; - describe
  ;; - inspect
  ;; - documentation
  ;; - time
  ;; - trace, untrace
  ;; - step
  ;; - ed
  ;; - dribble
  ;; - file-system operations
  )


