#define TITLE Making Sense of Lambda Calculus 2: Numerous Quirks of Numbers
#define DESCRIPTION "Lambda Calculus is extremely elegant, including in how it handles numbers. But this elegance often comes at the cost of understandability. This post goes through examples of arithmetics in Lambda Calculus to understand how they work."
#define IMAGE "assets/thumbnail-CD7F32-FFFFFF-5E5184.png"
#define LIGHT_COLOR HASH()ffffff
#define DARK_ACCENT_COLOR HASH()cd7f32
#define LIGHT_ACCENT_COLOR HASH()5e5184

#include "template/header"

P()
With
AHERE(lambda-0, basic terms) and
AHERE(lambda-1, order of evaluation) out of the way,
we can finally get to practical things.
Like numbers.

P()
Numbers in Lambda Calculus (LC) are usually represented by Church Numerals.
These are usually two-argument functions taking a function and a value.
Numbers apply the function to the value N times and return the result.
Here are some numbers:

PRE()
0 = λfx.x
1 = λfx.f x
2 = λfx.f (f x)
5 = λfx.f (f (f (f (f x))))
...
PRECAP(Examples of Church Numerals)

P()
There's also an encoding of numbers with pairs.
But that's too much overhead for questionable benefit.
So let's stick with regular Church numerals and see that...

SECTION2(compositions, Numbers Are Compositions)

P()
This wasn't immediately clear: numbers are compositions.
A Church numeral is applying a function it's given a certain number of times.
So you can compose a function with itself by passing it to a number.
Elegant, yet non-obvious.

P()
Imagine you want a function taking the fifth (zero-indexed) element of the linked list.
So you strip off list beginnings five times and then get the fifth element directly.
Implying that CD(tail) returns the tail of the list and CD(head) returns the head:

PRE()
head ((5 tail) list)
head (((λfx.(f (f (f (f (f x)))))) tail) list)
head ((λx.(tail (tail (tail (tail (tail x)))))) list)
head (tail (tail (tail (tail (tail list)))))
PRECAP(Example of numbers as function compositions)

SECTION2(succ, Successor)

P()
So numbers are clearly compositions.
If we want to increase some number, we can simply take this number and compose another function call on top of it.
That's the logic of Successor function:

PRE()
succ = λnfx.f (n f x)
succ 5
succ (λdy.(d (d (d (d (d y)))))) // α-substitution fx -> dy
λfx.f ((λdy.(d (d (d (d (d y)))))) f x)
λfx.f (f (f (f (f (f x))))) // η-reduction
PRECAP(Successor function)

SECTION3(bracket, Form-closing Bracket)

P()
These closing parentheses are getting unruly.
I have to introduce another notational element to handle them.
I'll use <code>]</code> (as the Interlisp homage) at the end of deeply nested form.
That'll mean "arbitrary number of closing parentheses".
The final function in the example above can be rewritten as:

PRE()
λfx.f (f (f (f (f (f x]
PRECAP(6 in the bracket-terminated form)

P()
Hopefully that's not too off-putting.

SECTION2(add, Addition and Peano Arithmetics)

P()
Peano arithmetics—as far as I was able to understand it—
is all about building a complete set of numeric operations out of
constants and successor operator.
It's also possible to implement these arithmetics in the toolkit Lambda Calculus provides.
Building on the foundations:

ULI Constants are Church numerals like <code>λfx.\f (\f (\f (\f (\f (\f x]</code>.
LI Successor operation is CD(succ).
LI CD(m+n) Addition is applying CD(succ) to CD(m) an CD(n) number of times.
LI Multiplication is... let's leave it for later.
END(UL)

P()
So here's how addition formula based on
A(https://en.wikipedia.org/wiki/Peano_axioms, Peano axioms)
looks in Lambda Calculus:

PRE()
// Peano
a + 0 = a
a + S(b) = S(a + b)
// Lambda Calculus:
add = λmn.n succ m,
PRECAP(Translating Peano addition to Lambda Calculus)

P()
While these look quite different, the idea is the same:
apply the successor CD(n) times to get CD(n+m).
Wikipedia page has a good example that I won't be able to reproduce here.
But, believe me, these are the same.

SECTION2(pred-and-sub, Deadly Predecessor & Subtraction)

P()
Now what Peano arithmetics don't cover is subtraction.
There are no negative numbers, so there's no "subtraction is addition of negative number".
So we need to somehow strip off some function applications from the number.
The smart piece of code that Wikipedia lists is:

PRE()
pred = λnfx.n (λgh.h (g f)) (λu.x) (λu.u)
PRECAP(Predecessor function)

P()
Here's some reduction to show how it works.
Beware: it's quite overwhelming!

PRE()
pred 3
(λnfx.n (λgh.h(g f)) (λu.x) (λu.u)) 3
(λnfx.n (λgh.h(g f)) (λu.x) (λu.u)) (λfx.f(f(fx]
λfx.(λgh.h(g f)) ((λgh.h(g f)) ((λgh.h(g f)) (λu.x))) (λu.u)
λfx.(λgh.h(g f)) ((λgh.h(g f)) (λh.h x)) (λu.u)
λfx.(λgh.h(g f)) ((λgh.h(g f)) (λh.h x)) (λu.u)
λfx.(λgh.h(g f)) ((λgh.h(g f)) (λd.d x)) (λu.u)
λfx.(λgh.h(g f)) (λh.h((λd.d x) f)) (λu.u)
λfx.(λgh.h(g f)) (λh.h(f x)) (λu.u)
λfx.(λgh.h(g f)) (λv.v(f x)) (λu.u)
λfx.(λh.h((λv.v(f x)) f)) (λu.u)
λfx.(λh.hf(f x)) (λu.u)
λfx.((λu.u) f(f x))
λfx.f(f x)
PRECAP(Reduction of "pred 3" expression)

P()
Essentially (you probably got it just from looking at it, right?)
what it does is introduce a proxy function that gets passed to the number.
The first application of this function is returning CD(x).
And the rest of the applications just apply CD(f) to it.
So it's stripping off one innermost call to
CD(f) from the number.
Scary, but useful.

P()
Now that the predecessor is covered, subtraction is a piece of cake.
The principle is the same as addition: just apply predecessor N times:

PRE()
sub = λmn.n pred m,
PRECAP(Subtraction function)

P()
Quite elegant...
if we don't consider the underlying horrors of
CD(pred).

SECTION2(mult-and-expt, Multiplication and Exponentiation)

P()
Multiplication and exponentiation are beautiful in LC.
But they are not necessarily apparent in how they work.
So I'll try to make some reductions and explain how these make their magic.
Here's multiplication:

PRE()
mult = λmnf.m (n f)
PRECAP(Multiplication of m×n in Lambda Calculus)

P()
The idea is clear: take CD(n), apply it to CD(f), and repeat that CD(m) times.
But once you squint at it, it unfolds into a series of questions:

ULI Where is CD(x) in it? The zero/init/base value, I mean.
LI  How does
A("lambda-0#currying", currying)
work in there?
LI Is applying one number to another going to be useful?
END(UL)

P()
To deepen the feeling of displacement, here's the exponentiation function:

PRE()
pow = λbe.e b
PRECAP(Raising b to the power of e in Lambda Calculus)

P()
This is basically this application of one number to another one.
Quite intuitive on the surface.
But it gives me a stack overflow when I try to mentally reduce it.
So here are written reduction to understand CD(mult) and CD(pow):

PRE()
mult 2 3
(λmnf.m (n f)) 2 3
(λmnf.m (n f)) (λfx.f (f x)) 3
(λmnf.m (n f)) (λfx.f (f x)) (λfx.f (f (f x]
(λnf.(λfx.f (f x)) (n f)) (λfx.f (f (f x]
λf.(λfx.f (f x)) ((λfx.f (f (f x))) f)
λf.(λgy.g (g y)) ((λhz.h (h (h z))) f) // α-substitution
λf.(λgy.g (g y)) (λz.f (f (f z)))
λfy.(λz.f (f (f z))) ((λz.f (f (f z))) y) // NB
λfy.(λz.f (f (f z))) (f (f (f y]
λfy.f (f (f (f (f (f y]
λfx.f (f (f (f (f (f x] // SIX!!!!!
PRECAP(Reduction of multiplication over 2 and 3)

P()
So what happens here is indeed this curried number thing.
CD(n) is applied to the base value (implied here) CD(m) times.
The biggest plot twist there is the line marked with CD(NB):
the number with CD(f) being curried inside it is getting applied exactly CD(m) times.
Which is the same as I said earlier, but now proven by this small reduction
(at least compared to the predecessor one.)
Now the tasty stuff with even more levels of implied currying:

PRE()
pow 3 2
(λbe.e b) 3 2
(λbe.e b) (λfx.f (f (f x))) 2
(λbe.e b) (λfx.f (f (f x))) (λfx.f (f x))
(λe.e (λfx.f (f (f x))))  (λfx.f (f x))
(λf.λx.f (f x)) (λfx.f (f (f x)))
λx.(λfx.f (f (f x))) ((λfx.f (f (f x))) x)
λx.(λfx.f (f (f x))) ((λfy.f (f (f y))) x) // α-substitution
λx.(λfx.f (f (f x))) (λy.x (x (x y))) // NB
λx.(λfz.f (f (f z))) (λy.x (x (x y))) // α-substitution
λxz.(λy.x (x (x y))) ((λy.x (x (x y))) ((λy.x (x (x y))) z))
λxz.(λy.x (x (x y))) ((λy.x (x (x y))) (x (x (x z)))
λxz.(λy.x (x (x y))) (x (x (x (x (x (x z]
λxz.x (x (x (x (x (x (x (x (x z]
λfz.f (f (f (f (f (f (f (f (f z]  // α-substitution
λfx.f (f (f (f (f (f (f (f (f x]  // α-substitution
λfx.f (f (f (f (f (f (f (f (f x] // NINE!!!
PRECAP(Reduction of exponentiation of 3 by 2)

P()
Now what happens here, especially on the line marked with CD(NB), is a crime.
Crime against static type systems and fundamentalist minds.
The value that's supposed to be a base/zero/init value...
Is used as a function.
This makes Lambda Calculus both more cryptic and beautiful:

ULI Functions are all there is to LC.
LI  Even the ones supposed to be scalar values.
LI  Even the ones supposed to be "functions".
LI  And even the ones that oscillate between the two!
END(UL)

P()
This is a takeaway that the exponentiation reduction refreshed in my mind:
(After all, it's been almost 4 months since the last Making Sense of Lambda Calculus post.)
Don't be afraid to break the supposed contract your lambdas are establishing.
They are just that: lambdas.
Nothing else.

P()
But there are cases when even the smart currying,
re-interpretation of data as functions and vice versa,
and other arcane stuff doesn't help.
Like division:

SECTION2(div, Is There Division?)

P()
Division is always a problem.
It's not following many rules that addition and multiplication establish.
And there's a whole page on
A(https://en.wikipedia.org/wiki/Division_algorithm, division algorithms)
on Wikipedia.

P()
The simplest algo is the subtraction-based one.
Basically subtracting the denominator from the numerator until it's below the denominator.
Here's how
A(https://github.com/aartaka/stdlambda, stdlambda)
implementation of division works:

PRE()
div = λnd.Y (λr.λq.(lt q d) 0 (succ (r (- q d)))) n
PRECAP(Division implementation based on recursive subtration)

P()
A lot of things there:

DL(Y)
Y combinator for recursion on CD(r)
DD(lt)
Less than function working as a termination condition.
DD(succ)
Successor function from above.
DD(sub)
Subtraction.
END(DL)

P()
Putting it all together:

ULI Apply the function to numerator (CD(n)) and denominator (CD(d))
LI  If the quotient (CD(q), numerator initially) is less than the denominator, then return 0.
LI  Otherwise, subtract the denominator from the quotient and recurse.
LI  Add one to the recursive call result every time we subtract the denominator.
LI  Once the recursion terminates, 0 turns into the number of denominator subtractions.
LI  Which is basically what a division is.
END(UL)

P()
And yes, that's the simplest implementation I can think of.
No elegant stuff like applying numbers to each other.
A(https://justine.lol/lambda/, Justine Tunney lists a division function in her "Lambda Calculus in 383 Bytes"),
but I was not able to decipher how it works.
I'm stuck with advanced things like Y combinator for the seemingly simple operation: division.

SECTION2(up-next, Up Next: Truth or Dare With Booleans)

P()
Summarizing all of the above:

ULI Numbers are compositions.
LI  Increasing them is a matter of composing yet another function on top.
LI  Decreasing/subtracting them is... complicated.
LI  Multiplication and exponentiation are beautiful.
LI  While division is... complicated.
END(UL)

P()
One type of numeric operations that I haven't mentioned is comparisons.
Because comparisons get us into the uncharted lands of booleans.
Reviewing booleans is a big topic beyond this (already huge) post.
So booleans come next, in Making Sense of Lambda Calculus 3!

#include "template/feedback"
#include "template/footer"
