#define TITLE Using ed(1) as My Static Site Generator
#define DESCRIPTION "You can tell I'm an ed fan. \
Not only do I use it for esoteric challenges or compiler building, it also is my new SSG \
(Static Site Generator.) \
So here's how it works:"

#include "template/header"

P()
I have a long story with bipolar spikes making me re-evaluate my tech choices.
You can observe it with multiple of my website setups:
AHERE(tripod, I started with Tripod, my Gemtext-oriented OOP Lisp engine).
AHERE(this-post-is-lisp, Then I used the DSLs Common Lisp provides to generate HTML from s-expressions).
AHERE(this-post-is-cpp, And, finally, I switched to C Preprocessor).
And here's a new flare-up, I guess?

P()
This time I'm testing the limits of what ed-family editors can do.
AHERE(paren-regex, Including parsing Lisp).
AHERE(sed-ed, And using ed as an everyday scripting environment).

P()
I've also tried ex/vi (not Vim!)
A(https://github.com/aartaka/emvi, To the point of making a Lisp programming config for it).
A(https://merveilles.town/@aartaka/113546886276193594, But ex/vi are too troublesome, so forget it).

P()
Back to ed then!
A(https://github.com/aartaka/modal.ed, Here's a compiler from high-level Modal system to ed, written in ed itself).
A(scripts/wisp.ed, Here's a script processing Wisp syntax extension for Lisps).
A(https://rosettacode.org/wiki/Category:Ed, And here's a whole category for ed tasks I solved on RosettaCode).
The overarching narrative can be a transformation from one format to the other.
Like... what site generators do?
So I can use ed as my site generator, and get away with it?

SECTION2(setup, Setup: Glorified ed Scripts)

P()
So the way ed scripts are ran:

PRE(sh)
ed -s file.html < script.ed
PRECAP(Example script run)

P()
This incantation

ULI Opens a CD(file.html),
LI  Reads commands from stdin (redirected CD(script.ed).)
LI  And saves/writes (CD(w) command) file if there are respective commands in the stdin.
END(UL)

P()
A huge advantage is that I can swap commands in the script file.
Replacing writing commands to printing ones, for example.
This way, I can print the content of the post without modifying it.
Y'know, for blog debugging (everyone does that?)

P()
With that general formula, I can abstract article builds per format with two scripts:
a preprocessing (deabbreviation and escaped char expansion) script and the actual conversion.
Here's how
A(makefile, my Makefile rule for .txt building)
looks like:

PRE(make)
%.txt: %.h
	-cp $^ $@
HYPERTEXTONLY(	-ed -s $@ < A(scripts/preprocess.ed, scripts/preprocess.ed))
HYPERTEXTONLY(	-ed -s $@ < A(scripts/totext.ed, scripts/totext.ed))
EXCEPTHYPERTEXT(	-ed -s $@ < scripts/preprocess.ed)
EXCEPTHYPERTEXT(	-ed -s $@ < scripts/totext.ed)
PRECAP(Makefile implicit rule for conversion from .h to actual output text files)

P()
Repeat this for every format (my site supports five atm) with dedicated CD(to*.ed) scripts.
And that's it!

SECTION2(advantages, Advantages)

P()
I can use whatever syntax I want now!
With C Preprocessor, I was restricted to CD(X(ARG1, ARG2, ...)) structure for all my elements.
With ed, I can go crazy with something like CD(LT()cd code-here>), imitating self-closing HTML tags.
Or whatever.
Closer to the way I model my writing.

P()
Another upside: I don't have to rewrite all of my posts in a new format.
My ed scripts are processing the old preprocessor-oriented format too!
So ed is capable of replacing C Preprocessor and even adds some features on top.

P()
One more advantage is that there no longer are weird chars breaking everything.
When I see an apostrophe, my eye starts twitching.
C Preprocessor recognizes apostrophe as a char marker and reads the rest of the line verbatim.
Same goes for double quote and hash.
Special chars, forbidden magic.

P()
In general, ed has this sloppiness of regex/substitutions.
And interactivity of a REPL (well, to an extent.)
Perfect for text processing and document authoring.

SECTION2(limitations, Limitations)

P()
ed is stupid (I say lovingly.)
It has no file inclusion, for one.
So C Preprocessor's CD(HASH()include) is no longer accessible.
I manage without it, because most of the files I include are easily hard-codeable.
But the mere inability to include arbitrary files is somewhat alarming.

P()
Another problem that I already got the taste of:
having arbitrary substitutions at the fingertips spoils me.
I might end up with some really otherworldly syntax without noticing.
And I wouldn't be able to rollback to e.g. C Preprocessor to generate my posts.
But then, ed is so simple and more or less standard, so what can go wrong?

DETAILS(It did go wrong)
P()
Arch and Alpine package slightly different versions of ed.
Alpine version was breaking on line joining and variable substitution.
So I had to swap Alpine for Arch on the Gitlab CI.
I would have been able to stay on Alpine if I was able to debug what's wrong with ed there.
But Gitlab (like any other CI platform?) doesn't make it easy to debug things on CI.
END(DETAILS)

SECTION2(you-too, Should You Use ed As Site Generator?)

P()
No, not at all.
It's a weird choice that's only there because I'm having a new phase.
But still, it's fun!
Try ed if you have too much free time and are bored out of your mind 😜

#include "template/feedback"
#include "template/footer"
