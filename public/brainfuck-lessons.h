#define TITLE What Writing 2K+ Lines of Brainfuck Taught Me
#define DESCRIPTION "I am a Brainfuck programmer. \
I made some projects that caused me immence pai... joy. \
I learned something from these, and you can too!"
#define IMAGE "/assets/thumbnail-76FF7A-FF00FF-FFFFBB.png"
#define LIGHT_COLOR HASH()ffffcc
#define LIGHT_ACCENT_COLOR HASH()00a806
#define DARK_ACCENT_COLOR HASH()ff00ff

#include "template/header"

P()
Programming in Brainfuck is fun.
No, that's not the tears of pain streaming down my face.
I write Brainfuck almost professionally.
At least I founded
A(https://github.com/bf-enterprise-solutions, BF Enterprise Solutions).
Some of the products I made:

ULI A full-featured operating system,
A(https://github.com/bf-enterprise-solutions/os.bf, OS.bf).
LI  Actually usable text editor,
A(https://github.com/bf-enterprise-solutions/ed.bf, ed.bf).
LI  A meta-circular evaluator for Brainfuck itself written in Brainfuck,
A(https://github.com/bf-enterprise-solutions/meta.bf, meta.bf).

END(UL)

P()
So I had my share of blood, sweat, and tears.
Here's what I've got from it.

SECTION2(patience, Patience)

P()
Some of my projects are big enough to do something useful.
They are also big enough to have enough chaos in them.
In case you make a typo in the code, they work.
They don't miscompile, they don't fail type checking.
Mostly because it's Brainfuck and there's no type checking.

P()
So yes, they work... In a slightly broken way.
Some inputs simply cause infinite loops.
Some inputs make the program stop responding.
Some inputs happen... even if the program requires no input!
Okay, that last one was an exaggeration.
But still, the point stands: broken code doesn't break immediately.

P()
See commit
A(https://github.com/bf-enterprise-solutions/meta.bf/commit/6f0b33b16bc9122c876a48ba0f8e37d3250c5c2c, 6f0b33b)
for the example of several typos that broke recursive bracket evaluation in meta.bf.

P()
Debugging is simple: just run the program in the
A(https://www.nayuki.io/page/brainfuck-interpreter-javascript, memory-displaying interpreter).
And step through every command tracking what happens to the memory.
Yes, every single command.
Even if the program is half a thousand lines long.

P()
I am a much more patient person now that I wrote my share of Brainfuck.
<del>I will automatically qualify as Embedded Software Engineer.
Debugging a running hardware is nothing compared to debugging opaque Brainfuck programs.</del>

SECTION2(mental-debugging, Mental Debugging)

P()
I must admit that stepping through every command is deadly.
It's often impossible or too long.
Yes, I had half an hour long session of one pass through the buggy program.
And there are several such sessions one needs to find and fix the bug.
So the only adaptation strategy I got is stepping through the code in my head.

P()
Many C programmers write screenfuls of code without trying it out.
Compiler often starts screaming. A lot. Panicky.
But the more code one writes, the less compiler warnings they get.

P()
So again, I'm a better C (or any compiled language really) programmer now.
I can compile and run arbitrarily complex code mentally.
And debug it too.

SECTION2(data-over-algorithms, Data Over Algorithms)

P()
I'm often lying awake at night and iterating on Brainfuck code in my head.
Some algorithm I can improve, or some data layout that my programs can benefit from.
Especially when that relates for string/array algorithms from
A(https://github.com/bf-enterprise-solutions/str.bf, str.bf),
like
A(https://github.com/bf-enterprise-solutions/str.bf/commit/e067d73a1d82f8b4f7d42cdeacb67a001cee74c6, swap.bf optimization commit e067d73).

P()
Data layouts are extremely important.
A good data layout might eliminate a lot of copying loops and minimize memory use.
It's also a good way to reason about programs.
That's why
A(https://github.com/bf-enterprise-solutions/bf.doc, bf.doc)
has an extremely detailed section on memory layout notation.

P()
Any sufficiently big library or algorithm needs a good data layout.
Much like the conventional wisdom: good data structure eliminates the need for smart algorithms.
So yes, I'm prepared for practical programming: I am focused on data instead of code.

SECTION2(beyond-von-neumann, Thinking Beyond von Neumann Architecture)

P()
A lot of programming wisdom is specific to byte-churning von Neumann architecture.
The pinnacle of programming excellence is using bit hacks.
It looks like a random ASCII soup, but it works miracles!

P()
Brainfuck has no bit hacks, because its execution model is arithmetic.
Implementing boolean operations is actually hard in Brainfuck.
Once you start programming in Brainfuck, your brain rewires itself.
You have an alternative to von Neumann bithack-driven programming.
You're free now.

SECTION2(should-you, Should You Try Brainfuck Too?)

P()
Yes, you should.
It's an illuminating experience.
While I'm not sure if it's good for your mental health, it's fun at least.

P()
So go ahead, hack up your own
A(https://github.com/bf-enterprise-solutions/os.bf, OS.bf)
and learn your own lessons from programming in the best programming language out there!

P()
Or just star some of
A(https://github.com/bf-enterprise-solutions, my projects)
and get horrified from their code 😃

#include "template/feedback"
#include "template/footer"
