#define TITLE I Want My Scrollbar Back (A 2-Minute Read)
#define DESCRIPTION "The modern web is empowered by dynamic content loading. The modern reading is impoverished by it."
#define IMAGE assets/thumbnail-323230-EDDB4F-F06529-FFFFFF.png
#define DARK_COLOR HASH()323230
#define LIGHT_COLOR HASH()ffffff
#define DARK_ACCENT_COLOR HASH()eddb4f
#define LIGHT_ACCENT_COLOR HASH()f06529

#include "template/header"

P()
One of my main activities every day is reading.
I read fiction (often as physical books I've gone through a great pain to get.)
I read tech docs.
I read blog posts.

P()
I'm deprived of my attention, though.
So I usually can't read long-form posts, research papers, and non-fiction books.
I skip pages or slip through them without realizing what the words say.

P()
That's why I always want to know what kind of page I'm looking at.
Is this a casual intro blog post?
Is this a researchy long-read?
Is this an industry report?
I have to allocate my resources accordingly.
Time and focus—I lack the latter, but often the former too.

P()
While some authors/blogs/engines provide the "minutes to read" metric, most don't.
So I have to rely on the omnipresent interface affordance: the scrollbar.
When I see that the scrollbar takes one-fourth of the screen,
I'm like "Okay, two minute read, I'll get to it right now!"
When the scrollbar is a dot instead of a bar, well...
I'm going to read it, but later, I guess?

P()
But the scrollbar is dead.
All too often I open a page, seeing a big scrollbar.
Embrace the calmness of a short text ahead.
And discover that the page is a long-ass read with chunks of content loading dynamically via JavaScript.
I can no longer rely on the scrollbar because they broke it.

P()
Please, get my scrollbar back.
I can't read your writing without it.

DETAILS(Okay, how do I fix it?)
ULI Move your content to static pages or pre-bake it into the pages server-side.
LI  Load your content eagerly right at the page load or shortly after,
so that the scrollbar is more realistic most of the time.
LI  Page the huge content (like books) so that the effect of scrollbar surprise is less pronounced.
LI  In the dire case none of these solutions work and you're in the 1% that actually needs dynamic loading for text:
provide an alternative indication for page length, like "minutes to read" or custom progress bar.
END(UL)
END(DETAILS)

P()
Thank you.

#include "template/feedback"
#include "template/footer"
