#define TITLE There Is No Such Thing As The Regex
#define DESCRIPTION "Regular expressions seem to be quite coherent, right? \
This scary (.*[^}(]) symbol soup that only the select few can master.\
Except that there are at least half a dozen (slightly incompatible) varieties of this soup.\
Bon Appétit!"
#define IMAGE assets/the-regex.png
#define IMAGE_ALT "Cold colors digital thumbnail with handwritten text on it.\
The text in the center says 'The R*gex' (meaning Regex).\
It's crossed out.\
In the corners, there's attribution to Artyom Bologov and aartaka.me address."
#define LIGHT_COLOR HASH()eff8ff
#define DARK_COLOR HASH()26201a
#define LIGHT_ACCENT_COLOR HASH()e60026
#define DARK_ACCENT_COLOR HASH()ff3800

#include "template/header"

IMG(IMAGE, IMAGE_ALT)

P()
I have a new challenge for myself: solving RosettaCode only using CD(ed) scripts.
Not even CD(sed), plain old CD(ed).
This makes me try to run as many versions of this venerable editor.
Or at least read the manuals—it's hard to get BSD/Mac ed while on Guix.
Several things I noticed:

ULI CLI flags differ dramatically between versions, both in meaning and quantity, with 10 for GNU ed and 1 for Plan 9 ed.
LI  Command lists are slightly incompatible too—GNU ed CD(z) command is CD(b) in Plan 9 ed, for example.
LI  And the regex syntax is not really portable, with many essential features missing from some versions.
END(UL)

P()
I'm saying "essential", but who's to decide?
Apparently, GNU, BSD, MacOS, and Plan 9 CD(ed) maintainers chose their regex version consciously.
And, apparently, they thought their version is The Regex.

P()
But there's no such thing as The Regex.
There are only heaps of slightly incompatible syntaxes and extensions to the basic idea.
And there's no basic idea either, as I'll try to show.

P()
I'll use the chrestomathic example: parsing email addresses.
Don't expect the RFC-compliant pattern, rather let's simplify this to essentials.
Email address comes in three parts:

ULI A user name with almost anything in it.
LI  And an alphanumeric and dashed domain name with periods.
LI  Separated by an at sign.
END(UL)

P()
This should be enough to showcase the features of most dialects.
But (here's a spoiler) it might be too simple for some regex to shine.

SECTION2(regular-languages, Regular Languages and Kleene Star)

P()
So they wanted to describe languages in the middle of previous century.
A(https://en.wikipedia.org/wiki/Regular_language, The definition from Wikipedia) is:
"[Regular language] is a formal language that can be defined by a regular expression."
Not making things clearer.
Luckily, Wikipedia defines it more rigorously below.
Here's my summary:

DL(Ø)
Empty language, nothingness.
DD(a)
Some symbol.
DD({a...})
A set of symbols.
DD(X*)
Something with Kleene star, repeated zero or more times.
DD(∪ and •)
Union and concatenation, set operations on sub-languages.
END(DL)

P()
Quite different from the popular idea of regex as we know it, right?
The only comforting resemblance is Kleene star.
Let's try to define a regular expression describing emails in it.

PRE(math)
{a..z, A..Z, 0..9, ...}*•{@}•{a..z, A..Z, 0..9, -}*•({.}•{a..z, A..Z, 0..9, -})*
PRECAP(Email addresses as regular language)

P()
Notice that these ellipses are just me being lazy and not wanting to list out every possible character.
Because that's what happens when you mess with math people.
No mercy.

P()
"Regular expressions" as they initially were... look quite alien.
But they'll fix it in the course of history, right?

SECTION2(posix, Thompson AMP() Basic POSIX/UNIX Regex)

P()
Ken Thompson fixed regex and turned them into something we're familliar with.
The transitionary state can be seen in the
A(https://dl.acm.org/doi/abs/10.1145/363347.363387, Regular Expression Search Algorithm paper).
There's still • operator, even if used internally.
∪ turns into a vertical bar CD(|).
And Kleene star remains as is.
That's it really.

P()
What followed is so-called Basic Regular Expressions.
Notable additions:

ULI Period CD(.) as the placeholder for any single char.
LI  Character classes delimited by square brackets.
LI  CD(^) for start of line and CD($) for end of line.
LI  Curly braces for "match N to M times" scenario.
LI  Parentheses for regex grouping.
LI  Kleene star is stil there, of course.
END(UL)

P()
One problematic thing is that one has to mark braces and parentheses by backslash.
To highlight their special status.
What about square brackets?
No backslash.
Inconsistent and error-prone.

P()
Here's how POSIX Basic Regular Expressions version of email address might look like:

PRE(sed)
.\{1,\}@[-[:alnum:]]*\(\.[-[:alnum:]]*\)*
PRECAP(Email address as POSIX Basic Regex)

P()
A lot of backslashes, ugh.
It's only intuitive that one might want less backslashes.
One superpower of BRE, though: backreferences!
CD(\\n) (backslash with a number) means referencing the previous regex group.
The parenthesized one.
Most of the flawors below lost this ability for some reason.
A shame.

SECTION2(plan-9, Plan 9 Regex)

P()

A(https://9fans.github.io/plan9port/man/man7/regexp.html, These)
are closer to initial Thompson ones, but add some quality of life features:

ULI CD(+) for "one or more match".
LI  CD(?) for "one or zero matches".
LI  No backslashes!
END(UL)

PRE(sed)
.+@[-a-zA-Z0-9]+(\.[-a-zA-Z0-9]+)+
PRECAP(Email address as Plan 9)

P()
What Plan 9 regex miss is curly braces and spelled-out char classes.
Yes, no quantified matches and convenient Unicode-aware matching.
So overall Plan 9 regex are a syntactic evolution over BRE, but they miss a lot of semantic necessities.
For whatever reason.

SECTION2(extended-posix, Extended POSIX Regex)

P()
The synthesis (not necessarily historical, but rather ideological) of BREs and Plan 9: ERE.
A(https://manpage.me/index.cgi?q=re_format&sektion=7&apropos=0&manpath=FreeBSD+12-CURRENT+and+Ports, Extended Regular Expressions).
No backslashes, curly braces are back, as are char classes.
That's where we get closer to conventional regex as we know them:

PRE(sed)
.+@[-[:alnum:]]+(\.[-[:alnum:]]+)+
PRECAP(Email address as POSIX Basic Regex)

SECTION2(gnu-emacs, GNU AMP() Emacs Regex)

P()
GNU regex (as present in GNU ed, which I'm currently using, and in GNU Emacs as the main dissemination source) are extending over POSIX ones.
Mainly via backslash sequences.
A(https://www.gnu.org/software/emacs/manual/html_node/emacs/Regexp-Backslash.html, Lots of them).
Notable highlights:

DL(\\w)
For any word (alphanumeric and underscore) char.
DD(\\W)
For any NON-word char.
DD(\\b)
For word boundary.
DD(\\B)
For inside-word char boundary.
DD(\\s)
For spaces.
DD(\\S)
For NON-spaces.
END(DL)

P()
You probably got the idea—some letter for char class, and its capital version for inversion or special case.
No, wait, there are lots of special cases.
I'm not even sure what's the heuristic.
But still, lots of special sequences for fun and profit:

PRE(sed)
.+@\w+(\.\w+)+
PRECAP(Email address as GNU regex)

P()
Our email pattern gets shorter and shorter, which is a win.
(Ignore the fact that I'm excluding dashes from domain names now.)
But it's also confined to GNU software, which sucks.
Because there is other software.
Like...

SECTION2(sql-postgres, SQL AMP() Postgres)

P()
SQL has these weird CD(LIKE) patterns:

PRE(sql)
an_thing -- _ stands for any single char
%prefixed -- % is any set of chars
suffixed% -- Same %, but ending a string now
%wrapped% -- A string containing 'wrapped' in any position
PRECAP(LIKE patterns in SQL)

P()
But these are unrelated to regex, right?
They look so alien, after all.
Unfortunately, there are systems where this alien syntax is valid regex.
Like Postgres.

P()
Postgres regex, as they call it, is a frivolous mix of POSIX and SQL LIKE.
Period (CD(.)), our trusted friend, is missing in Postgres syntax, and is replaced by underscore (CD(_)).
A soul-calming CD(.*) sequence is CD(%) instead.
The rest is plain POSIX EREs.

PRE(sed)
%@[-a-zA-Z0-9]+(.[-a-zA-Z0-9]+)+
PRECAP(Email address as Postgres regex)

SECTION2(scheme-regex, Scheme Regex (SRFI 115))

P()
Scheme (and Lisps in general) are famous for the Domain-Specific Languages.
You can build a subset of a language that maps 1-to-1 to the problem domain.
Which is a blessing, isn't it?
But this also gives rise to really complex and Turing-complete languages one can conjure.
Like Scheme Regular Expressions (SRE)
A(https://srfi.schemers.org/srfi-115, described in SRFI 115).

P()
These encode regular expressions as parenthesised lists.
The patterns form trees of expressions that are parseable but somewhat verbose.
And, I bet, they don't look like the idea of The Regex:

PRE(scheme)
(: (* any) #\@ (* (or #\- alnum)) (+ (: #\. (+ (or #\- alnum)))))
PRECAP((Untested) email pattern as a Scheme regex)

P()
Here's your portion of parentheses disgust for today.
No need to thank me.

SECTION2(perl, Perl Regex)

P()
I'll skip this section, because I'm unfamiliar with Perl.
But what I know is that Perl regex/string processing it Turing-complete.
They basically are Extended Extended Extended POSIX regex.
Pretty with the basics, yet syntactically horrifying with advanced things
(like lookaheads and recursive matching.)

P()
I'm not listing an email example here, because it's going to look the same as GNU one.
See? No longer a useful litmus test!

SECTION2(modern, Modern Regex: Python, JS, .NET)

P()
Most modern languages and runtimes have their own regex libraries/syntax.
Some (JS) even go as far as having a dedicated language syntax for regex literals.
Given the ubiquity of regex, they must be similar, transferable, and still powerful, right?
Only the latter is true.

P()
A(https://stackoverflow.com/questions/546433/regular-expression-to-match-balanced-parentheses, See this StackOverflow question)
for a staggering showcase of the syntaxes modern RE systems boast.
I'm linking to it in part because my example with email is no longer useful.
Most modern RE systems will have something similar(ly short) to Perl/GNU regex.

SECTION2(post-modern, Post Modern Regex: Any Pattern-Matching Language?)

P()
What about PEG?
Parsing Expression Grammar, I mean.
The thing specifying parsers.
A parser for email might read:

PRE()
.*@[-a-zA-Z0-9]+(\.[-a-zA-Z0-9]+)+
PRECAP(A potential PEG parser for emails)

P()
Which looks almost like POSIX ERE, right?
Yet it's called PEG.
So one has two options here:

ULI Say that regexes are what looks like ones and embrace PEG.
LI  Say that regexes are what's called that (including Scheme Regex) and reject PEG regex-yness.
END(UL)

P()
Either way, regex proves to be a complicated concept with no set syntax/definition.
And any other pattern-matching language has a chance to be called regex, really.
Be it CD(scanf) patterns, BNF/yacc parsers, or templating engines.

SECTION2(really, The Regex, Really?)

P()
Given the variety of syntaxes listed in this post, there seem to be no basic properties to regex.
The only thing making appearance everywhere is Kleene star.
The only one.
Other "obvious" and "basic" things like CD(.) and CD({}) are often missing or re-imagined.
So, is there The Regex?

P()
No.
HYPERTEXTONLY(</p>)

DETAILS(A longer answer would be:)
There's no definitive spec/version/implementation.
Some implementations, like Perl and GNU, are more successful/useful/influential.
Some syntactic features appear more than the others.
But there's no consensus and there are lots of weird outliers.
END(DETAILS)

P()
Use whatever you like and don't let anyone tell you it is not regex.
(Just remember to mention which syntax/implementation you use when sharing your sed one-liners with people.)

#include "template/feedback"
#include "template/footer"
