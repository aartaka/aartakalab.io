;;; packages.scm:
;; (define-structure NAME (export main)
;;   (open prescheme)
;;   (open ps-flonums)
;;   (files FILE-NAME))

;;; REPL:
;; (prescheme-compiler 'NAME '("packages.scm") 'NAME-init "main.c")

(define-syntax check-arg
  (syntax-rules (exact-integer? inexact? string? vector? boolean? char?
                                input-port? output-port? address?)
    ((_ inexact? value . rest)
     (begin (fl+ 1.3 value) #t))
    ((_ exact-integer? value . rest)
     ;; TODO: how do we mark integers?
     (begin (+ 1 value) #t))
    ((_ string? value . rest)
     (begin (string-length value) #t))
    ((_ vector? value . rest)
     (begin (vector-length value) #t))
    ((_ boolean? value . rest)
     (begin (eq? #t value) #t))
    ((_ char? value . rest)
     (begin (char=? value (string-ref "a" 0)) #t))
    ((_ input-port? value . rest)
     (begin (eq? value (current-input-port)) #t))
    ((_ output-port? value . rest)
     (begin (eq? value (current-output-port)) #t))
    ((_ address? value . rest)
     (begin (address+ value 0) #t))
    ;; What is the type of structs?
    ;; ((_ anything value . rest)
    ;;  (begin (address+ value 0) #t))
    ))

(define-syntax values-checked
  (syntax-rules (exact-integer? inexact? string? vector? boolean? char? input-port? output-port?)
    ((_ (predicate) value)
     (begin (check-arg predicate value) value))
    ((_ (predicate ...) value ...)
     (values (values-checked (predicate) value) ...))))

(define-syntax %lambda-checked
  (syntax-rules ()
    ((_ name (body ...) args (checks ...))
     (lambda args
       checks ...
       body ...))
    ((_ name body (args ...) (checks ...) (arg pred) . rest)
     (%lambda-checked
      name body
      (args ... arg) (checks ... (check-arg pred arg 'name)) . rest))
    ((_ name body (args ...) (checks ...) arg . rest)
     (%lambda-checked
      name body
      (args ... arg) (checks ...) . rest))
    ((_ name body (args ...) (checks ...) . last)
     (%lambda-checked
      name body
      (args ... . last) (checks ...)))))

(define-syntax lambda-checked
  (syntax-rules ()
    ((_ () body ...)
     (lambda () body ...))
    ((_ (arg . args) body ...)
     (%lambda-checked lambda-checked (body ...) () () arg . args))
    ;; Case of arg->list lambda, no-op.
    ((_ arg body ...)
     (lambda arg body ...))))

(define-syntax define-checked
  (syntax-rules ()
    ;; Procedure
    ((_ (name . args) body ...)
     (define name (%lambda-checked name (body ...) () () . args)))
    ;; Variable
    ((_ name pred value)
     (define name (values-checked (pred) value)))))

;; Example use:
;; (define-checked (main (argc exact-integer?) (argv input-port?))
;;   (write-string "Hello, world!" (current-output-port))
;;   0)

;; Example output:
;; long main(long argc_0X, FILE * argv_1X)
;; {

;;  {  ps_write_string("Hello, world!", (stdout));
;;   return 0;}
;; }
