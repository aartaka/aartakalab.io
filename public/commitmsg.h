#define TITLE A&#39;s Commit Messages Guide: Location, Action, Rationale
#define DESCRIPTION "Commit messages are the critical thing you stare at often enough. So better make them informational and readable."
#define IMAGE assets/commitmsg.png
#define IMAGE_ALT "Warm colors thumbnail.\
On the left, 'Artyom's Commit Messages Guide' is written in dark orange.\
In the center, template 'file(function): Unfuck—because yes.' is written in contrasting black.\
In the upper right corner, 'aartaka.me' address is written in the same dark orange."
#define DARK_COLOR HASH()221b27
#define LIGHT_COLOR HASH()ecd8a4
#define LIGHT_ACCENT_COLOR HASH()b84125
#define DARK_ACCENT_COLOR HASH()f74e27

#include "template/header"

IMG(IMAGE, IMAGE_ALT)

P()
I'm writing and searching lots of commit messages every day.
It's easy to get lost in them.
Yet I don't.
The reason is that I write reasonable commit messages.
(I'm saying it in such a blunt way because it's not necessarily my merit—I
had great teachers on Nyxt team and Guix community!)
And I figured I should explain the reasoning behind my style.
Might be helpful to someone starting out or looking for better conventions.

P()
The heuristic is simple:

DL(Location) Localize the change to file/function/object etc.
DD(Action)  Explain what the change does.
DD(Rationale)   Clarify the reasons and context for it.
END(DL)

P()
Yes, all in one message without description (if possible.)
I'll go through these parts in the order.
Hopefully explaining things clearly enough for you to embrace the system.

SECTION2(location, Location: Isolating the Change)

P()
Filesystem is a huge helper and marker for project structure.
Java classpaths, C include paths, conventional Open Source project files
(README, LICENSE/COPYING, CONTRIBUTING.)
Locating the change via file structure is half of the problem for change explanation.
That's why my commit structure is so elaborate on locations.

P()
First, file paths.
Include full paths to files so that it's clear where the change happened.
If your project structure is (already) reflecting the code structure
then listing the file/directory should be enough to show change scope.
Some exceptions I use:

ULI Omit file extension if it's obvious.
LI  Omit full directory path if it's The Directory,
like CD(src/) for code-oriented project, CD(www/) or CD(public/) for pages, and CD(data/) for datasets.
LI  Use shell wildcards (CD(something.*.something)) or brace expansion (CD(file.{c,h})) to mark groups of files.
LI  Or outright omit the location when the commit involves multiple files without any main one.
END(UL)

P()
Second, exact change location.
This one is elaborate!
There are several syntactic markers for the location that I use:
Parentheses, square brackets, angle brackets, and some regex syntax.
Mostly in the order of increasing specificity:

ULI Parentheses are for toplevel entity highlighting.
In case the language is function/procedure-oriented (like Scheme or C), function name is such an entity.
In case the language is object-oriented (like Commmon Lisp or C++), it's the class that matters.
In other cases... whatever the general structure unit for the file is.
While writing for this blog, I often use section IDs as this toplevel entity markers.
As in CD(commitmsg(HASH()location): ...) for commit relating to the section you're reading right now.

LI  Square brackets are for sub-units of the parenthesized thing.
For OOP codebase, these usually are class methods or slots/properties/fields.
For functional ones, it might be local functions, arguments, or blocks.
It's rare enough seeing these, you should focus on parenthesized location first.

LI  Angle brackets are for sub-sub-entities.
Like individual arguments or variables in the body of the function/method.
Barely ever useful, I'm only including these for completeness.

LI  Here's an atypical bit: I use regex-y syntax
(as suggested in my A(regex-pronouns, Regex Pronouns) post)
to shorten/condense the location.
Writing CD(describe(describe-*): ...) (from Nyxt) involves all the CD(describe-something) functions.
Or doing a change to function named CD(hello-there) in file CD(hello.lisp) might be CD(hello(-there): ...)
Noticed the reuse of file path as function name prefix?
Easy to get carried away with regex, but I try to remain relatively sane about locations.
END(UL)

P()
Some examples:

PRE()
// From A(https://github.com/aartaka/cl-blc, cl-blc):
cl-blc.asd: // Plain file with extension.
reductions: // Obvious extension (.lisp in this case), not included
cl-blc(eval): // Function eval in cl-blc.lisp
(compile): // Omitting the obvious file name (if there is one?)—cl-blc.lisp

// From this website
ideas(How I write commit messages): // Section name as location
screen.css(code,kbd,sample): // CSS selector as location
markup(DETAILS HASH()GEMTEXT): // C Macros with HASH()ifdef-s

// From my A(https://srfi.schemers.org/srfi-253, SRFI-253)
srfi-253.html(HASH()spec--values-checked): // HTML ID as the unit
impl.{chicken,kawa}.scm: // Bracket replacement
impl.*.scm(%lambda-checked): // Wildcard for all the matching files

// From Nyxt
buffer(buffer,panel-buffer[style]): // Square bracket slots in comma-separated classes
mode((dis|en)able-modes): // Complex regex use

// Not giving examples for angle brackets, sorry
PRECAP(Examples of locations I use in my projects)

P()
The trend is somewhat traceable: simple projects often only need file/function locations.
Complex projects (like SRFIs and Nyxt) need more concrete locations.

SECTION2(action, Action: Explaining the Change)

P()
Commit messages are the part most commit guidelines focus on.
Writing in imperative mood, expressing the "why" etc.
It's Writing 101 all over again.
The vital thing I took (and ignored on this blog) from writing classes?
Put important things first.
With commits/changes/alterations/modifications...
the most important thing is the action.
Verb, in other words.

P()
Some possible verbs/actions:

ULI Add.
LI  Remove/Delete.
LI  Update/Amend.
LI  Refactor.
LI  Optimize.
LI  Fix.
LI  Unfuck.
END(UL)

P()
The pattern is simple—whatever happens to the code, ends up in commit message.
The person reading the logs immediately gets the idea of what you did
(and where, if you followed previous section.)
Commits tell a story of what one did to the code/data/narrative.

P()
The action-ability of my commit messages is the reason I dislike
A(https://www.conventionalcommits.org/en/v1.0.0/, Conventional Commits) and the kind.
You don't need to say whether the thing is a feature or a bugfix, you just say what's there.
Directly.
CD(Add) and CD(Optimize) also reads better than CD(feat) and CD(perf).

P()
Oh, and, this goes without saying?
You can only distill the commit to one verb if it does only one thing.
Make changes atomic and self-contained.
You'll have no trouble explaining what you did then.

SECTION2(rationale, Rationale: Contextualizing the Change)

P()
Put rationale (the "why") into commit message whenever possible.
That's where my style breaks some of the conventions.
The reason?
No one reads commit descriptions!
What was the last time you, dear reader, intentionally looked through commit descriptions?
Especially so—using the un-ergonomic CLI git display?
Ugh.

P()
That's why I try to put rationale for the change into commit message.
The reader (often the future Artyom himself!) will thank me later.

P()
Luckily, verb/action-orientation is good for rationale.
All the "Fix" messages almost automatically hint at what was broken.
All the "Optimize" messages hint at what was slow/over-allocating.
And the "Remove" messages (my favorite!) often end up with
"Remove X—unused!" or "Remove Y—useless!"
A relief.

P()
My favorite composition of the three things I suggested is "Add" message.

ULI Location (often precise to the new function or slot) shows what was added,
LI  Action tells that there was a signification addition,
LI  And there's a looooot of space for rationale after that!
And you might not even need that space—location and action already tell a lot.
END(UL)

SECTION2(formalities, Formalities: Capitalization, Punctuation, etc.)

P()
The main structure (Where, What, Why?) out of the way.
Time to get to formalities:

ULI Separate location from the rest of the message with a colon and a space.
LI  Capitalize the first word of the message (usually verb), unless it's a code entity or something.
LI  Finish messages with whatever you like. I tend to use period out of stubbornness.
Most other guides recommend not using punctuation, so I guess don't use it?
LI  Use Unicode when necessary—even terminals are Unicode-enabled now. Add some m-dashes, won't you?
LI  Try to cap commit message length at 72 chars.
Note that I'm not saying 50 here, like all the other guides.
My style requires much more space, but also provides more information to the log reader.
And then, our screens are much larger now than they used to be when 50 char restriction was useful.
END(UL)

SECTION2(real-guix, Real World Inspiration: Guix!)

P()
I am not trying to say that Guix uses my convention!
It's rather the reverse: my style is heavily influenced by Guix.
And stripped down for my small scale projects.

P()
So what Guix does is

ULI Add location (usually a toplevel directory like CD(gnu/)) to the front.
LI  Add the summary for the change after location.
LI  And list all the files, functions/variables (in parentheses),
slots (in square brackets),
and minor details (in angle brackets) with detailed changes—all in the commit description.
END(UL)

P()
Here's a sample commit:

PRE()
gnu: kvantum: Update to 1.1.2.

* gnu/packages/qt.scm (kvantum): Update to 1.1.2.
    [source]: Switch to git-fetch.
    [arguments]: Set #:qtbase to qtbase.
    <#:phases>: Adjust patch-style-dir phase.
    [inputs]: Remove libx11, libxext, qtbase-5, qtsvg-5, and qtx11extras; add qtsvg.
    [native-inputs]: Remove qttools-5; add qttools.
PRECAP(One of the (more involved) Guix commit messages)

P()
It's big and has lots of syntax, but it also gives you a lot of information about what's changed!
That's what I strive for in my commits—maximum meaning per minimum space.
Preferably fitting things into one commit message without description (unlike Guix):

PRE()
packages/qt(kvantum): Update to 1.1.2 AMP() switch to git-fetch.
PRECAP(Guix commit refactored in my style)

SECTION2(summary, Summary: Write Commits!)

ULI Localize the change to file/function/class/slot/variable.
LI  Highlight the action you've commited.
LI  And explain why you did it.
END(UL)

PRE()
file(function): Unfuck—was preventing further refactoring.
PRECAP(Reference for my commit style)

P()
Hopefully y'all learned something from this posts and I can see more informative commits around!
Thanks for your care 🖤

#include "template/feedback"
#include "template/footer"
