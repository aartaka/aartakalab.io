#define TITLE Stuplime Programming
#define SUBTITLE Doing Meaningless Programs... Because Yes
#define DESCRIPTION ""

#include "template/slide"

SECTION2(identity, Identity)

P()
Artyom Bologov is:

ULI Lisp, C, and Brainfuck programmer.
LI  Absurdist artist.
LI  Fascinated by Turing-completeness and algorave.
LI  Casually into philosophy (and Sianne Ngai in particular).
END(UL)

SECTION2(stuplimity, Stuplimity)

P()
I like Ngai's idea of stuplimity, a mixed feeling on amazement and boredom.
All too characteristic of modern days, ain't it?
Here's how she defines it:

BQ()One strategy for calling attention to the ... the mixture of shock and exhaustion produced and sustained by a text like Americans ... is to refer to the aesthetic experience in which astonishment is paradoxically united with boredom as stuplimity.
BQCAP(Sianne Ngai on the contrast of stuplime and sublime, Ugly Feelings, 6: stuplimity)

SECTION2(stuplimity-2, Stuplimity: Attempt 2)

P()
Okay, no, let's try once again:

BQ()Stuplimity reveals the limits of our ability to comprehend a vastly extended form as a totality, as does Kant’s mathematical sublime, yet not through an encounter with the infinite but with finite bits and scraps of material in repetition.
BQCAP(Sianne Ngai on the uniqueness of stuplimity, Ugly Feelings, 6: stuplimity)

SECTION2(stuplimity-3, Stuplimity: Attempt 3)

P()
No, that's not right either.

P()
Stuplimity is something horrifyingly huge, yet astonishingly boring:

ULI 200000 lines of logs.
LI  Codebases written in ANSI C.
LI  Raw representation of neural network results.
LI  Fully portable Makefiles 500 lines long.
LI  Numpy/Sci-py-exclusive Python code.
END(UL)

P()
You are overwhelmed by the mere existence of these.
You wouldn't ever like to deal with them.
But the fear is bordering boredom at how mundane and meaningless all these are.

</section>

#include "template/footer"
